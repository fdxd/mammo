import torch
from torch import Tensor
from torch.nn import Linear, MSELoss, functional as F
from torch.optim import SGD, Adam, RMSprop
from torch.autograd import Variable
import numpy as np
from math import cos,sin
from math import pi as PI
from matplotlib.pyplot import imshow,plot,figure,show,scatter
import torch.nn as nn
from torch.optim import lr_scheduler

def R(theta):
    s = torch.sin(theta)
    c = torch.cos(theta)
    rot = torch.stack([torch.stack([c, -s]),
                       torch.stack([s, c])])
    return rot

def P2(x,coef):
    xx = torch.cat((x ** 2,x, torch.ones(x.shape)), axis=1)
    return torch.matmul(xx,coef).unsqueeze(1)

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import copy
def init_parabola(x,y):
    polynomial_features = PolynomialFeatures(degree=2)
    x_poly = polynomial_features.fit_transform(x)

    model = LinearRegression()
    model.fit(x_poly, y)
    y_poly_pred = model.predict(x_poly)
    coef = copy.deepcopy( model.coef_.squeeze(0))
    coef[0] = coef[0] +  copy.deepcopy(model.intercept_)
    coef = np.flipud(coef).copy()
    return y_poly_pred,coef


# define our data generation function
def data_generator(data_size=1000):
    if True :
        theta = torch.tensor([0.])
        coef = torch.tensor([1., 0., -2.])
    else :
        theta = PI * (torch.rand(1) / 2) - PI / 4
        coef = torch.tensor([1., 0., -2.])
        #coef =  torch.rand(3)

    x = torch.rand(data_size,1) * 2 - 1

    yp = P2(x, coef)
    XY = torch.matmul(R(theta).squeeze(-1),torch.cat((x,yp),axis=1).transpose(0,1) )

    nX = XY[0, :]
    nY = XY[1, :]

    return nX,nY,{'coef':coef,'theta':theta}


class Net(nn.Module):
    def __init__(self,coef=None,theta=None):
        super().__init__()

        self.coef = nn.Parameter(torch.rand(3)) if coef is None else nn.Parameter(torch.tensor(coef).type(torch.FloatTensor))
        self.theta = nn.Parameter(torch.rand(1) * 2 * PI) if theta is None else nn.Parameter(torch.tensor([theta]))

        self.coef.requires_grad = True
        self.theta.requires_grad = True

    def get_vars(self):
        coef = copy.deepcopy(self.coef.cpu().detach())
        theta = copy.deepcopy(self.theta.cpu().detach())
        return coef,theta

    def forward(self, x):
        y = P2(x, self.coef)

        s = torch.sin(self.theta)
        c = torch.cos(self.theta)

        nX = c * x - s * y
        nY = s * x + c * y

        return nX,nY


# Lets implement the error function
def mse(y_hat,y):
    return((y_hat-y)**2).mean()

def fit2(x, y):
    xx = torch.cat((x ** 2,x, torch.ones(x.shape)), axis=1)
    a_hat = torch.tensor([1.,1.,0])
    a_hat = nn.Parameter(a_hat)
    # Lets define our gradient descent updates
    accumulate_loss = []
    def update():
        y_hat = xx@a_hat # estimate y_hat
        loss = mse(y,y_hat) # compute error or loss
        accumulate_loss.append(loss)
    #     if t % 10 == 0: print(loss)
        loss.backward() # compute derivative/grad with respect to each parameter
        with torch.no_grad(): # deactivate backprop
            a_hat.sub_(lr*a_hat.grad) # gradient descent step
            a_hat.grad.zero_() # reset grads

    lr = 1e-2 # this is learning rate parameter, if curious, do google more on it
    for t in range(10000):
        update()


def fit(model,X, Y):
    critereon = MSELoss()
    nb_epochs = 2000000

    optimizer = SGD(model.parameters(), lr=0.01)
    #scheduler = lr_scheduler.StepLR(optimizer, step_size=200, gamma=0.7)

    for epoch in range(nb_epochs):

        optimizer.zero_grad()

        nX,nY = model(X)

        loss = critereon(nY, Y)

        loss.backward()

        optimizer.step()
        #scheduler.step()


        epoch_loss = loss.data


        coef,theta = model.get_vars()

        if not (epoch % 100) :
            print(f"Epoch: {epoch} Loss: {epoch_loss} , {coef},{theta}")

    figure(5); scatter(nX.detach().numpy(), nY.detach().numpy());            show()

    return model

if __name__ == "__main__":
    X,Y,verbose = data_generator(data_size=1000)
    figure(1);    scatter(X, Y);      show()
    X = X.unsqueeze(1)
    Y = Y.unsqueeze(1)

    iY,coef = init_parabola(X, Y)
    figure(2);    scatter(X, iY);    show()

    yp = P2(X, torch.tensor(coef).float())
    XY = torch.matmul(torch.cat((X, yp), axis=1), R(torch.tensor([0.])))
    nX = XY[0, :, 0]
    nY = XY[1, :, 0]

    figure(3);scatter(nX.detach().numpy(), nY.detach().numpy());show()

    print(verbose)



    model = Net(coef=torch.rand(3),theta=0.1)
    #model = Net(coef=copy.deepcopy(verbose['coef']),theta=copy.deepcopy(verbose['theta']))

    coef,theta = fit(model,X, Y)
    #fit2(X, Y)
