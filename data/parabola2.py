import torch
from torch import Tensor
from torch.nn import Linear, MSELoss, functional as F
from torch.optim import SGD, Adam, RMSprop
from torch.autograd import Variable
import numpy as np
from math import cos,sin

# define the model
class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.coef =Variable(Tensor(3))
        self.theta = Variable(Tensor(1))

    def R(theta):
        return [[cos(theta), -sin(theta)]
                [sin(theta), cos(theta)]]

    def forward(self, x):
        yp = self.coef * [(x ** 2) , x , 1]

        XY = self.R(self.theta) * [x ,yp]
        return XY


def fit_rotated_parabola(x,y) :
    xy=[x,y]

    Rxy=R(theta)*xy





# define our data generation function
def data_generator(data_size=1000):
    # f(x) = y = x^2 + 4x - 3
    inputs = []
    labels = []

    # loop data_size times to generate the data
    for ix in range(data_size):
        # generate a random number between 0 and 1000
        x = np.random.randint(2000) / 1000 # I edited here for you

        # calculate the y value using the function x^2 + 4x - 3
        y = (x * x) + (4 * x) - 3

        # append the values to our input and labels lists
        inputs.append([x*x, x])
        labels.append([y])

    return inputs, labels

# define the model
class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = Linear(2, 1)

    def forward(self, x):
        return self.fc1(x)

def train_model(model):
    # define the loss function
    critereon = MSELoss()
    # define the optimizer
    optimizer = SGD(model.parameters(), lr=0.01)


    # define the number of epochs and the data set size
    nb_epochs = 20000
    data_size = 1000

    # create our training loop
    for epoch in range(nb_epochs):
        X, y = data_generator(data_size)
        X = Variable(Tensor(X))
        y = Variable(Tensor(y))

        y_pred = model(X)

        loss = critereon(y_pred, y)

        epoch_loss = loss.data
        optimizer.zero_grad()

        loss.backward()

        optimizer.step()

        print("Epoch: {} Loss: {}".format(epoch, epoch_loss))

    return model
model = Net()
model = train_model(model)
# test the model
model.eval()
test_data = data_generator(1)
prediction = model(Variable(Tensor(test_data[0][0])))
print("Prediction: {}".format(prediction.data[0]))
print("Expected: {}".format(test_data[1][0]))


print('a & b:', model.fc1.weight)
print('c:', model.fc1.bias)

