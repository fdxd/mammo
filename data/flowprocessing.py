
def warp(x, flo):
    B, H, W, C = x.size()
    # mesh grid
    xx = torch.arange(0, W).view(1, -1).repeat(H, 1)
    yy = torch.arange(0, H).view(-1, 1).repeat(1, W)

    xx = xx.view(1, H, W, 1).repeat(B, 1, 1, 1)
    yy = yy.view(1, H, W, 1).repeat(B, 1, 1, 1)

    grid = torch.cat((xx, yy), 3).float()

    if x.is_cuda:
        grid = grid.cuda()

    vgrid = Variable(grid) + flo

    ## scale grid to [-1,1]
    vgrid[:, :, :, 0] = 2.0 * vgrid[:, :, :, 0].clone() / max(W - 1, 1) - 1.0
    vgrid[:, :, :, 1] = 2.0 * vgrid[:, :, :, 1].clone() / max(H - 1, 1) - 1.0

    x = x.permute(0, 3, 1, 2)

    output = torch.nn.functional.grid_sample(x, vgrid)
    mask = torch.autograd.Variable(torch.ones(x.size()))
    mask = torch.nn.functional.grid_sample(mask, vgrid)

    mask[mask < 0.9999] = 0
    mask[mask > 0] = 1

    return output * mask