# Cross-view transformers for multi-view analysis of unregistered medical images
# Copyright (C) 2021 Gijs van Tulder / Radboud University, the Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import csv
import os.path
import traceback
import tqdm
import numpy as np
import h5py
import pydicom
import skimage.transform
import pandas as pd
import nyu_cropping
from sklearn.model_selection import KFold

def get_options() :
    parser = argparse.ArgumentParser()
    parser.add_argument('--save-hdf5', metavar='HDF5',help='save scans to hdf5')
    parser.add_argument('--ntype', metavar='mass', help='mass or calc')
    parser.add_argument('--k_folds', default=5, type=int, required=False, help='rescaled images size')

    args = parser.parse_args()

    return args

import random

def get_samples_by_side(f):
    patients = list(f['scans'])
    k = []
    for i in patients :
        view = f['scans'][i]
        for v in view :
            k.append((i , v))

    return k

def get_samples_by_scan(f):
    patients = list(f['scans'])
    k = []
    for i in patients :
        view = f['scans'][i]
        for v in view :
            scan = f['scans'][i][v]
            for s in scan:
                k.append((i, v, s))

    return k


def filter_pairs(f,k):
    k_ = []
    for i in k :
        views = list(f['scans'][i[0]][i[1]])
        if ('cc' in views) and  ('mlo' in views)  :
            k_.append(i)
    return k_


def get_folds(k,k_folds=5,ntype='mass') :
    if k_folds > 0 :
        kfold = KFold(n_splits=k_folds, shuffle=True)
        folds = []
        for fold, [train, valid] in enumerate(kfold.split(k)):
            folds.append({'train_list' : sorted([(ntype,k[i]) for i in train]),
                          'valid_list' : sorted([(ntype,k[i]) for i in valid])})
    else :
        folds = [{'data_list': sorted([(ntype, i) for i in k])}]

    return folds

def get_samples_list(datafile,sample_type) :
    if sample_type == 'pairs' :
        f = h5py.File(datafile, 'r')
        k = get_samples_by_side(f)
        k = filter_pairs(f, k)
    elif sample_type == 'singles' :
        f = h5py.File(datafile, 'r')
        k = get_samples_by_scan(f)
    else :
        k =[]
    random.shuffle(k)
    return k


def create_folds_pairs(args,sample_type,k_folds) :
    k = get_samples_list(args.save_hdf5,sample_type)
    folds = get_folds(k,k_folds,args.ntype)

    p, f, e = fileparts(args.save_hdf5)
    for idx, fold in enumerate(folds) :
        foldpath = os.path.join(p, f'{f}_{sample_type}_fold_{idx:02}.pkl')
        with open(foldpath, 'wb') as file:
            pickle.dump(fold, file, pickle.HIGHEST_PROTOCOL)

from core.common import fileparts
import pickle
if __name__ == "__main__":
    args = get_options()
    rootpath = '/home/mahpods/WSP/datasets/Mammogram/DDSM/data_1024/'

    args_list = [{'filename': 'ddsm-WS20-mass-train.h5', 'ntype': 'mass'} ,
                 {'filename': 'ddsm-WS20-calc-train.h5', 'ntype': 'calc'}]

    for iargs in args_list :
        args.save_hdf5 = os.path.join(rootpath,iargs['filename'])
        args.ntype = iargs['ntype']
        create_folds_pairs(args, 'pairs',args.k_folds)
        create_folds_pairs(args,'singles',args.k_folds)

    args_list = [{'filename': 'ddsm-WS20-mass-test.h5', 'ntype': 'mass'},
                 {'filename': 'ddsm-WS20-calc-test.h5', 'ntype': 'calc'}]

    for iargs in args_list :
        args.save_hdf5 = os.path.join(rootpath,iargs['filename'])
        args.ntype = iargs['ntype']
        create_folds_pairs(args, 'pairs',0)
        create_folds_pairs(args,'singles',0)