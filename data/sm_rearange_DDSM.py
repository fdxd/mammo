import pandas as pd
import argparse
import os
import shutil
import numpy as np
import pydicom

from core.fileutils import get_files_list,fileparts

def get_study_list(df) :
    study_list = []
    for index, row in df.iterrows():
        study_list.append(row['image file path'].split('/')[0])

    return study_list

def copy_fields(row) :
    meta = dict()
    for item in row.keys() :
        meta[item] = row[item]

    return meta

'''
, ['patient_id','breast density','left or right breast','image view',\
                                      'abnormality id','abnormality type','calc type','calc distribution',\
                                              'assessment','pathology','subtlety']
'''
def get_meta_rows(df,item) :
    meta = []
    for index, row in df.iterrows():
        if row['image file path'].split('/')[0] == item :
            rdata = copy_fields(row)
                                              
            meta.append(rdata)
    return meta


import json

def rearange(datafolder,inst,outpath,meta) :
    study_item = inst[0]
    l = get_files_list(os.path.join(datafolder, study_item), '.dcm')
    if not (len(l) == 1) :
        print('assertion len(l) != 1')

    srcfile = l[0]
    dstfile = os.path.join(outpath, f'{study_item}.dcm')

    with open(os.path.join(outpath,'meta.json'), 'w') as f:
        json.dump(meta, f)

    print(f'Copied {srcfile} ---> {dstfile} ')

    for idx,item in enumerate(inst[1:]) :
        l = get_files_list(os.path.join(datafolder,item), '.dcm')
        for srcfile in l :
            index = int(item.replace(f'{study_item}_', ''))

            os.makedirs(os.path.join(outpath, f'{index:02}'),exist_ok=True)

            dstfile =''
            ds = pydicom.dcmread(srcfile)
            try:
                img_type = ds.SeriesDescription
                if "crop" in img_type:
                    dstfile = os.path.join(outpath, f'{index:02}', f'{study_item}_CROP.dcm')
                elif "mask" in img_type:
                    dstfile = os.path.join(outpath, f'{index:02}', f'{study_item}_MASK.dcm')
                else :
                    print('NOT CROP , NOT MASK')
            except:
                    arr = ds.pixel_array
                    unique = np.unique(arr).tolist()
                    if len(unique) != 2:
                        dstfile = os.path.join(outpath, f'{index:02}', f'{study_item}_CROP.dcm')
                    elif len(unique) == 2:
                        dstfile = os.path.join(outpath, f'{index:02}', f'{study_item}_MASK.dcm')
                    else :
                        print('NOT CROP , NOT MASK')

            shutil.copy2(srcfile, dstfile)
    
            print(f'Copied {srcfile} ---> {dstfile} ')


def rearange_DDSM(dataroot,indir,outroot) :
    l = {'Calc' : {'Train': 'calc_case_description_train_set.csv',  'Test': 'calc_case_description_test_set.csv'},
         'Mass': {'Train': 'mass_case_description_train_set.csv' , 'Test': 'mass_case_description_test_set.csv'}}

    datafolder = os.path.join(dataroot, indir,'CBIS-DDSM').replace('\\','/')
    subfolders = [f.path.replace('\\','/').split('/')[-1] for f in os.scandir(datafolder) if f.is_dir()]

    os.makedirs(outroot, exist_ok=True)

    for dstype in  l.keys() :
        for ds in l[dstype].keys() :
            shutil.copy2(os.path.join(dataroot,l[dstype][ds]), outroot)
            df = pd.read_csv(os.path.join(dataroot,l[dstype][ds]))
            study_list = get_study_list(df)
            for item in study_list :
                outpath = os.path.join(outroot,dstype,ds,item)
                os.makedirs(outpath,exist_ok=True)
                meta = get_meta_rows(df,item)
                inst = [i for i in subfolders if item in i]
                rearange(datafolder, inst, outpath,meta)

class BaseOptions():
    def __init__(self):
        self.initialized = False

    def initialize(self, parser):
        parser.add_argument('--dataroot', default = './dataset/DDSM/' , required=True, help='path to images')
        parser.add_argument('--outroot', default = './workset/CBIS-DDSM/WS01/', required=True, help='path to images')
        parser.add_argument('--indir', default ='manifest-ZkhPvrLo5216730872708713142', help='Internal directory path')

        self.initialized = True
        return parser

    def gather_options(self):
        if not self.initialized:  # check if it has been initialized
            parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
            parser = self.initialize(parser)

        # get the basic options
        opt, _ = parser.parse_known_args()

        self.parser = parser
        return parser.parse_args()

    def parse(self):
        """Parse our options, create checkpoints directory suffix, and set up gpu device."""
        opt = self.gather_options()

        self.opt = opt
        return self.opt


if __name__ == "__main__":
    opt = BaseOptions().parse()
    print(opt)
    rearange_DDSM(opt.dataroot, opt.indir,opt.outroot)



