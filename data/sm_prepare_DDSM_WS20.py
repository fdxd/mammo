import argparse
import csv
import os.path
import tqdm
import numpy as np
import h5py
import pydicom
import skimage.transform
import pandas as pd

def print_verbose(args, **kwargs):
    print(args, **kwargs)


def get_options() :
    parser = argparse.ArgumentParser()
    parser.add_argument('--csv', metavar='CSV', required=True, nargs='+')
    parser.add_argument('--basedir', metavar='DIR', required=True)
    parser.add_argument('--metafile', metavar='META', required=True)
    parser.add_argument('--save-hdf5', metavar='HDF5',
                        help='save scans to hdf5')
    parser.add_argument('--verbose', action='store_true')
    args = parser.parse_args()


    print_verbose(args)
    return args


def get_filepath_from_metadata(df,val) :
    SubjectID, StudyUID, SeriesUID, _ = val.split('/')
    curr = df.loc[(df['Subject ID'] == SubjectID) & (df['Study UID'] == StudyUID) & (df['Series UID'] == SeriesUID)]
    filepath = curr['File Location'].item()
    filepath = '/'.join(filepath.replace('\\','/').split('/')[1:])
    return filepath


def get_subjects(args) :
    subjects = {}

    ASSESSMENT_MAP = {'BENIGN_WITHOUT_CALLBACK': 'benign',
                      'BENIGN': 'benign', 'MALIGNANT': 'malignant'}
    ASSESSMENTS = ('benign', 'malignant')
    SIDE_MAP = {'LEFT': 'left', 'RIGHT': 'right'}
    ABNORMALITY_MAP = {'calcification': 'calcification', 'mass': 'mass'}
    VIEW_MAP = {'CC': 'cc', 'MLO': 'mlo'}

    df = pd.read_csv(args.metafile)

    for filename in args.csv:
    #   print_verbose(filename)
        with open(filename, 'r') as f:
            for line in tqdm.tqdm(csv.DictReader(f), desc='preload'):
                scan_key = (line['patient_id'], line['left or right breast'],
                            line['image view'], line['abnormality type'])
                side = SIDE_MAP[line['left or right breast']],
                view = VIEW_MAP[line['image view']],

                image_file_path = line['image file path'].strip()
                image_file_path = get_filepath_from_metadata(df, image_file_path)
                ROI_file_path1 = line['ROI mask file path'].strip()
                ROI_file_path1 = get_filepath_from_metadata(df, ROI_file_path1)
                ROI_file_path2 = line['cropped image file path'].strip()
                ROI_file_path2 = get_filepath_from_metadata(df, ROI_file_path2)

                #cropped_image_file_path = line['cropped image file path'].strip()
                #cropped_image_file_path = get_filepath_from_metadata(df, cropped_image_file_path)
                image_file_path = os.path.join(args.basedir, image_file_path , '1-1.dcm')
                if ROI_file_path1 == ROI_file_path2 :
                    ROI_file_path1 = os.path.join(args.basedir, ROI_file_path1,'1-1.dcm')
                    ROI_file_path2 = os.path.join(args.basedir, ROI_file_path2,'1-2.dcm')
                else :
                    ROI_file_path1 = os.path.join(args.basedir, ROI_file_path1,'1-1.dcm')
                    ROI_file_path2 = os.path.join(args.basedir, ROI_file_path2,'1-1.dcm')

                if os.path.getsize(ROI_file_path1) >  os.path.getsize(ROI_file_path2) :
                    crop_file_path = ROI_file_path2
                    mask_file_path = ROI_file_path1
                else :
                    crop_file_path = ROI_file_path1
                    mask_file_path = ROI_file_path2

                if not os.path.exists(image_file_path):
                    print_verbose('File not found: %s' % image_file_path)
                elif not os.path.exists(crop_file_path):
                    print_verbose('File not found: %s' % crop_file_path)
                elif not os.path.exists(mask_file_path):
                    print_verbose('File not found: %s' % mask_file_path)
                else:
                    with pydicom.filereader.dcmread(image_file_path, stop_before_pixels=True) as d:
                        img_size = (d['Rows'].value, d['Columns'].value)
                    with pydicom.filereader.dcmread(crop_file_path, stop_before_pixels=True) as d:
                        crop_size = (d['Rows'].value, d['Columns'].value)
                    with pydicom.filereader.dcmread(mask_file_path, stop_before_pixels=True) as d:
                        mask_size = (d['Rows'].value, d['Columns'].value)

                    assert(not (crop_size[0] == mask_size[0]))


                    if crop_size[0] > mask_size[0]:
                        crop_file_path, mask_file_path = mask_file_path, crop_file_path
                        crop_size, mask_size = mask_size, crop_size

                    if scan_key not in subjects:
                        subjects[scan_key] = {
                            'patient_id':       line['patient_id'],
                            'side':             SIDE_MAP[line['left or right breast']],
                            'view':             VIEW_MAP[line['image view']],
                            'density_score':    int(line['breast density'] if 'breast density' in line else line['breast_density']),
                            'image_file':       image_file_path,
                            'abnormality' : {},
                        }

                    abnormality_id = line['abnormality id']
                    abnormality_data = {
                        'abnormality type': ABNORMALITY_MAP[line['abnormality type']],
                        'assessment_score': int(line['assessment']),
                        'assessment_label': ASSESSMENT_MAP[line['pathology']],
                        'mask_file': mask_file_path,
                    }
                    for feature in ['mass shape','mass margins','subtlety','calc type','calc distribution'] :
                        if feature in line.keys() :
                            abnormality_data[feature] = line[feature]
                        else :
                            abnormality_data[feature] = None

                    subjects[scan_key]['abnormality'][abnormality_id] = abnormality_data

    return subjects

from data.preprocessing import checkLRFlip

def create_h5py(subjects, h5out, dst_size = (1024,1024)):
    for subject_idx, (subject_id, subject) in enumerate(tqdm.tqdm(subjects.items())):
        # load input image
        ds = pydicom.dcmread(subject['image_file'])
        image = ds.pixel_array
        image_fg_mask = image > 0

        print('Shape of input:', image.shape)
        print('Unique values: ', np.unique(image))
        print('Percentage > 0:', np.sum(image_fg_mask) / float(np.product(image.shape)))

        # scale roughly to [-1, +1]
        image = 2 * (image.astype(float) / 65534) - 1

        patient_id = subject['patient_id']
        view = subject['view']
        side = subject['side']

        # load input segmentations
        print_verbose('Segmentations:')
        segmentations = []
        for i, abn in enumerate(subject['abnormality']):
            s = subject['abnormality'][abn]['mask_file']
            ds = pydicom.dcmread(s)
            segmentation = ds.pixel_array

            # some segmentations have an incorrect size
            if segmentation.shape != image.shape:
                print('  %d: Original shape:' % i, segmentation.shape)
                segmentation = skimage.transform.resize(segmentation, image.shape, order=0)

            segmentations.append(segmentation > 0)
            print('  %d: Segmentation shape:' % i, segmentation.shape)
            assert segmentation.shape == image.shape
            assert len(np.unique(segmentation)) == 2
            print('      Foreground voxels: ', np.sum(segmentation > 0))
            print('      Unique values:     ', np.unique(segmentation))

        LR_flip = checkLRFlip(image)
        image = skimage.transform.resize(image, dst_size, order=0).astype('float16')
        image = np.fliplr(image) if LR_flip else image
        for idx,segmentation in enumerate(segmentations):
            segmentations[idx] = skimage.transform.resize(segmentation, dst_size, order=0).astype('bool')
            segmentations[idx] = np.fliplr(segmentations[idx]) if LR_flip else segmentations[idx]

        h5out.create_dataset(f'scans/{patient_id}/{side}/{view}/image',
                             data=image.astype('float16'), compression='gzip')
        h5out.create_dataset(f'scans/{patient_id}/{side}/{view}/segmentations',
                             data=segmentations, compression='gzip')

        group = h5out.create_group(f'scans/{patient_id}/{side}/{view}/meta')
        for k in ('patient_id', 'side', 'view', 'density_score', 'image_file'):
            group.attrs[k] = subject[k]

        group.attrs['num_abnormality'] = len(subject['abnormality'].keys())

        for ab in subject['abnormality']:
            group = h5out.create_group(f'scans/{patient_id}/{side}/{view}/abnormality_{ab}')
            for k in subject['abnormality'][ab]:
                if subject['abnormality'][ab][k] is not None :
                    group.attrs[k] = subject['abnormality'][ab][k]

        '''
        h5out.create_dataset(f'scans/{subject_idx}/image',
                             data=image.astype('float16'), compression='gzip')
        h5out.create_dataset(f'scans/{subject_idx}/segmentations',
                             data=segmentations, compression='gzip')
        group = h5out.create_group(f'scans/{subject_idx}/meta')
        for k in ('patient_id', 'side', 'view', 'density_score', 'image_file'):
            group.attrs[k] = subject[k]

        group.attrs['num_abnormality'] = len(subject['abnormality'].keys())

        for ab in subject['abnormality']:
            group = h5out.create_group(f'scans/{subject_idx}/abnormality_{ab}')
            for k in subject['abnormality'][ab]:
                if subject['abnormality'][ab][k] is not None :
                    group.attrs[k] = subject['abnormality'][ab][k]
        '''
if __name__ == "__main__":
    args = get_options()

    subjects = get_subjects(args)

    h5out = h5py.File(args.save_hdf5, 'w')

    create_h5py(subjects, h5out, dst_size = (1024,1024))

