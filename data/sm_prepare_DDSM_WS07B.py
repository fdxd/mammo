import argparse
import os
import shutil
import pydicom
import json

#from common.mammo_preprocessing import fullMammoPreprocess,maskPreprocess,sumMasks,norm_16b
#import matplotlib.pyplot as plt
import cv2
import pandas as pd
from skimage.measure import regionprops
from data.imutils import image_crop
from data.datautils import get_data
def get_meta_by_idx(meta,id) :
    for item in meta :
        if item['abnormality id'] == id :
            return item
    return []
from data.imutils import imshowpts
import numpy as np
def get_corners(msk) :
    white_pixels = np.array(np.where(msk >0))
    sy = min(white_pixels[0, :])
    ey = max(white_pixels[0, :])
    sx = min(white_pixels[1, :])
    ex = max(white_pixels[1, :])
    return sy,sx,ey,ex


def prepare_item(srcdir, dstdir,item,outsize) :
    os.makedirs(os.path.join(dstdir,item), exist_ok=True)
    shutil.copy2(os.path.join(srcdir,item,'meta.json'), os.path.join(dstdir,item,'meta.json'))

    simg,masks,labels = get_data(srcdir, item)

    for mimg,label in zip(masks,labels) :
        bbox = get_corners(mimg)

        csimg = image_crop(simg, bbox, perc=0.3)
        cmimg = image_crop(mimg, bbox, perc=0.3)

        csimg = cv2.resize(csimg, (outsize, outsize))
        cmimg = cv2.resize(cmimg, (outsize, outsize))

        dsimg = image_crop(simg, bbox, dstlen=outsize)
        dmimg = image_crop(mimg, bbox, dstlen=outsize)

        abid = f"{label['abnormality_id']:02}"
        cv2.imwrite(os.path.join(dstdir, os.path.join(item, f'{item}_RZCROP_{abid}.png')), csimg)
        cv2.imwrite(os.path.join(dstdir, os.path.join(item, f'{item}_RZMASK_{abid}.png')), cmimg)
        cv2.imwrite(os.path.join(dstdir, os.path.join(item, f'{item}_FXCROP_{abid}.png')), dsimg)
        cv2.imwrite(os.path.join(dstdir, os.path.join(item, f'{item}_FXMASK_{abid}.png')), dmimg)

    return labels
import copy
def prepare_DDSM(srcpath,dstpath,outsize) :
    #l = {'Calc' : {'Train': 'calc_case_description_train_set.csv',  'Test': 'calc_case_description_test_set.csv'},
#         'Mass': {'Train': 'mass_case_description_train_set.csv' , 'Test': 'mass_case_description_test_set.csv'}}
    l = {'Calc': {'Train': 'calc_case_description_train_set.csv'}}
    os.makedirs(dstpath, exist_ok=True)

    for dstype in  l.keys() :
        for ds in l[dstype].keys() :
            csvfile = os.path.join(dstpath,f'{ds}_{dstype}.csv')
            shutil.copy2(os.path.join(srcpath,l[dstype][ds]), os.path.join(dstpath,l[dstype][ds]))
            srcdir = os.path.join(srcpath, dstype, ds)
            dstdir = os.path.join(dstpath, dstype, ds)
            subfolders = sorted(os.listdir(srcdir))
            os.makedirs(dstdir, exist_ok=True)

            df = None
            for idx,item in enumerate(subfolders) :
                label = prepare_item(srcdir, dstdir, item, outsize)
                df_ = pd.DataFrame.from_dict(label)
                df = copy.deepcopy(df_) if df is None else df.append(df_,ignore_index=True)

                print(f'{ds} {dstype}-({idx} out of {len(subfolders)}) - {item})')
                #df = df.append(ilabel,ignore_index=True)

            df.to_csv(csvfile)
            print(f'Dumped - {csvfile}')


class BaseOptions():
    def __init__(self):
        self.initialized = False

    def initialize(self, parser):
        parser.add_argument('--srcpath', default = 'workset/CBIS-DDSM/WS01/' , required=False, help='path to images')
        parser.add_argument('--dstpath', default = 'workset/CBIS-DDSM/WS07B/576/', required=False, help='path to images')
        parser.add_argument('--outsize', default=576,  type=int, required=False, help='rescaled images size')

        self.initialized = True
        return parser

    def gather_options(self):
        if not self.initialized:  # check if it has been initialized
            parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
            parser = self.initialize(parser)

        # get the basic options
        opt, _ = parser.parse_known_args()

        self.parser = parser
        return parser.parse_args()

    def parse(self):
        """Parse our options, create checkpoints directory suffix, and set up gpu device."""
        opt = self.gather_options()

        self.opt = opt
        return self.opt


if __name__ == "__main__":
    from core.private_settings import ROOT_PATH
    opt = BaseOptions().parse()
    print(opt)
    prepare_DDSM(os.path.join(ROOT_PATH,opt.srcpath), os.path.join(ROOT_PATH,opt.dstpath),opt.outsize)



