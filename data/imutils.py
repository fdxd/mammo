import matplotlib.pyplot as plt
from skimage.measure import regionprops
import cv2

import numpy as np
def image_crop(mimg, bbox, perc = 0.2 , dstlen = None) :
    y1, x1, y2, x2 = bbox
    cx, cy = round((x2 + x1) / 2), round((y2 + y1) / 2)

    if dstlen is None :
        l = round(max(y2 - y1, x2 - x1) * (1 + perc))
    else :
        l = dstlen

    y1, y2 = round(cy - l / 2), round(cy + l / 2)
    x1, x2 = round(cx - l / 2), round(cx + l / 2)

    ny1, ny2 = max(0, y1) , min(y2, mimg.shape[0])
    nx1, nx2 = max(0, x1) , min(x2, mimg.shape[1])

    ey1, ey2 = ny1 - y1, y2 - ny2
    ex1, ex2 = nx1 - x1, x2 - nx2

    cmimg = mimg[ny1:ny2, nx1:nx2]
    cmimg = np.pad(cmimg, ((ey1, ey2), (ex1, ex2)), mode = 'constant' )

    return cmimg

import numpy as np
from data.preprocessing import image_preprocessing,maskPreprocess

def bigercnt(contours):
  max_area=0
  cnt=[]
  for ii in contours:
    area=cv2.contourArea(ii)
    if area>max_area:
      cnt = ii
  return cnt


from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

def find_flowmap(bimg) :
    tight = cv2.Canny((bimg*255).astype(np.uint8),255,250)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))
    close = cv2.morphologyEx(tight, cv2.MORPH_CLOSE, kernel)
    contours, hierarchy = cv2.findContours( close.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnt = bigercnt(contours)
    cnt = cnt.squeeze(1)
    x = np.expand_dims(cnt[:, 0], axis=1)
    y = np.expand_dims(cnt[:, 1], axis=1)
    x_poly_pred = find_parabola(x, y)
    imshowpts(bimg,opts  = np.concatenate((x,y),axis=1) , pts = np.concatenate((x_poly_pred,y),axis=1))

def find_parabola(x,y):
    polynomial_features = PolynomialFeatures(degree=2)
    y_poly = polynomial_features.fit_transform(y)

    model = LinearRegression()
    model.fit(y_poly, x)
    x_poly_pred = model.predict(y_poly)
    return x_poly_pred


def imshowpts(im,pts=[],figure_flag = True, opts =[],title = '',show_flag = True) :
    if figure_flag :
        plt.figure()
    if im.shape[0] == 3 or im.shape[0] == 1 or im.shape[0] == 4:
        plt.imshow(im.permute(1,2,0))
    else :
        plt.imshow(im)
    if len(pts) != 0:
        plt.scatter(pts[:, 0], pts[:, 1], s=10, marker='.', c='r') # pts[:, 0] => x , pts[:, 1] => y
        plt.scatter(pts[0, 0], pts[0, 1], s=30, marker='*', c='r')
    if len(opts) != 0:
        plt.scatter(opts[:, 0], opts[:, 1], s=10, marker='o', c='g') # pts[:, 0] => x , pts[:, 1] => y
        plt.scatter(pts[0, 0], pts[0, 1], s=30, marker='*', c='g')
    plt.pause(0.001)  # p
    plt.title(title)
    if show_flag :
        plt.show(block=False)