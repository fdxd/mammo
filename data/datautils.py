import os
import pydicom
import json

#from common.mammo_preprocessing import fullMammoPreprocess,maskPreprocess,sumMasks,norm_16b
#import matplotlib.pyplot as plt
import cv2

def get_meta_by_idx(meta,id) :
    for item in meta :
        if item['abnormality id'] == id :
            return item
    return []


def get_data(srcdir,item,outsize = None) :
    mask_path  = [f.path.replace('\\', '/').split('/')[-1] for f in os.scandir(os.path.join(srcdir,item)) if f.is_dir()]

    with open(os.path.join(srcdir,item,'meta.json')) as f:
        meta = json.load(f)

    scan_img = os.path.join(srcdir,item,f'{item}.dcm')
    simg = pydicom.dcmread(scan_img)
    simg = simg.pixel_array
    if outsize is not None :
        simg = cv2.resize(simg, (outsize, outsize))  # cv2 converts dims order

    labels = []
    masks = []
    for mp_dir in mask_path:
        cmeta = get_meta_by_idx(meta, int(mp_dir))
        mp = os.path.join(srcdir,item,mp_dir,f'{item}_MASK.dcm')
        # Read mask(s) .dcm file(s).
        dimg = pydicom.dcmread(mp)
        mimg = dimg.pixel_array
        mimg = cv2.resize(mimg, (simg.shape[1],simg.shape[0])) #cv2 converts dims order
        masks.append(mimg)

        breast_density = cmeta['breast_density'] if 'breast_density' in cmeta.keys() else cmeta['breast density']
        labels.append({ 'pathology': cmeta['pathology'] ,
                        'type' :  cmeta['abnormality type'] ,
                        'abnormality_id' : cmeta['abnormality id'] ,
                        'patient_id' : cmeta['patient_id'],
                        'density' : breast_density,
                        'side' :  cmeta['left or right breast'],
                        'view' :  cmeta['image view']})

    return simg,masks,labels