import argparse
import os
import shutil
import pydicom
import json

#from common.mammo_preprocessing import fullMammoPreprocess,maskPreprocess,sumMasks,norm_16b
#import matplotlib.pyplot as plt
import cv2
import pandas as pd
from skimage.measure import regionprops
from core.crop_utils import image_crop

def get_meta_by_idx(meta,id) :
    for item in meta :
        if item['abnormality id'] == id :
            return item
    return []

def prepare_item(srcdir, dstdir,item,outsize) :
    os.makedirs(os.path.join(dstdir,item), exist_ok=True)
    shutil.copy2(os.path.join(srcdir,item,'meta.json'), os.path.join(dstdir,item,'meta.json'))

    mask_path  = [f.path.replace('\\', '/').split('/')[-1] for f in os.scandir(os.path.join(srcdir,item)) if f.is_dir()]


    with open(os.path.join(srcdir,item,'meta.json')) as f:
        meta = json.load(f)

    scan_img = os.path.join(srcdir,item,f'{item}.dcm')
    simg = pydicom.dcmread(scan_img)
    simg = simg.pixel_array
    label = []
    for mp_dir in mask_path:
        cmeta = get_meta_by_idx(meta, int(mp_dir))
        mp = os.path.join(srcdir,item,mp_dir,f'{item}_MASK.dcm')
        # Read mask(s) .dcm file(s).
        dimg = pydicom.dcmread(mp)
        mimg = dimg.pixel_array
        mimg = cv2.resize(mimg, (simg.shape[1],simg.shape[0])) #cv2 converts dims order

        props = regionprops(mimg)

        # cimg = np.concatenate((np.expand_dims((simg/255).astype('uint8'),axis=2),np.expand_dims((simg/255).astype('uint8'),axis=2),np.expand_dims((mimg/2).astype('uint8'),axis=2)),axis=2)

        for idx,prop in enumerate(props):
            bbox = prop.bbox
        if idx > 1 :
             print('problem')

        csimg = image_crop(simg, bbox, perc=0.3)
        cmimg = image_crop(mimg, bbox, perc=0.3)

        csimg = cv2.resize(csimg, (outsize, outsize))
        cmimg = cv2.resize(cmimg, (outsize, outsize))

        dsimg = image_crop(simg, bbox, dstlen=outsize)
        dmimg = image_crop(mimg, bbox, dstlen=outsize)

        #plt.imshow(dmimg)
        #plt.imshow
        print(f'{mp}========{csimg.shape}-{cmimg.shape}-{dsimg.shape}-{dmimg.shape}')
        cv2.imwrite(os.path.join(dstdir, os.path.join(item, f'{item}_RZCROP_{mp_dir}.png')), csimg)
        cv2.imwrite(os.path.join(dstdir, os.path.join(item, f'{item}_RZMASK_{mp_dir}.png')), cmimg)
        cv2.imwrite(os.path.join(dstdir, os.path.join(item, f'{item}_FXCROP_{mp_dir}.png')), dsimg)
        cv2.imwrite(os.path.join(dstdir, os.path.join(item, f'{item}_FXMASK_{mp_dir}.png')), dmimg)

        breast_density = cmeta['breast_density'] if 'breast_density' in cmeta.keys() else cmeta['breast density']
        label.append({'imgpath' : os.path.join(dstdir, os.path.join(item, f'{item}_RZCROP_{mp_dir}.png')),
                      'pathology': cmeta['pathology'] ,
                      'abnormality_id' : cmeta['abnormality id'] ,
                      'patient_id' : cmeta['patient_id'],
                      'density' : breast_density})

    print(f"DONE ITEM: {os.path.join(srcdir, item)}")

    return label

def prepare_DDSM(srcpath,dstpath,outsize) :
    l = {'Calc' : {'Train': 'calc_case_description_train_set.csv',  'Test': 'calc_case_description_test_set.csv'},
         'Mass': {'Train': 'mass_case_description_train_set.csv' , 'Test': 'mass_case_description_test_set.csv'}}

    os.makedirs(dstpath, exist_ok=True)

    for dstype in  l.keys() :
        for ds in l[dstype].keys() :
            csvfile = os.path.join(dstpath,f'{ds}_{dstype}.csv')
            shutil.copy2(os.path.join(srcpath,l[dstype][ds]), os.path.join(dstpath,l[dstype][ds]))
            srcdir = os.path.join(srcpath, dstype, ds)
            dstdir = os.path.join(dstpath, dstype, ds)
            subfolders = sorted(os.listdir(srcdir))
            os.makedirs(dstdir, exist_ok=True)

            df = pd.DataFrame(columns = ['imgpath', 'pathology', 'abnormality_id', 'patient_id', 'density'])
            for item in subfolders :
                label = prepare_item(srcdir, dstdir, item, outsize)
                for ilabel in label :
                    ilabel['type'] = dstype
                    ilabel['ds'] = ds
                    df = df.append(ilabel,ignore_index=True)

            df.to_csv(csvfile)


class BaseOptions():
    def __init__(self):
        self.initialized = False

    def initialize(self, parser):
        parser.add_argument('--srcpath', default = './workset/CBIS-DDSM/WS01/' , required=True, help='path to images')
        parser.add_argument('--dstpath', default = '/workset/CBIS-DDSM/WS07/512/', required=True, help='path to images')
        parser.add_argument('--outsize', default=256,  type=int, required=True, help='rescaled images size')

        self.initialized = True
        return parser

    def gather_options(self):
        if not self.initialized:  # check if it has been initialized
            parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
            parser = self.initialize(parser)

        # get the basic options
        opt, _ = parser.parse_known_args()

        self.parser = parser
        return parser.parse_args()

    def parse(self):
        """Parse our options, create checkpoints directory suffix, and set up gpu device."""
        opt = self.gather_options()

        self.opt = opt
        return self.opt


if __name__ == "__main__":
    opt = BaseOptions().parse()
    print(opt)
    prepare_DDSM(opt.srcpath, opt.dstpath,opt.outsize)



