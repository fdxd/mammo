from ddsm_train_EXP0400 import *
from options.train_options import TrainOptions

if __name__ == '__main__':
    opts = TrainOptions()
    args = opts.parse()

    args.ensamble_id = 1
    run_fold(args)