import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.metrics import roc_auc_score
import wandb
import os

def main():
	print("testing this cool cluster")

	print("checking the cuda devices")

	print(torch.cuda.is_available())
	print(torch.cuda.device_count())

	g = torch.cuda.current_device()

	print(g)
	print(torch.cuda.device(g))
	print(torch.cuda.get_device_name(g))

	for i in range(50):
		print("at epoch", i)

	f = open("data/work/model_out.txt", "w")
	f.write("testing write")
	f.close()

if __name__ == "__main__":
	raise SystemExit(main())
