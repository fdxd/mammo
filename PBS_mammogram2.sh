#!/bin/bash

#PBS -N NN_mammogram_folds2

# cpu + gpu parallel select (requires MPI)
#PBS -l select=1:ncpus=20:ngpus=1:mem=128gb
#PBS -l walltime=72:00:00

### Queue
#PBS -q RTX

### job logs
#PBS -k eod
#PBS -e log2.err
#PBS -o log2.out

### Mail
#PBS -m abe
#PBS -M daragma.feras@gmail.com

###Always load modules first
module load python/3 gcc hdf5/serial StdEnv

cd $PBS_O_WORKDIR 

### commands 
python3 ddsm_train_EXP0400-2.py --EXP EXP0488 --wandb_mode offline