import argparse
import os
import torch
import json
import shlex

class BaseOptions():
    def __init__(self):
        self.parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        self.initialized = False

    def initialize(self):
        self.parser.add_argument('--EXP',default='None', required=False, help='')
        self.parser.add_argument('--model_type', required=False, help='')
        self.parser.add_argument('--datatype',default='3types', required=False, help='')
        self.parser.add_argument('--dataset',default='single', required=False, help='')
        self.parser.add_argument('--dataname', default='ddsm-WS20', required=False, help='')
        self.parser.add_argument('--pairstype',default='channels', required=False, help='')
        self.parser.add_argument('--use_augmentation', type=int, default=1, help='')
        self.parser.add_argument('--augmentation', default=None, help='')
        self.parser.add_argument('--ensamble_id', type=int, default=0, help='')
        self.parser.add_argument('--optimizer_type', default='SGD', required=False, help='')
        self.parser.add_argument('--scheduler_type', default='LAMBDA', required=False, help='')

        self.initialized = True

    def parse(self,argstr=None):
        if not self.initialized:
            self.initialize()
        if argstr is None :
            self.opt = self.parser.parse_args()
        else :
            self.opt = self.parser.parse_args(shlex.split(argstr))

        args = vars(self.opt)

        print('------------ Options -------------')
        for k, v in sorted(args.items()):
            print('%s: %s' % (str(k), str(v)))
        print('-------------- End ----------------')

        return self.opt

    def dump(self,outpath):

        args = vars(self.opt)
        # save to the disk
        expr_dir = os.path.join(outpath,'opts')
        os.makedirs(expr_dir,exist_ok=True)

        file_name = os.path.join(expr_dir, 'opts.txt')
        jfile_name = os.path.join(expr_dir, 'opts.json')

        with open(file_name, 'wt') as opt_file:
            opt_file.write('------------ Options -------------\n')
            for k, v in sorted(args.items()):
                opt_file.write('%s: %s\n' % (str(k), str(v)))
            opt_file.write('-------------- End ----------------\n')

        with open(jfile_name, 'w') as opt_file:
            json.dump(args, opt_file)

        return self.opt