from options.base_options import BaseOptions

class TrainOptions(BaseOptions):
    def initialize(self):
        BaseOptions.initialize(self)

        self.parser.add_argument('--batch_size', type=int, default=16, help='input batch size')
        self.parser.add_argument('--gpu_ids', type=int, default=[0], help='0 = cpu , 3 = 3 gpus , [0] = gpu device 0 , [0,1,3] = gpus devices 0,1,3')
        self.parser.add_argument('--epochs', type=int, default=400, help='')
        self.parser.add_argument('--num_workers', type=int, default=8, help='')
        self.parser.add_argument('--lr', type=float, default=1e-4, help='')
        self.parser.add_argument('--gamma', type=float, default=0.7, help='')
        self.parser.add_argument('--seed', type=float, default=42, help='')
        self.parser.add_argument('--step_size', type=int, default=5, help='')
        self.parser.add_argument('--name',  type=str, default='ddsm' , help='')
        self.parser.add_argument('--notes', type=str, default='-', help='')
        self.parser.add_argument('--datapath',  type=str, default='workset/CBIS-DDSM/WS07/576' , help='')
        self.parser.add_argument('--workpath', type=str, default='workspace/CBIS-DDSM/WS07/576' , help='')
        self.parser.add_argument('--wandb_mode', type=str, default='disabled', help='')
        self.parser.add_argument('--lr_policy',  type=str, default='lambda' , help='')
        self.parser.add_argument('--niter', type=int, default=50, help='')
        self.parser.add_argument('--niter_decay', type=int, default=50, help='')
        self.parser.add_argument('--begin_epoch', type=int, default=0, help='')
