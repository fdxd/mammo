from __future__ import print_function
import os
from options.train_options import TrainOptions
from core.engine01_test import test_stage, wandb_init

from core.private_settings import DATA_ORIG,WORK_ROOT

import copy

def EXP0400_args(args_,ensamble_id) :
    args = copy.deepcopy(args_)
    args.model_type = 'CrosseiverBiT10'
    args.name = f'EXP0400-{args.model_type}'
    args.job_type = f'fold-{ensamble_id}'

    #data
    args.dataname = 'ddsm-WS20'
    args.dataset  = 'pairs_fullbody'
    args.datapath = 'data_1024'
    args.workpath = 'data_1024'
    args.augmentation = ['vflip', 'crop20']  # 'hflip','vflip','elastic','crop20','gaussiannoise'

    # model
    args.lr_policy = 'lambda'
    args.lr = 1e-3
    args.batch_size = 1
    args.niter = 500
    args.niter_decay = 1500
    args.epochs = args.niter + args.niter_decay
    #config = {'perceivers_layers_num' : 2 , 'num_stages' : 3, 'dropout' : 0.25 , 'query_dim' : 4096}
    config = {'perceivers_layers_num' : 6*1 , 'internal_dim' : 32,  'dropout' : 0.25, 'multi_loss' : True}

    return args,config


def EXP0465_args(args_) :
    args = copy.deepcopy(args_)
    args.model_type = 'CrosseiverBiT02'
    # args.name = f'EXP0465-baseline-noscore-1500-calconly-{args.model_type}'

    args.name = 'EXP0465-baseline-aug-1000-pairs_fullbody_combined-CrosseiverBiT02'
    # EXP0465-baseline-aug-1000-pairs_fullbody_combined-CrosseiverBiT02

    #data
    args.dataname = 'ddsm-WS20'
    args.dataset  = 'pairs_fullbody_combined'
    args.datapath = 'data_combined'
    args.workpath = 'work'
    args.augmentation = ['vflip', 'hflip', 'crop20', 'elastic']  # 'hflip','vflip','elastic','crop20','gaussiannoise'

    # model
    args.lr_policy = 'lambda'
    args.lr = 1e-3
    args.batch_size = 1
    args.niter = 500
    args.niter_decay = 1000
    args.maxab = 10
    args.epochs = args.niter + args.niter_decay
    config = {'perceivers_layers_num' : 6*1 , 'internal_dim' : 32,  'dropout' : 0.25, 'multi_loss' : True, 'maxab':args.maxab, 'interpolation_size':1024}

    return args,config

def EXP0455_args(args_,ensamble_id) :
    args = copy.deepcopy(args_)
    args.model_type = 'CrosseiverBiT02'
    args.name = f'EXP0455-baseline-noscore-1500-fix-{args.model_type}'
    args.job_type = f'fold-{ensamble_id}'

    #data
    args.dataname = 'ddsm-WS20'
    args.dataset  = 'pairs_fullbody'
    args.datapath = 'data_1024'
    args.workpath = 'work'
    args.augmentation = ['vflip', 'crop20']  # 'hflip','vflip','elastic','crop20','gaussiannoise'

    # model
    args.lr_policy = 'lambda'
    args.lr = 1e-3
    args.batch_size = 16
    args.niter = 500
    args.niter_decay = 1000
    args.maxab = 10
    args.epochs = args.niter + args.niter_decay
    config = {'perceivers_layers_num' : 6*1 , 'internal_dim' : 32,  'dropout' : 0.25, 'multi_loss' : True, 'maxab':args.maxab}

    return args,config

def EXP0488_args(args_, ensamble_id=1):
    args = copy.deepcopy(args_)
    args.model_type = "CrosseiverBiT02"
    args.job_type = f"fold-{ensamble_id}"

    # data
    args.dataname = "ddsm-WS20"
    args.dataset = "pairs_fullbody_combined"
    args.datapath = "data_combined"
    args.workpath = "work"
    args.augmentation = [
        "vflip",
        "hflip",
        "crop20",
        "elastic",
    ]  # 'hflip','vflip','elastic','crop20','gaussiannoise'

    # model
    args.lr_policy = "lambda"
    args.lr = 1e-3
    args.batch_size = 1
    args.niter = 500
    args.niter_decay = 500
    args.maxab = 10
    args.epochs = args.niter + args.niter_decay
    args.name = f"EXP0488-aug-meta-1000-pairs_fullbody_combined-CrosseiverBiT02"
    config = {
        "perceivers_layers_num": 6,
        "internal_dim": 32,
        "dropout": 0.25,
        "multi_loss": True,
        "maxab": args.maxab,
        "interpolation_size": 384,
    }

    return args, config


#Custom
def EXP0450_args(args_,ensamble_id) :
    args = copy.deepcopy(args_)
    args.model_type = 'CrosseiverBiT03'
    args.name = f'EXP0450-allzero-{args.model_type}'
    args.job_type = f'fold-{ensamble_id}'

    #data
    args.dataname = 'ddsm-WS20'
    args.dataset  = 'pairs_fullbody'
    args.datapath = 'data_1024'
    args.workpath = 'work'
    args.augmentation = ['vflip', 'crop20']  # 'hflip','vflip','elastic','crop20','gaussiannoise'

    # model
    args.lr_policy = 'lambda'
    args.lr = 1e-3
    args.batch_size = 32
    args.niter = 500
    args.niter_decay = 700
    args.epochs = args.niter + args.niter_decay
    args.maxab = 10
    config = {'perceivers_layers_num' : 6*1 , 'internal_dim' : 32,  'dropout' : 0.25, 'multi_loss' : True, 'maxab':args.maxab}

    return args,config

def EXP0401_args(args_,ensamble_id) :
    args = copy.deepcopy(args_)
    args,config = EXP0400_args(args,ensamble_id)
    args.name = f'EXP0401-{args.model_type}'
    config['internal_dim'] = 16
    config['dropout'] = 0.5

    return args,config

def EXP0402_args(args_,ensamble_id) :
    args = copy.deepcopy(args_)
    args,config = EXP0400_args(args,ensamble_id)
    args.name = f'EXP0402-{args.model_type}'
    config['internal_dim'] = 16
    config['dropout'] = 0.5
    config['max_freq'] = 100.
    config['num_freq_bands'] = 32

    return args,config

def EXP0410_args(args_,ensamble_id) :
    args = copy.deepcopy(args_)
    args,config = EXP0400_args(args,ensamble_id)
    args.name = f'EXP0410-{args.model_type}'

    config['faetures_type'] = 'BitFeatures02'

    return args,config

def EXP0411_args(args_,ensamble_id) :
    args = copy.deepcopy(args_)
    args,config = EXP0400_args(args,ensamble_id)
    args.name = f'EXP0411-{args.model_type}'

    config['faetures_type'] = 'BitFeatures02'
    config['input_channels'] = 32
    config['internal_dim'] = 16
    config['dropout'] = 0.5
    config['max_freq'] = 100.
    config['num_freq_bands'] = 32
    config['ff_mult'] = 1

    return args,config


def EXP0412_args(args_,ensamble_id) :
    args = copy.deepcopy(args_)
    args,config = EXP0400_args(args,ensamble_id)
    args.name = f'EXP0412-{args.model_type}'

    config['faetures_type'] = 'BitFeatures02'
    config['input_channels'] = 32
    config['internal_dim'] = 32
    config['dropout'] = 0.5
    config['max_freq'] = 100.
    config['num_freq_bands'] = 16
    config['ff_mult'] = 2

    return args,config


def ensemble_all_folds(args) :
    ensamble_id = args.ensamble_id

    if args.EXP == 'EXP0400' :
        args, config = EXP0400_args(args,ensamble_id =ensamble_id)
    if args.EXP == 'EXP0401' :
        args, config = EXP0401_args(args,ensamble_id =ensamble_id)
    if args.EXP == 'EXP0402' :
        args, config = EXP0402_args(args,ensamble_id =ensamble_id)
    if args.EXP == 'EXP0410' :
        args, config = EXP0410_args(args,ensamble_id =ensamble_id)
    if args.EXP == 'EXP0411' :
        args, config = EXP0411_args(args,ensamble_id =ensamble_id)
    if args.EXP == 'EXP0412' :
        args, config = EXP0412_args(args,ensamble_id =ensamble_id)
    if args.EXP == 'EXP0450' :
        args, config = EXP0450_args(args,ensamble_id =ensamble_id)
    if args.EXP == 'EXP0455' :
        args, config = EXP0455_args(args,ensamble_id =ensamble_id)
    if args.EXP == 'EXP0465' :
        args, config = EXP0465_args(args)
    if args.EXP == 'EXP0488' :
        args, config = EXP0488_args(args)

    args.datapath = os.path.join(DATA_ORIG, args.datapath)
    args.workpath = os.path.join(WORK_ROOT,args.workpath)

    print(f'datapath : {args.datapath}')
    print(f'workpath : {args.workpath}')

    wandb_run = wandb_init(args,config)

    test_stage(args,device='cuda',config=config)

    wandb_run.finish()

if __name__ == '__main__':
    opts = TrainOptions()
    args = opts.parse()

    args.num_workers = 8

    args.num_models = 5

    ensemble_all_folds(args)
