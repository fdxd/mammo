from __future__ import print_function

import torch
from torch.utils.data import DataLoader
from torchvision import transforms
import os

from core.common import DDSMDataset, get_train_split, seed_everything, DDSMPairsDataset


def get_by_string(slist, x):
    index_list = [i for i in range(len(slist)) if slist[i] == x]
    return index_list

    # [(x, case_id.count(x)) for x in set(case_id)]


def get_pairs(train_list):
    case_id = [f"{item['patient_id']}-{item['side']}" for item in train_list]
    view = [item["view"] for item in train_list]
    pathology = [item["pathology"] for item in train_list]
    case_pairs = sorted([x for x in set(case_id) if case_id.count(x) == 2])
    pairs_list_ = []
    for item in case_pairs:
        index_list = get_by_string(case_id, item)
        assert len(index_list) == 2
        view_list = [view[i] for i in index_list]
        if len(set(view_list)) == 1:
            print(f"{item} - warning")
            continue
        pathology_list = [pathology[i] for i in index_list]
        if len(set(pathology_list)) != 1:
            print(f"{item} - warning")
            continue
        pairs_list_.append(item)

    pairs_list = []
    for pairs in pairs_list_:
        indexes = get_by_string(case_id, pairs)
        pairs_list.append(indexes)
    return pairs_list


def prepare_trainset_pairs(args):
    print(f"Torch: {torch.__version__}")
    print(__file__)
    batch_size = args.batch_size
    seed = args.seed

    seed_everything(seed)
    datapath = args.datapath
    workpath = args.datapath

    import pandas as pd

    df_train_calc = pd.read_csv(os.path.join(datapath, "Train_Calc.csv"))
    df_train_mass = pd.read_csv(os.path.join(datapath, "Train_Mass.csv"))

    train_list = df_train_calc.to_dict("records") + df_train_mass.to_dict("records")

    # In[17]:
    for item in train_list:
        if item["pathology"] == "BENIGN_WITHOUT_CALLBACK":
            item["pathology"] = "BENIGN"

    pairs_list = get_pairs(train_list)

    labels = [train_list[item[0]]["pathology"] for item in pairs_list]
    print(set(labels))

    num_maligant = sum([1 for i in labels if i == "MALIGNANT"])
    num_benign = sum([1 for i in labels if i == "BENIGN"])

    print(f"num_maligant={num_maligant}  , num_benign={num_benign}")
    print(f"Train Data: {len(train_list)}")

    train_pairs, valid_pairs = get_train_split(
        pairs_list,
        test_size=0.2,
        stratify=labels,
        random_state=seed,
        workpath=workpath,
        ensamble_id=args.ensamble_id,
    )

    print(f"Train pairs Data: {len(train_pairs)}")
    print(f"Validation pairs Data: {len(valid_pairs)}")

    # ## Image Augumentation
    if args.use_augmentation:
        train_transforms = transforms.Compose(
            [
                # transforms.Resize((640, 640)),
                transforms.RandomRotation(15, expand=True),
                transforms.CenterCrop(576),
                transforms.RandomCrop(512),
                transforms.RandomHorizontalFlip(),
                transforms.RandomVerticalFlip(),
            ]
        )
    else:
        train_transforms = None

    train_data = DDSMPairsDataset(
        train_list,
        datapath,
        train_pairs,
        transform=train_transforms,
        datatype=args.datatype,
        phase="Train",
        pairstype=args.pairstype,
    )
    valid_data = DDSMPairsDataset(
        train_list,
        datapath,
        valid_pairs,
        transform=train_transforms,
        datatype=args.datatype,
        phase="Train",
        pairstype=args.pairstype,
    )

    train_loader = DataLoader(
        dataset=train_data,
        batch_size=batch_size,
        shuffle=True,
        num_workers=args.num_workers,
    )
    valid_loader = DataLoader(
        dataset=valid_data,
        batch_size=batch_size,
        shuffle=False,
        num_workers=args.num_workers,
    )

    print(len(train_data), len(train_loader))
    print(len(valid_data), len(valid_loader))

    return train_loader, valid_loader


def prepare_trainset(args):
    print(f"Torch: {torch.__version__}")
    print(__file__)
    batch_size = args.batch_size
    seed = args.seed

    seed_everything(seed)
    datapath = args.datapath
    workpath = args.datapath

    import pandas as pd

    df_train_calc = pd.read_csv(os.path.join(datapath, "Train_Calc.csv"))
    df_train_mass = pd.read_csv(os.path.join(datapath, "Train_Mass.csv"))

    train_list = df_train_mass.to_dict("records") + df_train_calc.to_dict("records")

    # In[17]:
    for item in train_list:
        if item["pathology"] == "BENIGN_WITHOUT_CALLBACK":
            item["pathology"] = "BENIGN"

    labels = [item["pathology"] for item in train_list]
    print(set(labels))
    labels = [
        "BENIGN" if item == "BENIGN_WITHOUT_CALLBACK" else item for item in labels
    ]
    print(set(labels))

    num_maligant = sum([1 for i in train_list if i["pathology"] == "MALIGNANT"])
    num_benign = sum([1 for i in train_list if i["pathology"] == "BENIGN"])

    print(f"num_maligant={num_maligant}  , num_benign={num_benign}")
    print(f"Train Data: {len(train_list)}")

    train_list, valid_list = get_train_split(
        train_list,
        test_size=0.2,
        stratify=labels,
        random_state=seed,
        workpath=workpath,
        ensamble_id=args.ensamble_id,
    )

    print(f"Train Data: {len(train_list)}")
    print(f"Validation Data: {len(valid_list)}")

    # ## Image Augumentation
    if args.use_augmentation:
        train_transforms = transforms.Compose(
            [
                # transforms.Resize((640, 640)),
                transforms.RandomRotation(15, expand=True),
                transforms.CenterCrop(576),
                transforms.RandomCrop(512),
                transforms.RandomHorizontalFlip(),
                transforms.RandomVerticalFlip(),
            ]
        )
    else:
        train_transforms = None

    train_data = DDSMDataset(
        train_list, datapath, transform=train_transforms, datatype=args.datatype
    )
    valid_data = DDSMDataset(
        valid_list, datapath, transform=train_transforms, datatype=args.datatype
    )

    train_loader = DataLoader(
        dataset=train_data,
        batch_size=batch_size,
        shuffle=True,
        num_workers=args.num_workers,
    )
    valid_loader = DataLoader(
        dataset=valid_data,
        batch_size=batch_size,
        shuffle=False,
        num_workers=args.num_workers,
    )

    print(len(train_data), len(train_loader))
    print(len(valid_data), len(valid_loader))

    return train_loader, valid_loader


def prepare_testset(args):
    print(f"Torch: {torch.__version__}")
    print(__file__)
    batch_size = args.batch_size
    seed = args.seed

    seed_everything(seed)
    datapath = args.datapath
    workpath = args.datapath

    import pandas as pd

    df_test_calc = pd.read_csv(os.path.join(datapath, "Test_Calc.csv"))
    df_test_mass = pd.read_csv(os.path.join(datapath, "Test_Mass.csv"))

    test_list = df_test_mass.to_dict("records") + df_test_calc.to_dict("records")

    for item in test_list:
        if item["pathology"] == "BENIGN_WITHOUT_CALLBACK":
            item["pathology"] = "BENIGN"

    labels = [item["pathology"] for item in test_list]
    print(set(labels))
    labels = [
        "BENIGN" if item == "BENIGN_WITHOUT_CALLBACK" else item for item in labels
    ]
    print(set(labels))

    num_maligant = sum([1 for i in test_list if i["pathology"] == "MALIGNANT"])
    num_benign = sum([1 for i in test_list if i["pathology"] == "BENIGN"])

    print(f"num_maligant={num_maligant}  , num_benign={num_benign}")
    print(f"Test Data: {len(test_list)}")

    print(f"Test Data: {len(test_list)}")

    # ## Image Augumentation
    if args.use_augmentation:
        test_transforms = transforms.Compose(
            [
                # transforms.Resize((640, 640)),
                transforms.RandomRotation(15, expand=True),
                transforms.CenterCrop(576),
                transforms.RandomCrop(512),
                transforms.RandomHorizontalFlip(),
                transforms.RandomVerticalFlip(),
            ]
        )
    else:
        test_transforms = None

    test_data = DDSMDataset(
        test_list, datapath, transform=test_transforms, datatype=args.datatype
    )

    test_loader = DataLoader(
        dataset=test_data,
        batch_size=batch_size,
        shuffle=False,
        num_workers=args.num_workers,
    )

    print(len(test_data), len(test_loader))

    return test_loader


import numpy as np


def worker_init_fn(w):
    # workers need different random seeds to produce independent samples
    worker_seed = torch.utils.data.get_worker_info().seed % (2**32)
    np.random.seed(worker_seed)


def prepare_trainset_h5py(args):
    from core.ddsm_data import DDSMDataset

    fold_train = [0, 1, 2, 3]
    fold_valid = [args.ensamble_id]
    [fold_train.remove(v) for v in fold_valid]
    fold_test = [4]
    massset = "data/20210219-ddsm-csv-rescale0.5-nyucrop-ds0.5-mass-CC+MLO-subset"
    calcset = "data/20210219-ddsm-csv-rescale0.5-nyucrop-ds0.5-calc-CC+MLO-subset"

    train_data, val_data, test_data = [], [], []
    for fold in fold_train:
        train_data.append(os.path.join(args.datapath, f"{massset}{fold}.h5"))
        train_data.append(os.path.join(args.datapath, f"{calcset}{fold}.h5"))
    for fold in fold_valid:
        val_data.append(os.path.join(args.datapath, f"{massset}{fold}.h5"))
        val_data.append(os.path.join(args.datapath, f"{calcset}{fold}.h5"))
    for fold in fold_test:
        test_data.append(os.path.join(args.datapath, f"{massset}{fold}.h5"))
        test_data.append(os.path.join(args.datapath, f"{calcset}{fold}.h5"))

    if args.use_augmentation:
        augmentation = ["hflip", "vflip", "elastic", "crop20", "gaussiannoise"]
    else:
        augmentation = None
    dtype = torch.float
    ddsm_views = ["mlo", "cc"]
    mb_size = args.batch_size
    datatype = args.datatype

    datasets = {}
    datasets["train"] = [
        DDSMDataset(
            filename,
            augment=augmentation,
            normalize=True,
            dtype_x=dtype,
            dtype_y=dtype,
            views=ddsm_views,
            datatype=datatype,
        )
        for filename in train_data
    ]
    datasets["val"] = [
        DDSMDataset(
            filename,
            augment=[],
            normalize=True,
            dtype_x=dtype,
            dtype_y=dtype,
            views=ddsm_views,
            datatype=datatype,
        )
        for filename in val_data
    ]
    datasets["test"] = [
        DDSMDataset(
            filename,
            augment=[],
            normalize=True,
            dtype_x=dtype,
            dtype_y=dtype,
            views=ddsm_views,
            datatype=datatype,
        )
        for filename in test_data
    ]

    number_of_views = len(ddsm_views)

    # construct loaders

    ds_train = torch.utils.data.ConcatDataset(datasets["train"])
    ds_valid = torch.utils.data.ConcatDataset(datasets["val"])
    ds_test = torch.utils.data.ConcatDataset(datasets["test"])
    train_loader = torch.utils.data.DataLoader(
        ds_train,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    valid_loader = torch.utils.data.DataLoader(
        ds_valid,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    test_loader = torch.utils.data.DataLoader(
        ds_test,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )

    print(f"Dataset Train: {len(ds_train)} samples")
    print(f"Dataset Valid: {len(ds_valid)} samples")
    print(f"Dataset Test : {len(ds_test)} samples")

    return train_loader, valid_loader, test_loader


import pickle


def get_datalist_combined(datapath, dataname, fold, datatype="pairs"):
    with open(
        os.path.join(datapath, f"{dataname}-mass_{datatype}_fold_{fold:02}.pkl"), "rb"
    ) as file:
        mass = pickle.load(file)
    with open(
        os.path.join(datapath, f"{dataname}-calc_{datatype}_fold_{fold:02}.pkl"), "rb"
    ) as file:
        calc = pickle.load(file)
    return mass, calc


def get_datalist(datapath, dataname, phase, fold, datatype="pairs"):
    with open(
        os.path.join(
            datapath, f"{dataname}-mass-{phase}_{datatype}_fold_{fold:02}.pkl"
        ),
        "rb",
    ) as file:
        mass = pickle.load(file)
    with open(
        os.path.join(
            datapath, f"{dataname}-calc-{phase}_{datatype}_fold_{fold:02}.pkl"
        ),
        "rb",
    ) as file:
        calc = pickle.load(file)
    return mass, calc


"""
with open(os.path.join(args.datapath,f'{dataname}-calc-train_pairs_fold_{args.ensamble_id:02}.pkl'), 'rb') as file:
    calc = pickle.load(file)
with open(os.path.join(args.datapath,f'{dataname}-mass-train_pairs_fold_{args.ensamble_id:02}.pkl'), 'rb') as file:
    mass = pickle.load(file)

"""


def prepare_trainset_pairs_mass_combined(args):
    from core.ddsm_data import DDSMDataset3

    dataname = args.dataname

    mass, calc = get_datalist_combined(args.datapath, dataname, args.ensamble_id)
    train_list = mass["train_list"]
    valid_list = mass["valid_list"]
    test_list = mass["data_list"]

    data = {}
    for item in ["mass"]:
        for phase in ["train", "test"]:
            data[f"{item}_{phase}"] = os.path.join(
                args.datapath, f"{dataname}-{item}-{phase}.h5"
            )

    if args.use_augmentation:
        augmentation = args.augmentation
        # augmentation = ['vflip','crop20'] #'hflip','vflip','elastic','crop20','gaussiannoise'
    else:
        augmentation = None

    dtype = torch.float
    ddsm_views = ["mlo", "cc"]
    mb_size = args.batch_size
    datatype = args.datatype

    ds_train = DDSMDataset3(
        data,
        train_list,
        maxab=args.maxab,
        augment=augmentation,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )
    ds_valid = DDSMDataset3(
        data,
        valid_list,
        maxab=args.maxab,
        augment=augmentation,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )
    ds_test = DDSMDataset3(
        data,
        test_list,
        maxab=args.maxab,
        augment=None,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )

    number_of_views = len(ddsm_views)

    train_loader = torch.utils.data.DataLoader(
        ds_train,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    valid_loader = torch.utils.data.DataLoader(
        ds_valid,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    test_loader = torch.utils.data.DataLoader(
        ds_test,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )

    print(f"Dataset Train: {len(ds_train)} samples")
    print(f"Dataset Valid: {len(ds_valid)} samples")
    print(f"Dataset Test : {len(ds_test)} samples")

    return train_loader, valid_loader, test_loader


def prepare_trainset_pairs_calc_combined(args):
    from core.ddsm_data import DDSMDataset3

    dataname = args.dataname

    mass, calc = get_datalist_combined(args.datapath, dataname, args.ensamble_id)
    train_list = calc["train_list"]
    valid_list = calc["valid_list"]
    test_list = calc["data_list"]

    data = {}
    for item in ["calc"]:
        for phase in ["train", "test"]:
            data[f"{item}_{phase}"] = os.path.join(
                args.datapath, f"{dataname}-{item}-{phase}.h5"
            )

    if args.use_augmentation:
        augmentation = args.augmentation
        # augmentation = ['vflip','crop20'] #'hflip','vflip','elastic','crop20','gaussiannoise'
    else:
        augmentation = None

    dtype = torch.float
    ddsm_views = ["mlo", "cc"]
    mb_size = args.batch_size
    datatype = args.datatype

    ds_train = DDSMDataset3(
        data,
        train_list,
        maxab=args.maxab,
        augment=augmentation,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )
    ds_valid = DDSMDataset3(
        data,
        valid_list,
        maxab=args.maxab,
        augment=augmentation,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )
    ds_test = DDSMDataset3(
        data,
        test_list,
        maxab=args.maxab,
        augment=None,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )

    number_of_views = len(ddsm_views)

    train_loader = torch.utils.data.DataLoader(
        ds_train,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    valid_loader = torch.utils.data.DataLoader(
        ds_valid,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    test_loader = torch.utils.data.DataLoader(
        ds_test,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )

    print(f"Dataset Train: {len(ds_train)} samples")
    print(f"Dataset Valid: {len(ds_valid)} samples")
    print(f"Dataset Test : {len(ds_test)} samples")

    return train_loader, valid_loader, test_loader


def prepare_trainset_pairs_fullbody_combined(args):
    from core.ddsm_data import DDSMDataset3
    #from core.ddsm_data import DDSMDataset4

    dataname = args.dataname

    mass, calc = get_datalist_combined(args.datapath, dataname, args.ensamble_id)
    train_list = calc["train_list"] + mass["train_list"]
    valid_list = calc["valid_list"] + mass["valid_list"]
    test_list = calc["data_list"] + mass["data_list"]

    data = {}
    for item in ["calc", "mass"]:
        for phase in ["train", "test"]:
            data[f"{item}_{phase}"] = os.path.join(
                args.datapath, f"{dataname}-{item}-{phase}.h5"
            )

    if args.use_augmentation:
        augmentation = args.augmentation
    else:
        augmentation = None

    dtype = torch.float
    ddsm_views = ["mlo", "cc"]
    mb_size = args.batch_size
    datatype = args.datatype

    ds_train = DDSMDataset3(
        data,
        train_list,
        maxab=args.maxab,
        augment=augmentation,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )
    ds_valid = DDSMDataset3(
        data,
        valid_list,
        maxab=args.maxab,
        augment=augmentation,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )
    ds_test = DDSMDataset3(
        data,
        test_list,
        maxab=args.maxab,
        augment=None,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )

    train_loader = torch.utils.data.DataLoader(
        ds_train,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    valid_loader = torch.utils.data.DataLoader(
        ds_valid,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    test_loader = torch.utils.data.DataLoader(
        ds_test,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )

    print(f"Dataset Train: {len(ds_train)} samples")
    print(f"Dataset Valid: {len(ds_valid)} samples")
    print(f"Dataset Test : {len(ds_test)} samples")

    # for data, label in train_loader:
    #     print(data)
    #     print(label)

    return train_loader, valid_loader, test_loader


def prepare_trainset_pairs_fullbody(args):
    from core.ddsm_data import DDSMDataset2

    dataname = args.dataname

    mass, calc = get_datalist(args.datapath, dataname, "train", args.ensamble_id)
    train_list = calc["train_list"] + mass["train_list"]
    valid_list = calc["valid_list"] + mass["valid_list"]

    mass, calc = get_datalist(args.datapath, dataname, "test", 0)
    test_list = calc["data_list"] + mass["data_list"]

    train_data = {}
    for item in ["calc", "mass"]:
        train_data[item] = os.path.join(args.datapath, f"{dataname}-{item}-train.h5")

    test_data = {}
    for item in ["calc", "mass"]:
        test_data[item] = os.path.join(args.datapath, f"{dataname}-{item}-test.h5")

    if args.use_augmentation:
        augmentation = args.augmentation
        # augmentation = ['vflip','crop20'] #'hflip','vflip','elastic','crop20','gaussiannoise'
    else:
        augmentation = None

    dtype = torch.float
    ddsm_views = ["mlo", "cc"]
    mb_size = args.batch_size
    datatype = args.datatype

    ds_train = DDSMDataset2(
        train_data,
        train_list,
        maxab=args.maxab,
        augment=augmentation,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )
    ds_valid = DDSMDataset2(
        train_data,
        valid_list,
        maxab=args.maxab,
        augment=augmentation,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )
    ds_test = DDSMDataset2(
        test_data,
        test_list,
        maxab=args.maxab,
        augment=None,
        normalize=True,
        dtype_x=dtype,
        dtype_y=dtype,
        views=ddsm_views,
        datatype=datatype,
    )

    number_of_views = len(ddsm_views)

    train_loader = torch.utils.data.DataLoader(
        ds_train,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    valid_loader = torch.utils.data.DataLoader(
        ds_valid,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )
    test_loader = torch.utils.data.DataLoader(
        ds_test,
        batch_size=mb_size,
        shuffle=True,
        num_workers=args.num_workers,
        worker_init_fn=worker_init_fn,
        pin_memory=True,
    )

    print(f"Dataset Train: {len(ds_train)} samples")
    print(f"Dataset Valid: {len(ds_valid)} samples")
    print(f"Dataset Test : {len(ds_test)} samples")

    return train_loader, valid_loader, test_loader
