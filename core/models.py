
from __future__ import print_function

import torch.nn as nn

import torchvision.models as models
import numpy as np
# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)
from vit_pytorch.vit import ViT
from vit_pytorch.vitsa import ViTsa
from pytorch_pretrained_vit import ViT as preViT
import torch.nn.functional as F

from torchvision import models
import bit_pytorch.models as bit_models

from torchsummary import summary
#from pytorch_model_summary import summary

import torch

from core.models_lib import VGGVit,VGGVit02,BitVit03,BitVit02
from core.common import print_model_grads,create_partial_model,set_requires_grad,reset_grads
from core.common import Meausres,get_scheduler


class VitAndBit03(nn.Module):
    def __init__(self,config):
        super(VitAndBit03, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']

        self.vit_model = preViT('B_16_imagenet1k', pretrained=True)
        self.vit_model.fc =  nn.Linear(in_features=768, out_features=2, bias=True)

        self.bit_model = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
        weights = np.load(pretrained_model)
        self.bit_model.load_from(weights)

        print_model_grads(self.vit_model)
        print_model_grads(self.bit_model)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        if self.freeze_mode ==  'FC_only' :
            set_requires_grad(self, 'vit_model.norm', True)
            set_requires_grad(self, 'vit_model.fc', True)
            set_requires_grad(self, 'bit_model.head', True)
        elif self.freeze_mode == 'LastBlock':
            set_requires_grad(self, 'vit_model.transformer.blocks.11', True)
            set_requires_grad(self, 'vit_model.norm', True)
            set_requires_grad(self, 'vit_model.fc', True)

            set_requires_grad(self, 'bit_model.body.block4.unit03', True)
            set_requires_grad(self, 'bit_model.head', True)

        print_model_grads(self)

        for item in self.named_modules() :
            print(item)

        layers = ['bit_model.head.avg','vit_model.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))

        summary(self.vit_model, (3, 384, 384), device='cpu')
        summary(self.bit_model, (3, 384, 384), device='cpu')

        bit_features = self._features['bit_model.head.avg'].squeeze(2).squeeze(2)
        vit_features = self._features['vit_model.norm'][:, 0]

        last_fc_size = bit_features.shape[1] + vit_features.shape[1]
        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        self.loss_fn = nn.CrossEntropyLoss()

        if self.alternative_mode :
            self.count = 0
            self.step_size = self.alternative_step_size
            self.block_count = 0
            self.base_block = 'vit_model.transformer.blocks.11.attn'
            self.alternative_blocks = ['vit_model.transformer.blocks.11.attn.proj_q',
                                       'vit_model.transformer.blocks.11.attn.proj_k',
                                       'vit_model.transformer.blocks.11.attn.proj_v']


    def alternative_step(self):
        self.count = self.count + 1
        if self.count == self.step_size :
            self.count = 0
            set_requires_grad(self, self.base_block, False)
            set_requires_grad(self, self.alternative_blocks[self.block_count], True)
            self.block_count = self.block_count + 1
            if self.block_count == len(self.alternative_blocks) :
                self.block_count = 0



    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def criterion(self,output,label):
        loss_merge = self.loss_fn(output['output'],label)
        loss_vit = self.loss_fn(output['out_vit'], label)
        loss_bit = self.loss_fn(output['out_bit'], label)
        loss = loss_bit+loss_vit+loss_merge
        return loss

    def forward(self, x):
        if self.alternative_mode:
            self.alternative_step()
        x = F.interpolate(x, (384, 384), mode='bicubic',align_corners=True)
        out_vit = self.vit_model(x)
        out_bit = self.bit_model(x)

        bit_features = self._features['bit_model.head.avg'].squeeze(2).squeeze(2)
        vit_features = self._features['vit_model.norm'][:,0]

        features = torch.cat((bit_features,vit_features),dim=1)

        features = self.norm(features)
        out_merge = self.classifier(features)

        return {'output': out_merge , 'out_vit':out_vit,'out_bit':out_bit}

class VitAndBit04(nn.Module):
    def __init__(self,config):
        super(VitAndBit04, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']

        self.vit_model = preViT('B_16_imagenet1k', pretrained=True)
        self.vit_model.fc =  nn.Linear(in_features=768, out_features=2, bias=True)

        self.bit_model = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
        weights = np.load(pretrained_model)
        self.bit_model.load_from(weights)

        print_model_grads(self.vit_model)
        print_model_grads(self.bit_model)
        self.head = nn.Sequential(
            nn.Conv2d(3, 3, kernel_size= (3, 3),padding=1),
            nn.ReLU(),
            nn.Conv2d(3, 3, kernel_size= (3, 3) ,stride=2,padding=1),
            nn.ReLU()
        )

        print_model_grads(self)
        set_requires_grad(self, '', False)
        if self.freeze_mode ==  'FC_only' :
            set_requires_grad(self, 'vit_model.norm', True)
            set_requires_grad(self, 'vit_model.fc', True)
            set_requires_grad(self, 'bit_model.head', True)
        elif self.freeze_mode == 'LastBlock':
            set_requires_grad(self, 'vit_model.transformer.blocks.11', True)
            set_requires_grad(self, 'vit_model.norm', True)
            set_requires_grad(self, 'vit_model.fc', True)

            set_requires_grad(self, 'bit_model.body.block4.unit03', True)
            set_requires_grad(self, 'bit_model.head', True)

        set_requires_grad(self, 'head', True)

        print_model_grads(self)

        for item in self.named_modules() :
            print(item)

        layers = ['bit_model.head.avg','vit_model.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))

        summary(self.vit_model, (3, 384, 384), device='cpu')
        summary(self.bit_model, (3, 384, 384), device='cpu')

        bit_features = self._features['bit_model.head.avg'].squeeze(2).squeeze(2)
        vit_features = self._features['vit_model.norm'][:, 0]

        last_fc_size = bit_features.shape[1] + vit_features.shape[1]
        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        self.loss_fn = nn.CrossEntropyLoss()

        if self.alternative_mode :
            self.count = 0
            self.step_size = self.alternative_step_size
            self.block_count = 0
            self.base_block = 'vit_model.transformer.blocks.11.attn'
            self.alternative_blocks = ['vit_model.transformer.blocks.11.attn.proj_q',
                                       'vit_model.transformer.blocks.11.attn.proj_k',
                                       'vit_model.transformer.blocks.11.attn.proj_v']


    def alternative_step(self):
        self.count = self.count + 1
        if self.count == self.step_size :
            self.count = 0
            set_requires_grad(self, self.base_block, False)
            set_requires_grad(self, self.alternative_blocks[self.block_count], True)
            self.block_count = self.block_count + 1
            if self.block_count == len(self.alternative_blocks) :
                self.block_count = 0



    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def criterion(self,output,label):
        loss_merge = self.loss_fn(output['output'],label)
        loss_vit = self.loss_fn(output['out_vit'], label)
        loss_bit = self.loss_fn(output['out_bit'], label)
        loss = loss_bit+loss_vit+loss_merge
        return loss

    def forward(self, x):
        if self.alternative_mode:
            self.alternative_step()
        x = F.interpolate(x, (384*2, 384*2), mode='bicubic',align_corners=True)
        x = self.head(x)
        out_vit = self.vit_model(x)
        out_bit = self.bit_model(x)

        bit_features = self._features['bit_model.head.avg'].squeeze(2).squeeze(2)
        vit_features = self._features['vit_model.norm'][:,0]

        features = torch.cat((bit_features,vit_features),dim=1)

        features = self.norm(features)
        out_merge = self.classifier(features)

        return {'output': out_merge , 'out_vit':out_vit,'out_bit':out_bit}



def get_model(model_type,config={}) :
    from core.models_lib import  VitPre01,VitAndBit01,VitAndBit02
    if model_type == 'Perceiver01' :
        from core.models_perceivers import Perceiver01
        model = Perceiver01(config)
    if model_type == 'Perceiver02' :
        from core.models_perceivers import Perceiver02
        model = Perceiver02(config)
    if model_type == 'Perceiver03' :
        from core.models_perceivers import Perceiver03
        model = Perceiver03(config)

    if model_type == 'Perceiver04' :
        from core.models_perceivers import Perceiver04
        model = Perceiver04(config)
    if model_type == 'Perceiver05' :
        from core.models_perceivers import Perceiver05
        model = Perceiver05(config)
    if model_type == 'Perceiver06' :
        from core.models_perceivers import Perceiver06
        model = Perceiver06(config)

    if model_type == 'Crosseiver01' :
        from core.models_crosseivers import Crosseiver01
        model = Crosseiver01(config)
    if model_type == 'Crosseiver02' :
        from core.models_crosseivers import Crosseiver02
        model = Crosseiver02(config)
    if model_type == 'Crosseiver03' :
        from core.models_crosseivers import Crosseiver03
        model = Crosseiver03(config)
    if model_type == 'Crosseiver04' :
        from core.models_crosseivers import Crosseiver04
        model = Crosseiver04(config)
    if model_type == 'Crosseiver05' :
        from core.models_crosseivers import Crosseiver05
        model = Crosseiver05(config)
    if model_type == 'Crosseiver06' :
        from core.models_crosseivers import Crosseiver06
        model = Crosseiver06(config)

    if model_type == 'CrosseiverBiT01' :
        from core.models_crosseivers import CrosseiverBiT01
        model = CrosseiverBiT01(config)
    if model_type == 'CrosseiverBiT02' : #custom
        from core.models_crosseivers import CrosseiverBiT02
        model = CrosseiverBiT02(config)

    if model_type == 'CrosseiverBiT03' :
        from core.models_crosseivers import CrosseiverBiT03
        model = CrosseiverBiT03(config)

    if model_type == 'CrosseiverBiT10' :
        from core.models_crosseivers import CrosseiverBiT10
        model = CrosseiverBiT10(config)


    if model_type == 'BITPAIRS_01' :
        from core.models_bitpairs import BitPairs01
        model = BitPairs01(config)
    if model_type == 'VITPAIRS_01' :
        from core.models_vitpairs import VitPairs01
        model = VitPairs01(config)
    if model_type == 'VITPAIRS_01A' :
        from core.models_vitpairs import VitPairs01A
        model = VitPairs01A(config)
    if model_type == 'VITPAIRS_02':
        from core.models_vitpairs import VitPairs02
        model = VitPairs02(config)
    if model_type == 'VITPAIRS_02A':
        from core.models_vitpairs import VitPairs02A
        model = VitPairs02A(config)
    if model_type == 'VITPAIRS_03':
        from core.models_vitpairs import VitPairs03
        model = VitPairs03(config)
    if model_type == 'VITPAIRS_04':
        from core.models_vitpairs import VitPairs04
        model = VitPairs04(config)
    if model_type == 'VITPAIRS_05':
        from core.models_vitpairs import VitPairs05
        model = VitPairs05(config)
    if model_type == 'VITPAIRS_06':
        from core.models_vitpairs import VitPairs06
        model = VitPairs06(config)

    if model_type == 'VIT&BIT_04' :
        model = VitAndBit04(config)
    if model_type == 'VITPRE_01' :
        model = VitPre01(config)
    if model_type == 'VIT&BIT_01' :
        model = VitAndBit01(config)
    if model_type == 'VIT&BIT_02' :
        model = VitAndBit02(config)
    if model_type == 'VIT&BIT_03' :
        model = VitAndBit03(config)
    if model_type == 'VGGVIT_01' :
        model = VGGVit()
    elif model_type == 'VGGVIT_02':
        model = VGGVit02()
    elif model_type == 'BITVIT50_02':
        model = BitVit02(config)
    elif model_type == 'BITVIT50_03':
        model = BitVit03(config)

    return model

def get_vgg16_vit_model_01(reset_params = False) :
    model = VGGVit()

    return model

# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)
def get_vgg16_model_01(reset_params = False) :
    model = models.vgg16(pretrained=True)
    model.classifier[6] = nn.Linear(in_features=model.classifier[6].in_features, out_features=2, bias=True)
    print('Before requires_grad habdling \n')
    for name, param in model.named_parameters():
        print(f'{param.requires_grad} \t - \t {name} \t - \t {param.shape}')

    for name,param in model.features.named_parameters():
        param.requires_grad = False

    for i in [26,28] :
        for name,param in model.features[i].named_parameters():
            param.requires_grad = False

    for name,param in model.classifier.named_parameters():
        param.requires_grad = True

    print('\nAfter requires_grad habdling \n')
    for name, param in model.named_parameters():
        print(f'{param.requires_grad} \t - \t {name} \t - \t {param.shape}')

    return model
# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)

def get_bit_model_01(reset_params = False) :
    model = models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
    pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
    weights = np.load(pretrained_model)
    model.load_from(weights)

    for name, param in model.named_parameters():
        print(f'{param.requires_grad} \t - \t {name}')

    for name,param in model.root.named_parameters():
        param.requires_grad = True

    for name,param in model.body.named_parameters():
        param.requires_grad = False
        if name.split('.')[0] in ['block4'] :
            param.requires_grad = True
        else :
            param.requires_grad = False

    for name,param in model.head.named_parameters():
        param.requires_grad = True


    if reset_params :
        for layer in model.root.children():
            if hasattr(layer, 'reset_parameters'):
                layer.reset_parameters()

        for layer in model.head.children():
            if hasattr(layer, 'reset_parameters'):
                layer.reset_parameters()


    for name, param in model.named_parameters():
        print(f'{param.requires_grad} \t - \t {name}')

    return model
