
from __future__ import print_function

import torch.nn as nn

import torchvision.models as models
import numpy as np
# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)
from vit_pytorch.vit import ViT
from vit_pytorch.vitsa import ViTsa
from pytorch_pretrained_vit import ViT as preViT
import torch.nn.functional as F


from torchvision import models
import bit_pytorch.models as bit_models

from torchsummary import summary
#from pytorch_model_summary import summary
from core.common import print_model_grads,create_partial_model,set_requires_grad,reset_grads,remove_blocks_from_model

import torch

class VitPre01(nn.Module):
    def __init__(self,config):
        super(VitPre01, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']

        self.model = preViT('B_16_imagenet1k', pretrained=True)
        self.model.fc =  nn.Linear(in_features=768, out_features=2, bias=True)
        print_model_grads(self.model)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        if self.freeze_mode ==  'FC_only' :
            set_requires_grad(self, 'model.norm', True)
            set_requires_grad(self, 'model.fc', True)
        elif self.freeze_mode == 'LastBlock':
            set_requires_grad(self, 'model.transformer.blocks.11', True)
            set_requires_grad(self, 'model.transformer.blocks.11.attn', False)

            set_requires_grad(self, 'model.norm', True)
            set_requires_grad(self, 'model.fc', True)

        print_model_grads(self)

        summary(self.model, ( 3, 384, 384), device='cpu')

        if self.alternative_mode :
            self.count = 0
            self.step_size = self.alternative_step_size
            self.block_count = 0
            self.base_block = 'model.transformer.blocks.11.attn'
            self.alternative_blocks = ['model.transformer.blocks.11.attn.proj_q',
                                       'model.transformer.blocks.11.attn.proj_k',
                                       'model.transformer.blocks.11.attn.proj_v']


    def alternative_step(self):
        self.count = self.count + 1
        if self.count == self.step_size :
            self.count = 0
            set_requires_grad(self, self.base_block, False)
            set_requires_grad(self, self.alternative_blocks[self.block_count], True)
            self.block_count = self.block_count + 1
            if self.block_count == len(self.alternative_blocks) :
                self.block_count = 0

    def forward(self, x):
        if self.alternative_mode :
            self.alternative_step()

        x = F.interpolate(x, (384, 384), mode='bicubic',align_corners=True)
        out = self.model(x)
        return out



class VitAndBit01(nn.Module):
    def __init__(self,config):
        super(VitAndBit01, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step = 1 if 'alternative_step' not in config.keys() else config['alternative_step']

        self.vit_model = preViT('B_16_imagenet1k', pretrained=True)
        self.vit_model.fc =  nn.Linear(in_features=768, out_features=2, bias=True)

        self.bit_model = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
        weights = np.load(pretrained_model)
        self.bit_model.load_from(weights)

        print_model_grads(self.vit_model)
        print_model_grads(self.bit_model)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        if self.freeze_mode ==  'FC_only' :
            set_requires_grad(self, 'vit_model.norm', True)
            set_requires_grad(self, 'vit_model.fc', True)
            set_requires_grad(self, 'bit_model.head', True)
        elif self.freeze_mode == 'LastBlock':
            set_requires_grad(self, 'vit_model.transformer.blocks.11', True)
            set_requires_grad(self, 'vit_model.norm', True)
            set_requires_grad(self, 'vit_model.fc', True)

            set_requires_grad(self, 'bit_model.body.block4.unit03', True)
            set_requires_grad(self, 'bit_model.head', True)

        print_model_grads(self)

        summary(self.vit_model, (3, 384, 384), device='cpu')
        summary(self.bit_model, (3, 384, 384), device='cpu')

        self.classifier = nn.Linear(in_features=2048+768, out_features=2, bias=True)

    def forward(self, x):

        x = F.interpolate(x, (384, 384), mode='bicubic',align_corners=True)
        out_vit = self.vit_model(x)
        out_bit = self.bit_model(x)
        out = (out_vit + out_bit)/2
        return out

class VitAndBit02(nn.Module):
    def __init__(self,config):
        super(VitAndBit02, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']

        self.vit_model = preViT('B_16_imagenet1k', pretrained=True)
        self.vit_model.fc =  nn.Linear(in_features=768, out_features=2, bias=True)

        self.bit_model = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
        weights = np.load(pretrained_model)
        self.bit_model.load_from(weights)

        print_model_grads(self.vit_model)
        print_model_grads(self.bit_model)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        if self.freeze_mode ==  'FC_only' :
            set_requires_grad(self, 'vit_model.norm', True)
            set_requires_grad(self, 'vit_model.fc', True)
            set_requires_grad(self, 'bit_model.head', True)
        elif self.freeze_mode == 'LastBlock':
            set_requires_grad(self, 'vit_model.transformer.blocks.11', True)
            set_requires_grad(self, 'vit_model.norm', True)
            set_requires_grad(self, 'vit_model.fc', True)

            set_requires_grad(self, 'bit_model.body.block4.unit03', True)
            set_requires_grad(self, 'bit_model.head', True)

        print_model_grads(self)

        for item in self.named_modules() :
            print(item)

        layers = ['bit_model.head.avg','vit_model.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))

        summary(self.vit_model, (3, 384, 384), device='cpu')
        summary(self.bit_model, (3, 384, 384), device='cpu')

        bit_features = self._features['bit_model.head.avg'].squeeze(2).squeeze(2)
        vit_features = self._features['vit_model.norm'][:, 0]

        last_fc_size = bit_features.shape[1] + vit_features.shape[1]
        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        self.loss_fn = nn.CrossEntropyLoss()

    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def criterion(self,output,label):
        loss_merge = self.loss_fn(output['output'],label)
        loss_vit = self.loss_fn(output['out_vit'], label)
        loss_bit = self.loss_fn(output['out_bit'], label)
        loss = loss_bit+loss_vit+loss_merge
        return loss

    def forward(self, x):

        x = F.interpolate(x, (384, 384), mode='bicubic',align_corners=True)
        out_vit = self.vit_model(x)
        out_bit = self.bit_model(x)

        bit_features = self._features['bit_model.head.avg'].squeeze(2).squeeze(2)
        vit_features = self._features['vit_model.norm'][:,0]

        features = torch.cat((bit_features,vit_features),dim=1)

        features = self.norm(features)
        out_merge = self.classifier(features)

        return {'output': out_merge , 'out_vit':out_vit,'out_bit':out_bit}


class VGGVit(nn.Module):
    def __init__(self):
        super(VGGVit, self).__init__()
        features = models.vgg16(pretrained=True).features
        self.features = nn.Sequential(*list(features.children())[:-1])

        self.classifier = ViT(
            image_size=32,
            patch_size=2,
            num_classes=2,
            channels=512,
            dim=768,
            depth=4,
            heads=16,
            mlp_dim=2048,
            dropout=0.1,
            emb_dropout=0.1)

        summary(self.features, ( 3, 512, 512), device='cpu')
        summary(self.classifier,( 512, 32, 32), device='cpu')
        summary(self, ( 3, 512, 512), device='cpu')

        #summary(self.features, torch.zeros(1, 3, 512, 512), show_input=True, print_summary=True)
        #summary(self.classifier, torch.zeros(1, 512, 32, 32))

        print('Before requires_grad habdling \n')
        for name, param in self.features.named_parameters():
            print(f'{param.requires_grad} \t - \t {name} \t - \t {param.shape}')

        for name, param in self.features.named_parameters():
            param.requires_grad = False

        for i in [26, 28]:
            for name, param in self.features[i].named_parameters():
                param.requires_grad = True

        for name, param in self.classifier.named_parameters():
            param.requires_grad = True

        print('\nAfter requires_grad habdling \n')
        for name, param in self.named_parameters():
            print(f'{param.requires_grad} \t - \t {name} \t - \t {param.shape}')


    def forward(self, x):
        x = self.features(x)
        x = self.classifier(x)
        return x



class VGGVit02(nn.Module):
    def __init__(self):
        super(VGGVit02, self).__init__()

        vgg = models.vgg16(pretrained=True)
        self.features = nn.Sequential(*list(vgg.features.children())[:-3])

        self.backbone = nn.Sequential(*list(vgg.features.children())[-3:])
        self.avgpool = nn.Sequential(vgg.avgpool,nn.Flatten())
        self.classifier = vgg.classifier
        self.classifier[6] = nn.Linear(in_features=self.classifier[6].in_features, out_features=2, bias=True)

        self.attention = ViTsa(
            image_size=32,
            patch_size=2,
            num_classes=2,
            channels=512,
            dim=768,
            depth=4,
            heads=16,
            mlp_dim=2048,
            dropout=0.1,
            emb_dropout=0.1)

        summary(self.features, ( 3, 512, 512), device='cpu')
        summary(self.attention,( 512, 32, 32), device='cpu')
        #summary(self, ( 3, 512, 512), device='cpu')

        #summary(self.features, torch.zeros(1, 3, 512, 512), show_input=True, print_summary=True)
        #summary(self.classifier, torch.zeros(1, 512, 32, 32))

        print('Before requires_grad habdling \n')
        for name, param in self.features.named_parameters():
            print(f'{param.requires_grad} \t - \t {name} \t - \t {param.shape}')

        for name, param in self.features.named_parameters():
            param.requires_grad = False

        for name, param in self.attention.named_parameters():
            param.requires_grad = False

        for name, param in self.backbone.named_parameters():
            param.requires_grad = True

        for name, param in self.avgpool.named_parameters():
            param.requires_grad = True

        for name, param in self.classifier.named_parameters():
            param.requires_grad = True

        print('\nAfter requires_grad habdling \n')
        for name, param in self.named_parameters():
            print(f'{param.requires_grad} \t - \t {name} \t - \t {param.shape}')

        self.use_attention = False

    def forward(self, x):
        x = self.features(x)

        if self.use_attention :
            x = self.attention(x)

        x = self.backbone(x)
        x = self.avgpool(x)
        x = self.classifier(x)
        return x


class BitVit03(nn.Module):
    def __init__(self,config):
        super(BitVit03, self).__init__()

        self.phase = 'trainBIT' if 'phase' not in config.keys() else config['phase']

        model = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
        weights = np.load(pretrained_model)
        model.load_from(weights)

        self.root = model.root
        self.body1 = create_partial_model(model.body, ['block1', 'block2', 'block3'])
        self.body2 = create_partial_model(model.body, ['block4'])
        self.head = model.head

        self.attention = ViTsa(image_size=32,patch_size=2,num_classes=2,channels=1024,dim=768,
                                depth=2, heads=16, mlp_dim=2048, dropout=0.5, emb_dropout=0.5)

        print_model_grads(self)
        if self.phase == 'trainBIT' :
            self.use_attention = False
            set_requires_grad(self, '', False)
            set_requires_grad(self, 'body2.block4.unit03', True)
            set_requires_grad(self, 'head', True)
            reset_grads(self, 'head')
        elif  self.phase == 'trainVIT' :
            self.use_attention = True
            set_requires_grad(self, '', False)
            set_requires_grad(self, 'attention', True)
        print_model_grads(self)

        summary(self.root, ( 3, 512, 512), device='cpu')
        summary(self.body1, ( 64, 128, 128), device='cpu')
        summary(self.attention, (1024, 32, 32), device='cpu')
        summary(self.body2, ( 1024, 32, 32), device='cpu')
        summary(self.head, ( 2048, 16, 16), device='cpu')

    def forward(self, x):
        x = self.root(x)
        x = self.body1(x)

        if self.use_attention :
            x = self.attention(x)

        x = self.body2(x)
        x = self.head(x)
        x = x.squeeze(3).squeeze(2)
        return x

class BitVit02(nn.Module):
    def __init__(self,config):
        super(BitVit02, self).__init__()

        self.phase = 'trainBIT' if 'phase' not in config.keys() else config['phase']

        model = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
        weights = np.load(pretrained_model)
        model.load_from(weights)

        self.root = model.root
        self.body1 = create_partial_model(model.body, ['block1', 'block2', 'block3'])
        self.body2 = create_partial_model(model.body, ['block4'])
        self.head = model.head

        self.attention = ViTsa(image_size=32,patch_size=2,num_classes=2,channels=1024,dim=768,
                                depth=4, heads=16, mlp_dim=2048, dropout=0.1, emb_dropout=0.1)


        print_model_grads(self)
        if self.phase == 'trainBIT' :
            self.use_attention = False
            set_requires_grad(self, '', False)
            set_requires_grad(self, 'body2', True)
            set_requires_grad(self, 'head', True)
            reset_grads(self, 'head')
        elif  self.phase == 'trainVIT' :
            self.use_attention = True
            set_requires_grad(self, '', False)
            set_requires_grad(self, 'attention', True)
        print_model_grads(self)

        summary(self.root, ( 3, 512, 512), device='cpu')
        summary(self.body1, ( 64, 128, 128), device='cpu')
        summary(self.attention, (1024, 32, 32), device='cpu')
        summary(self.body2, ( 1024, 32, 32), device='cpu')
        summary(self.head, ( 2048, 16, 16), device='cpu')

    def forward(self, x):
        x = self.root(x)
        x = self.body1(x)

        if self.use_attention :
            x = self.attention(x)

        x = self.body2(x)
        x = self.head(x)
        x = x.squeeze(3).squeeze(2)
        return x


class VitPre00(nn.Module):
    def __init__(self,config):
        super(VitPre00, self).__init__()

        self.phase = 'trainBIT' if 'phase' not in config.keys() else config['phase']
        self.model = preViT('B_16_imagenet1k', pretrained=True)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        set_requires_grad(self, 'model.norm', True)
        set_requires_grad(self, 'model.fc', True)
        print_model_grads(self)

        summary(self.model, ( 3, 384, 384), device='cpu')

    def forward(self, x):
        out = self.model(x)
        return out



def get_model(model_type,config={}) :
    if model_type == 'VITPRE_01' :
        model = VitPre01(config)
    if model_type == 'VGGVIT_01' :
        model = VGGVit()
    elif model_type == 'VGGVIT_02':
        model = VGGVit02()
    elif model_type == 'BITVIT50_02':
        model = BitVit02(config)
    elif model_type == 'BITVIT50_03':
        model = BitVit03(config)

    return model

def get_vgg16_vit_model_01(reset_params = False) :
    model = VGGVit()

    return model

# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)
def get_vgg16_model_01(reset_params = False) :
    model = models.vgg16(pretrained=True)
    model.classifier[6] = nn.Linear(in_features=model.classifier[6].in_features, out_features=2, bias=True)
    print('Before requires_grad habdling \n')
    for name, param in model.named_parameters():
        print(f'{param.requires_grad} \t - \t {name} \t - \t {param.shape}')

    for name,param in model.features.named_parameters():
        param.requires_grad = False

    for i in [26,28] :
        for name,param in model.features[i].named_parameters():
            param.requires_grad = False

    for name,param in model.classifier.named_parameters():
        param.requires_grad = True

    print('\nAfter requires_grad habdling \n')
    for name, param in model.named_parameters():
        print(f'{param.requires_grad} \t - \t {name} \t - \t {param.shape}')

    return model
# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)

def get_bit_model_01(reset_params = False) :
    model = models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
    pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
    weights = np.load(pretrained_model)
    model.load_from(weights)

    for name, param in model.named_parameters():
        print(f'{param.requires_grad} \t - \t {name}')

    for name,param in model.root.named_parameters():
        param.requires_grad = True

    for name,param in model.body.named_parameters():
        param.requires_grad = False
        if name.split('.')[0] in ['block4'] :
            param.requires_grad = True
        else :
            param.requires_grad = False

    for name,param in model.head.named_parameters():
        param.requires_grad = True


    if reset_params :
        for layer in model.root.children():
            if hasattr(layer, 'reset_parameters'):
                layer.reset_parameters()

        for layer in model.head.children():
            if hasattr(layer, 'reset_parameters'):
                layer.reset_parameters()


    for name, param in model.named_parameters():
        print(f'{param.requires_grad} \t - \t {name}')

    return model
