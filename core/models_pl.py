
from __future__ import print_function

import torch.nn as nn
import pytorch_lightning as pl

import torchvision.models as models
import numpy as np
# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)
from vit_pytorch.vit import ViT
from vit_pytorch.vitsa import ViTsa
from torchvision import models
import bit_pytorch.models as bit_models

from torchsummary import summary
#from pytorch_model_summary import summary
from sklearn.metrics import roc_auc_score

import torch


from core.common import print_model_grads,create_partial_model,set_requires_grad,reset_grads
from core.common import Meausres,get_scheduler

class BitVit03(nn.Module):
    def __init__(self,config):
        super(BitVit03, self).__init__()

        self.phase = 'trainBIT' if 'phase' not in config.keys() else config['phase']

        model = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
        weights = np.load(pretrained_model)
        model.load_from(weights)

        self.root = model.root
        self.body1 = create_partial_model(model.body, ['block1', 'block2', 'block3'])
        self.body2 = create_partial_model(model.body, ['block4'])
        self.head = model.head

        self.attention = ViTsa(image_size=32,patch_size=2,num_classes=2,channels=1024,dim=768,
                                depth=2, heads=16, mlp_dim=2048, dropout=0.5, emb_dropout=0.5)

        print_model_grads(self)
        if self.phase == 'trainBIT' :
            self.use_attention = False
            set_requires_grad(self, '', False)
            set_requires_grad(self, 'head', True)
            reset_grads(self, 'head')
        elif  self.phase == 'trainVIT' :
            self.use_attention = True
            set_requires_grad(self, '', False)
            set_requires_grad(self, 'attention', True)
        print_model_grads(self)

        summary(self.root, ( 3, 512, 512), device='cpu')
        summary(self.body1, ( 64, 128, 128), device='cpu')
        summary(self.attention, (1024, 32, 32), device='cpu')
        summary(self.body2, ( 1024, 32, 32), device='cpu')
        summary(self.head, ( 2048, 16, 16), device='cpu')

    def forward(self, x):
        x = self.root(x)
        x = self.body1(x)

        if self.use_attention :
            x = self.attention(x)

        x = self.body2(x)
        x = self.head(x)
        x = x.squeeze(3).squeeze(2)
        return x


def get_model(model_type,config={}) :
    if model_type == 'BITVIT50_03':
        model = BitVit03(config)

    return model

class LitBitVit(pl.LightningModule):
    def __init__(self,model_type,args,config):
        super().__init__()
        self.niter = args.niter
        self.niter_decay = args.niter_decay
        self.begin_epoch = args.begin_epoch
        self.lr = args.lr
        self.save_hyperparameters()
        self.model = get_model(model_type,config)
        self.criterion =  nn.CrossEntropyLoss()
        self.m = Meausres()
        self.train_outputs = []
    def forward(self, x):
        # in lightning, forward defines the prediction/inference actions
        out = self.model(x)
        return out

    def training_step(self, batch, batch_idx):
        data, label = batch
        output = self(data)
        loss = self.criterion(output, label)
        acc = (output.argmax(dim=1) == label).float().mean()
        #        self.log("train_loss", loss)
        #   self.log("train_acc", acc)

        self.m.measure_weights(self.model)

        result = {'loss' : loss.item(), 'acc' : acc.item() , 'label' : label.cpu().detach() , 'preds' : output.cpu().detach()}
        self.train_outputs.append(result)
        return loss

    def validation_step(self, batch, batch_idx):
        result = self.evaluate(batch)
        return result

    def test_step(self, batch, batch_idx):
        result = self.evaluate(batch)
        return result

    def evaluate(self, batch):
        data, label = batch
        output = self(data)
        loss = self.criterion(output, label)
        acc = (output.argmax(dim=1) == label).float().mean()

        result = {'loss' : loss.item(), 'acc' : acc.item() , 'label' : label.cpu().detach() , 'preds' : output.cpu().detach()}

        return result

    def configure_optimizers(self):
        optimizer = torch.optim.SGD(self.parameters(filter(lambda p: p.requires_grad, self.parameters())), lr=self.lr,
                              momentum=0.9,weight_decay=5e-4)
        scheduler = get_scheduler(optimizer, lr_policy='lambda', niter=self.niter, niter_decay=self.niter_decay,
                                  begin_epoch=self.begin_epoch)

        scheduler_dict = {
            "scheduler": scheduler,
            "interval": "epoch",
        }
        return {"optimizer": optimizer, "lr_scheduler": scheduler_dict}

    def on_train_epoch_end(self):
        acc,loss,auc = self.calc_stats(self.train_outputs)

        self.log_dict({'epoch': self.current_epoch ,
                   'train_accuracy': acc,
                   'train_auc': auc,
                   'train_loss': loss})


        layer_measures = self.m.get_active_layers()
        layer_measures['epoch'] =  self.current_epoch
        self.log_dict(layer_measures)

        self.train_outputs = []
        self.m.restart()

    def calc_stats(self,outputs):
        acc = np.array([i['acc'] for i in outputs]).mean()
        loss = np.array([i['loss'] for i in outputs]).mean()
        label = np.concatenate([np.array(i['label'].unsqueeze(1)) for i in outputs],axis=0)
        preds = np.concatenate([np.array(i['preds']) for i in outputs],axis=0)
        labels = np.concatenate((1 - label, label), axis=1)
        auc = roc_auc_score(labels,preds)

        return acc,loss,auc

    def validation_epoch_end(self, outputs):
        if not outputs:
            print('\nvalidation_epoch_end - outputs is empty\n')
            return

        acc,loss,auc = self.calc_stats(outputs)
        self.log_dict({'epoch': self.current_epoch ,
                   'val_accuracy': acc,
                   'val_auc': auc,
                   'val_loss': loss})


        '''
        self.log({'epoch': epoch ,
                   'train_loss': epoch_train_loss,
                   'train_accuracy': epoch_train_accuracy,
                   'val_accuracy': epoch_val_accuracy,
                   'learning_rate': scheduler.get_last_lr()[0],
                   'best_accuracy': best_accuracy,
                   'val_auc': val_auc,
                   'val_loss': epoch_val_loss})
        '''
    def optimizer_zero_grad(self, epoch, batch_idx, optimizer, optimizer_idx):
        optimizer.zero_grad(set_to_none=True)

    def on_load_checkpoint(self, checkpoint):
        self.begin_epoch = checkpoint['epoch']