
from __future__ import print_function

from visdom import Visdom

import glob
from itertools import chain
import os
import random
import zipfile

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
from PIL import Image
from sklearn.model_selection import train_test_split
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader, Dataset
from torchvision import datasets, transforms
import os
from core.fileutils import fileparts
import copy
import torch.nn as nn

def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True


def get_train_split(train_list,test_size,stratify,random_state,workpath='./',ensamble_id=0) :
    num_splits = round(1 / test_size)
    num_i = len(train_list)
    assert ensamble_id  < num_splits
    valid_size =  round(num_i*test_size)

    trainsplitpath = os.path.join(workpath, f'trainsplit{num_splits:02}_{ensamble_id:02}.pkl')

    import pickle
    if not os.path.isfile(trainsplitpath) :
        lst = np.array((range(len(train_list))))
        random.shuffle(lst)
        cidx = [lst[i:i + valid_size] for i in range(0, len(lst), valid_size)]
        for eidx,valid_idx in enumerate(cidx) :
            train_idx = list(set(lst)-set(valid_idx))
            data = {'train_list': [train_list[i] for i in train_idx], 'valid_list': [train_list[i] for i in valid_idx]}
            trainsplitpath = os.path.join(workpath, f'trainsplit{num_splits:02}_{eidx:02}.pkl')
            with open(trainsplitpath, 'wb') as f:
                pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

    trainsplitpath = os.path.join(workpath, f'trainsplit{num_splits:02}_{ensamble_id:02}.pkl')

    if os.path.isfile(trainsplitpath):
        with open(trainsplitpath, 'rb') as f:
            data = pickle.load(f)

        '''
        train_list, valid_list = train_test_split(train_list,test_size=test_size,stratify=stratify,random_state=random_state)
        data = {'train_list':train_list ,'valid_list' : valid_list}
        with open(trainsplitpath, 'wb') as f:
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
        '''
    return data['train_list'],data['valid_list']

def get_params_stats(model) :
    total_params = sum(p.numel() for p in model.parameters())
    total_train_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    return total_params,total_train_params

class DDSMDataset(Dataset):
    def __init__(self, file_list,rootpath, transform=None,datatype='3types'):
        self.file_list = file_list
        self.transform = transform
        self.rootpath = rootpath
        self.datatype = datatype

    def __len__(self):
        self.filelength = len(self.file_list)
        return self.filelength

    def __getitem__(self, idx):
        item = self.file_list[idx]
        dirpath,filename,e = fileparts(item['imgpath'])
        _, basename, _, = fileparts(dirpath)
        index = filename.split('_')[-1]

        imgs_path = []
        if self.datatype == '2types':
            imgs_path.append(os.path.join(dirpath, f'{basename}_RZCROP_{index}.png'))
            imgs_path.append(os.path.join(dirpath, f'{basename}_RZCROP_{index}.png')) # Just for 3c compatiable
            imgs_path.append(os.path.join(dirpath, f'{basename}_FXCROP_{index}.png'))
        if self.datatype == '3types' :
            imgs_path.append(os.path.join(dirpath, f'{basename}_RZCROP_{index}.png'))
            #imgs_path.append(os.path.join(dirpath, f'{basename}_RZMASK_{index}.png'))
            imgs_path.append(os.path.join(dirpath, f'{basename}_FXCROP_{index}.png'))
            imgs_path.append(os.path.join(dirpath, f'{basename}_FXMASK_{index}.png'))


        imgs = []
        for img_path in imgs_path :
            img = Image.open(img_path)
            img_ = img
            img_transformed = transforms.ToTensor()(np.array(img_))
            if img.getbands()[0] == 'I' :
                img_transformed = img_transformed/ (2 ** 16 - 1)
                img_transformed = (img_transformed - img_transformed.min()) / (img_transformed.max() - img_transformed.min())
            imgs.append(img_transformed)

        label = 1 if item['pathology'] == 'MALIGNANT' else 0

        imgs = torch.cat(imgs, 0)
        imgs = 2*(imgs - 0.5)
        img_transformed = imgs if self.transform is None  else self.transform(imgs)

        return img_transformed, label

def load_images(dirpath,basename,index,datatype,transform) :
    imgs_path = []
    if datatype == '2types':
        imgs_path.append(os.path.join(dirpath, f'{basename}_RZCROP_{index}.png'))
        imgs_path.append(os.path.join(dirpath, f'{basename}_RZCROP_{index}.png')) # Just for 3c compatiable
        imgs_path.append(os.path.join(dirpath, f'{basename}_FXCROP_{index}.png'))
    if datatype == '3types' :
        imgs_path.append(os.path.join(dirpath, f'{basename}_RZCROP_{index}.png'))
        #imgs_path.append(os.path.join(dirpath, f'{basename}_RZMASK_{index}.png'))
        imgs_path.append(os.path.join(dirpath, f'{basename}_FXCROP_{index}.png'))
        imgs_path.append(os.path.join(dirpath, f'{basename}_FXMASK_{index}.png'))


    imgs = []
    for img_path in imgs_path :
        img = Image.open(img_path)
        img_ = img
        img_transformed = transforms.ToTensor()(np.array(img_))
        if img.getbands()[0] == 'I' :
            img_transformed = img_transformed/ (2 ** 16 - 1)
            img_transformed = (img_transformed - img_transformed.min()) / (img_transformed.max() - img_transformed.min())
        imgs.append(img_transformed)

    imgs = torch.cat(imgs, 0)
    imgs = 2*(imgs - 0.5)
    img_transformed = imgs if transform is None  else transform(imgs)
    return img_transformed

class DDSMPairsDataset(Dataset):
    def __init__(self, file_list,rootpath,pairs_list, transform=None,datatype='3types',phase='Train',pairstype = 'channels'):
        self.file_list = file_list
        self.pairs_list = pairs_list
        self.transform = transform
        self.rootpath = rootpath
        self.datatype = datatype
        self.phase = phase
        self.pairstype = pairstype

    def __len__(self):
        return len(self.pairs_list)

    def get_filename(self,item):
        item_type =  'Calc' if item['type'] == 'calcification' else 'Mass'
        phase = 'Train' if self.phase == 'Train' else 'Test'

        basephase = 'Training' if self.phase == 'Train' else 'Test'
        basename = f"{item_type}-{basephase}_{item['patient_id']}_{item['side']}_{item['view']}"
        dirpath = os.path.join(self.rootpath,item_type,phase,basename)
        index = f"{item['abnormality_id']:02}"
        return dirpath,basename, index

    def __getitem__(self, idx):
        idx1,idx2 = self.pairs_list[idx]
        if random.random() > 0.5 :
            idx1,idx2 = idx2,idx1

        item1 = self.file_list[idx1]
        item2 = self.file_list[idx2]
        dirpath,basename, index = self.get_filename(item1)
        img_transformed1 = load_images(dirpath, basename, index, self.datatype, self.transform)

        dirpath,basename, index = self.get_filename(item2)
        img_transformed2 = load_images(dirpath, basename, index, self.datatype, self.transform)
        label = 1 if item1['pathology'] == 'MALIGNANT' else 0
        if self.pairstype == 'channels' :
            img_transformed = img_transformed1
            img_transformed[:,:,1] = img_transformed2[:,:,2]
        elif self.pairstype == 'dual' :
            img_transformed = {'img1' : img_transformed1,'img2' : img_transformed2}
        elif self.pairstype == 'single' :
            img_transformed = {'img1' : img_transformed1,'img2' : img_transformed1}
        else :
            assert(False)

        return img_transformed, label

'''
        if basename =='Calc-Training_P_00011_LEFT_MLO' :
            print(basename)
        print(f'Loading :{dirpath}  {basename} {index}')
        if basename =='Calc-Training_P_00011_LEFT_MLO' :
            print(basename)
        print(f'Loading :{dirpath}  {basename} {index}')

'''
class VisdomLinePlotter(object):
    """Plots to Visdom"""
    def __init__(self, env_name='main'):
        self.viz = Visdom()
        self.env = env_name
        self.plots = {}
    def plot(self, var_name, split_name, title_name, x, y):
        if var_name not in self.plots:
            self.plots[var_name] = self.viz.line(X=np.array([x,x]), Y=np.array([y,y]), env=self.env, opts=dict(
                legend=[split_name],
                title=title_name,
                xlabel='Epochs',
                ylabel=var_name
            ))
        else:
            self.viz.line(X=np.array([x]), Y=np.array([y]), env=self.env, win=self.plots[var_name], name=split_name, update = 'append')

'''
epoch_count = 60
niter = 500
niter_decay = 50
for epoch in range(epoch_count,niter+niter_decay) :
    lr_l = 1.0 - max(0, epoch + 1 - niter) / float(niter_decay + 1)
    print(epoch,epoch + 1 - niter,niter_decay + 1 ,lr_l)
'''
def get_scheduler(optimizer, lr_policy,niter = None,niter_decay=None ,lr_decay_iters =None,begin_epoch = 0):
    if lr_policy == 'lambda':
        def lambda_rule(epoch):
            lr_l = 1.0 - max(0, (begin_epoch + epoch) + 1 - niter) / float(niter_decay + 1)
            return lr_l
        scheduler = lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda_rule)
    elif lr_policy == 'step':
        scheduler = lr_scheduler.StepLR(optimizer, step_size=lr_decay_iters, gamma=0.1)
    elif lr_policy == 'plateau':
        scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.2, threshold=0.01, patience=5)
    else:
        return NotImplementedError('learning rate policy [%s] is not implemented', lr_policy)
    return scheduler


def freeze_model_weights(model,freeze = True):
    for param in model.parameters():
        param.requires_grad = freeze


def measure_weights(network):
    lweights = dict()
    for name, m in network.named_parameters():
        data_m = m.data.mean().item()
        data_s = m.data.std().item()
        if m.requires_grad and m.grad is not None:
            grad_m = m.grad.mean().item()
            grad_s = m.grad.std().item()
        else :
            grad_m = 0
            grad_s = 0
        lweights[name] =  {'requires_grad' : m.requires_grad ,'data_mean': data_m, 'data_std': data_s , 'grad_mean': grad_m, 'grad_std': grad_s}
    return lweights


class Meausres():
    def __init__(self):
        self.restart()

    def restart(self):
        self.lweights = list()
        self.count = 0

    def measure_weights(self,network):
        lweights = measure_weights(network)
        if self.count == 0 :
            self.lweights = copy.deepcopy(lweights)
        else :
            for item in lweights :
                for param in lweights[item].keys() :
                    self.lweights[item][param] = self.lweights[item][param] +  lweights[item][param]
        self.count = self.count + 1

    def get_avg_values(self)  :
        lweights = copy.deepcopy(self.lweights)
        if self.count == 0 :
            lweights =  []

        for item in lweights :
            for param in lweights[item].keys():
                lweights[item][param] = self.lweights[item][param]/self.count

        return lweights

    def get_measures(self):
        lweights = self.get_avg_values()

        columns = ['layer_name' ,'requires_grad' ,'data_mean', 'data_std', 'grad_mean', 'grad_std']
        data = []
        for item in lweights :
            data.append([item,lweights[item]['requires_grad'],lweights[item]['data_mean'], lweights[item]['data_std'], lweights[item]['grad_mean'], lweights[item]['grad_std']])

        return columns,data

    def get_active_layers(self,param = 'grad_std'):
        lweights = self.get_avg_values()

        data = dict()
        for item in lweights :
            if lweights[item]['requires_grad'] :
                data[item] = lweights[item][param]

        return data


def print_model_grads(model):
    for name, param in model.named_parameters():
        pshape = param.detach().numpy().shape
        pnumel = param.detach().numel()
        print(f'{param.requires_grad} \t - \t {name} \t \t \t \t {pnumel:,} \t \t - \t \t {pshape}')
    print(f'\n\n')

def create_partial_model(model,blocks):
    sequential = nn.Sequential()
    for layer in blocks:
        sequential.add_module(layer,model.__getattr__(layer))

    '''
    modules = []
    for layer in blocks:
        modules.append(model.__getattr__(layer))
    sequential = nn.Sequential(*modules)
    '''
    return sequential

def is_layer_in_block(model,attr):
    if attr == '' :
        for name, param in model.named_parameters():
            param.requires_grad = val
        return

    pmodel = model
    for i in attr.split('.') :
        pmodel = pmodel.__getattr__(i)

    for name, param in pmodel.named_parameters():
        param.requires_grad = val

def remove_blocks_from_model(model,blocks):
    sequential = nn.Sequential()
    for layer in blocks:
        sequential.add_module(layer,model.__getattr__(layer))

    '''
    modules = []
    for layer in blocks:
        modules.append(model.__getattr__(layer))
    sequential = nn.Sequential(*modules)
    '''
    return sequential

def reset_grads(model,attr):
    pmodel = model
    for i in attr.split('.') :
        pmodel = pmodel.__getattr__(i)

    for layer in pmodel.children():
        if hasattr(layer, 'reset_parameters'):
            layer.reset_parameters()


def set_requires_grad(model,attr,val):
    if attr == '' :
        for name, param in model.named_parameters():
            param.requires_grad = val
        return

    pmodel = model
    for i in attr.split('.') :
        pmodel = pmodel.__getattr__(i)

    for name, param in pmodel.named_parameters():
        param.requires_grad = val
