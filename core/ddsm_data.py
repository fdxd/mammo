# Cross-view transformers for multi-view analysis of unregistered medical images
# Copyright (C) 2021 Gijs van Tulder / Radboud University, the Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import skimage.transform
import elasticdeform
import torch
import torch.utils
import h5py

datasets = {}


def register_dataset(cls):
    datasets[cls.__name__] = cls
    return cls


# data loader for CBIS-DDSM data with two views
@register_dataset
class DDSMDataset(torch.utils.data.Dataset):
    # intensity of the foreground pixels
    FOREGROUND_THRESHOLD = -0.99

    def __init__(
        self,
        datafile,
        augment=False,
        dtype=float,
        dtype_x=None,
        dtype_y=None,
        views=["cc", "mlo"],
        normalize=False,
        datatype=None,
        debug_random_label=None,
    ):
        super().__init__()
        self.datafile = datafile
        self.augment = augment
        self.dtype_x = dtype_x or dtype
        self.dtype_y = dtype_y or dtype
        self.views = views
        self.normalize = normalize
        self.datatype = datatype
        ds = h5py.File(self.datafile, "r")
        self.scan_ids = list(ds["scans"])
        self.num_scans = len(self.scan_ids)
        self.debug_random_label = debug_random_label
        if self.debug_random_label:  # SM
            import random

            self.scan_ids_shuffle = self.scan_ids.copy()
            random.shuffle(self.scan_ids_shuffle)

    def __getitem__(self, i):
        ds = h5py.File(self.datafile, "r")
        scan = ds["scans"][self.scan_ids[i]]
        # sum([int(ds['scans'][i].attrs['assessment_label']=='malignant')  for i in ds['scans'].keys()])
        # load images for all views
        if "image" in scan:
            assert len(self.views) == 1, "reading a single-view file"
            x_i = [scan["image"][:]]
        else:
            x_i = [scan[view]["image"][:] for view in self.views]
            s_i = [scan[view]["segmentation"][:] for view in self.views]

            """
            #x_i = [scan[view]['segmentation'][:].astype(x_i[0].dtype) for view in self.views]  # SM
            x_i = [scan[view]['image'][:] for view in self.views]
            s_i = [scan[view]['segmentation'][:] for view in self.views] #SM
            for i,_ in enumerate(s_i) :
                x_i[i][s_i[i]] = 1
            """
        # determine target label: False for benign, True for malignant
        assert scan.attrs["assessment_label"] in ("benign", "malignant")
        y_i = int(scan.attrs["assessment_label"] == "malignant")

        if self.debug_random_label:  # SM
            shuffle_scan = ds["scans"][self.scan_ids_shuffle[i]]
            y_i = int(shuffle_scan.attrs["assessment_label"] == "malignant")

        # normalize to mean=0, std=1
        if self.normalize:
            x_i = [self.normalize_image(x_i_v) for x_i_v in x_i]

        if self.augment:
            # flipping should be done for all views together
            coflip = np.random.randint(4)
            # augment each view independently
            # x_i = [self.augment_image(x_i_v, coflip=coflip) for x_i_v in x_i]
            x_i, s_i = [
                self.augment_image_S(x_i_v, s_i_v, coflip=coflip)
                for x_i_v, s_i_v in zip(x_i, s_i)
            ]

        # convert each view's image to torch
        x_i = [torch.tensor(x_i_v[None, :, :], dtype=self.dtype_x) for x_i_v in x_i]

        # convert label to torch
        y_i = torch.tensor(y_i, dtype=self.dtype_y)

        img = {"img1": x_i[0], "img2": x_i[1]}
        # concatenate views + label
        if self.datatype == "heatmaps":
            s_i = [item.astype(float) for item in s_i]
            for s in s_i:
                s[s == 1] = (
                    y_i * 2 - 1
                )  # -1 for Benign , 0 for Nothing, 1 for Malignant
            label = {
                "msk1": torch.tensor(s_i[0], dtype=self.dtype_y),
                "msk2": torch.tensor(s_i[1], dtype=self.dtype_y),
                "label": y_i,
            }
        else:
            label = y_i
        return img, label

    def get_label_stats(self, label):  # SM
        ds = h5py.File(self.datafile, "r")
        val = sum(
            [
                int(ds["scans"][i].attrs["assessment_label"] == label)
                for i in ds["scans"].keys()
            ]
        )

        return val

    def normalize_image(self, x_i):
        if self.normalize:
            x_i = x_i.astype(float)
            # normalize in-place
            fg_mask = x_i > self.FOREGROUND_THRESHOLD
            x_i -= np.mean(x_i[fg_mask])
            x_i /= np.maximum(np.std(x_i[fg_mask]), 1e-5)
        return x_i

    def augment_image(self, x_i, coflip=None):
        # augment the image for one view
        if self.augment:
            x_i = x_i.astype(float)

            if "flip" in self.augment or "coflip" in self.augment:
                # flip (using sample setting for coflip)
                t = coflip if "coflip" in self.augment else np.random.randint(4)
                if t == 1:  # flip first dimension
                    x_i = x_i[::-1, :]
                elif t == 2:  # flip second dimension
                    x_i = x_i[:, ::-1]
                elif t == 3:  # flip both dimensions
                    x_i = x_i[::-1, ::-1]

            if "elastic" in self.augment:
                # elastic deformations
                t = np.random.randint(2)
                if t == 1:
                    # choose a random zoom factor [0.9, 1.1]
                    zoom = np.random.uniform(0.9, 1.1)
                    # choose a random rotation of [-30, +30] degrees
                    rotate = np.random.uniform(-30, 30)
                    x_i = elasticdeform.deform_random_grid(
                        x_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )

            if "crop20" in self.augment:
                # random crop 20 pixels on each side
                offset_x = np.random.randint(40)
                offset_y = np.random.randint(40)
                x_i = x_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]

            if "gaussiannoise" in self.augment:
                x_i = np.random.normal(x_i, 0.01)

            # pytorch does not like negative strides
            x_i = np.ascontiguousarray(x_i)

        return x_i

    def augment_image_S(self, x_i, s_i, coflip=None):
        # augment the image for one view
        if self.augment:
            x_i = x_i.astype(float)
            s_i = s_i.astype(float)

            if "flip" in self.augment or "coflip" in self.augment:
                # flip (using sample setting for coflip)
                t = coflip if "coflip" in self.augment else np.random.randint(4)
                if t == 1:  # flip first dimension
                    x_i = x_i[::-1, :]
                    s_i = s_i[::-1, :]
                elif t == 2:  # flip second dimension
                    x_i = x_i[:, ::-1]
                    s_i = s_i[:, ::-1]
                elif t == 3:  # flip both dimensions
                    x_i = x_i[::-1, ::-1]
                    s_i = s_i[::-1, ::-1]

            if "elastic" in self.augment:
                # elastic deformations
                t = np.random.randint(2)
                if t == 1:
                    # choose a random zoom factor [0.9, 1.1]
                    zoom = np.random.uniform(0.9, 1.1)
                    # choose a random rotation of [-30, +30] degrees
                    rotate = np.random.uniform(-30, 30)
                    x_i = elasticdeform.deform_random_grid(
                        x_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )
                    s_i = elasticdeform.deform_random_grid(
                        s_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )

            if "crop20" in self.augment:
                # random crop 20 pixels on each side
                offset_x = np.random.randint(40)
                offset_y = np.random.randint(40)
                x_i = x_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]
                s_i = s_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]

            if "gaussiannoise" in self.augment:
                x_i = np.random.normal(x_i, 0.01)

            # pytorch does not like negative strides
            x_i = np.ascontiguousarray(x_i)
            s_i = np.ascontiguousarray(s_i)

        return x_i, s_i

    def class_freq(self):
        # return the number of samples in class (e.g., [700, 300])
        freq = torch.tensor([0, 0])
        ds = h5py.File(self.datafile, "r")
        for scan_id in self.scan_ids:
            # determine target label: False for benign, True for malignant
            scan = ds["scans"][scan_id]
            assert scan.attrs["assessment_label"] in ("benign", "malignant")
            y_i = int(scan.attrs["assessment_label"] == "malignant")
            freq[y_i] += 1
        return freq

    def __len__(self):
        return self.num_scans


def create_datalist(ds):
    datalist = [sorted([(item, i) for i in list(ds[item]["scans"])]) for item in ds]
    datalist_ = []
    for x in datalist:
        datalist_.extend(x)
    return datalist_


from core.augmentation import coaugmentation as augmentation

# data loader for CBIS-DDSM data with two views
@register_dataset
class DDSMDataset2(torch.utils.data.Dataset):
    # intensity of the foreground pixels
    FOREGROUND_THRESHOLD = -0.99

    def __init__(
        self,
        datafile,
        datalist,
        maxab=10,
        augment=None,
        dtype=float,
        dtype_x=None,
        dtype_y=None,
        views=["cc", "mlo"],
        normalize=False,
        datatype=None,
        debug_random_label=None,
    ):
        super().__init__()
        self.datafile = datafile
        self.augment = augment
        self.dtype_x = dtype_x or dtype
        self.dtype_y = dtype_y or dtype
        self.views = views
        self.normalize = normalize
        self.datatype = datatype
        self.maxab = maxab

        ds = {}
        for item in datafile:
            ds[item] = h5py.File(self.datafile[item], "r")

        datalist = create_datalist(ds) if datalist is None else datalist

        self.scan_ids = list(datalist)
        self.num_scans = len(self.scan_ids)

    def get_labels(self, scan, view):
        meta = {}
        for k in list(scan[view]["meta"].attrs.keys()):
            meta[k] = scan[view]["meta"].attrs[k]
        abns = sorted([i for i in list(scan[view].keys()) if "abnormality" in i])
        y_i = []
        for iabn in abns:
            abn = {}
            for k in list(scan[view][iabn].attrs.keys()):
                abn[k] = scan[view][iabn].attrs[k]
            y_i.append(abn)
        return meta, y_i

    def __getitem__(self, i):
        scan_type, scan_id = self.scan_ids[i]
        pid, side = scan_id
        ds = h5py.File(self.datafile[scan_type], "r")
        scan = ds["scans"][pid][side]

        xcc = scan["cc"]["image"][:]
        xml = scan["mlo"]["image"][:]
        scc = scan["cc"]["segmentations"][:]
        sml = scan["mlo"]["segmentations"][:]

        meta_cc, abns_cc = self.get_labels(scan, "cc")
        meta_ml, abns_ml = self.get_labels(scan, "mlo")

        label_cc = [int(i["assessment_label"] == "malignant") for i in abns_cc]
        label_ml = [int(i["assessment_label"] == "malignant") for i in abns_ml]

        score_cc = [i["assessment_score"] for i in abns_cc]
        score_ml = [i["assessment_score"] for i in abns_ml]

        if self.normalize:
            xcc = self.normalize_image(xcc)
            xml = self.normalize_image(xml)

        xcc = np.expand_dims(xcc, axis=0)
        xml = np.expand_dims(xml, axis=0)
        if self.augment is not None:
            img = augmentation({"xcc": xcc, "scc": scc}, self.augment)
            xcc, scc = img["xcc"], img["scc"]
            img = augmentation({"xml": xml, "sml": sml}, self.augment)
            xml, sml = img["xml"], img["sml"]

        # convert each view's image to torch

        xcc = torch.tensor(xcc.copy(), dtype=self.dtype_x)
        xml = torch.tensor(xml.copy(), dtype=self.dtype_x)
        scc = torch.tensor(scc.copy(), dtype=self.dtype_x)
        sml = torch.tensor(sml.copy(), dtype=self.dtype_x)

        # xcc = torch.zeros_like(xcc)
        # xml = torch.zeros_like(xml)
        # scc = torch.zeros_like(scc)
        # sml = torch.zeros_like(sml)

        label_cc = torch.tensor(label_cc, dtype=self.dtype_y)
        label_ml = torch.tensor(label_ml, dtype=self.dtype_y)
        score_cc = torch.tensor(score_cc, dtype=self.dtype_y)
        score_ml = torch.tensor(score_ml, dtype=self.dtype_y)

        import torch.nn.functional as F

        maxab = self.maxab
        scc = F.pad(
            scc.permute(1, 2, 0), (0, maxab - scc.shape[0]), "constant", -1
        ).permute(2, 0, 1)
        sml = F.pad(
            sml.permute(1, 2, 0), (0, maxab - sml.shape[0]), "constant", -1
        ).permute(2, 0, 1)

        label_cc = F.pad(label_cc, (0, maxab - label_cc.shape[0]), "constant", -1)
        label_ml = F.pad(label_ml, (0, maxab - label_ml.shape[0]), "constant", -1)
        score_cc = F.pad(score_cc, (0, maxab - score_cc.shape[0]), "constant", -1)
        score_ml = F.pad(score_ml, (0, maxab - score_ml.shape[0]), "constant", -1)

        img = {
            "xcc": xcc,
            "xml": xml,
            "scc": scc,
            "sml": sml,
            "score_cc": score_cc,
            "score_ml": score_ml,
        }
        label = {
            "label_cc": label_cc,
            "label_ml": label_ml,
            "score_cc": score_cc,
            "score_ml": score_ml,
        }

        return img, label

    def get_label_stats(self, label):  # SM
        ds = h5py.File(self.datafile, "r")
        val = sum(
            [
                int(ds["scans"][i].attrs["assessment_label"] == label)
                for i in ds["scans"].keys()
            ]
        )

        return val

    def normalize_image(self, x_i):
        if self.normalize:
            x_i = x_i.astype(float)
            # normalize in-place
            fg_mask = x_i > self.FOREGROUND_THRESHOLD
            x_i -= np.mean(x_i[fg_mask])
            x_i /= np.maximum(np.std(x_i[fg_mask]), 1e-5)
        return x_i

    def augment_image(self, x_i, coflip=None):
        # augment the image for one view
        if self.augment:
            x_i = x_i.astype(float)

            if "flip" in self.augment or "coflip" in self.augment:
                # flip (using sample setting for coflip)
                t = coflip if "coflip" in self.augment else np.random.randint(4)
                if t == 1:  # flip first dimension
                    x_i = x_i[::-1, :]
                elif t == 2:  # flip second dimension
                    x_i = x_i[:, ::-1]
                elif t == 3:  # flip both dimensions
                    x_i = x_i[::-1, ::-1]

            if "elastic" in self.augment:
                # elastic deformations
                t = np.random.randint(2)
                if t == 1:
                    # choose a random zoom factor [0.9, 1.1]
                    zoom = np.random.uniform(0.9, 1.1)
                    # choose a random rotation of [-30, +30] degrees
                    rotate = np.random.uniform(-30, 30)
                    x_i = elasticdeform.deform_random_grid(
                        x_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )

            if "crop20" in self.augment:
                # random crop 20 pixels on each side
                offset_x = np.random.randint(40)
                offset_y = np.random.randint(40)
                x_i = x_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]

            if "gaussiannoise" in self.augment:
                x_i = np.random.normal(x_i, 0.01)

            # pytorch does not like negative strides
            x_i = np.ascontiguousarray(x_i)

        return x_i

    def augment_image_S(self, x_i, s_i, coflip=None):
        # augment the image for one view
        if self.augment:
            x_i = x_i.astype(float)
            s_i = s_i.astype(float)

            if "flip" in self.augment or "coflip" in self.augment:
                # flip (using sample setting for coflip)
                t = coflip if "coflip" in self.augment else np.random.randint(4)
                if t == 1:  # flip first dimension
                    x_i = x_i[::-1, :]
                    s_i = s_i[::-1, :]
                elif t == 2:  # flip second dimension
                    x_i = x_i[:, ::-1]
                    s_i = s_i[:, ::-1]
                elif t == 3:  # flip both dimensions
                    x_i = x_i[::-1, ::-1]
                    s_i = s_i[::-1, ::-1]

            if "elastic" in self.augment:
                # elastic deformations
                t = np.random.randint(2)
                if t == 1:
                    # choose a random zoom factor [0.9, 1.1]
                    zoom = np.random.uniform(0.9, 1.1)
                    # choose a random rotation of [-30, +30] degrees
                    rotate = np.random.uniform(-30, 30)
                    x_i = elasticdeform.deform_random_grid(
                        x_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )
                    s_i = elasticdeform.deform_random_grid(
                        s_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )

            if "crop20" in self.augment:
                # random crop 20 pixels on each side
                offset_x = np.random.randint(40)
                offset_y = np.random.randint(40)
                x_i = x_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]
                s_i = s_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]

            if "gaussiannoise" in self.augment:
                x_i = np.random.normal(x_i, 0.01)

            # pytorch does not like negative strides
            x_i = np.ascontiguousarray(x_i)
            s_i = np.ascontiguousarray(s_i)

        return x_i, s_i

    def class_freq(self):
        # return the number of samples in class (e.g., [700, 300])
        freq = torch.tensor([0, 0])
        ds = h5py.File(self.datafile, "r")
        for scan_id in self.scan_ids:
            # determine target label: False for benign, True for malignant
            scan = ds["scans"][scan_id]
            assert scan.attrs["assessment_label"] in ("benign", "malignant")
            y_i = int(scan.attrs["assessment_label"] == "malignant")
            freq[y_i] += 1
        return freq

    def __len__(self):
        return self.num_scans

@register_dataset
class DDSMDataset4(torch.utils.data.Dataset):
    # intensity of the foreground pixels
    FOREGROUND_THRESHOLD = -0.99

    def __init__(
        self,
        datafile,
        datalist,
        maxab=10,
        augment=None,
        dtype=float,
        dtype_x=None,
        dtype_y=None,
        views=["cc", "mlo"],
        normalize=False,
        datatype=None,
    ):
        super().__init__()
        self.datafile = datafile
        self.augment = augment
        self.dtype_x = dtype_x or dtype
        self.dtype_y = dtype_y or dtype
        self.views = views
        self.normalize = normalize
        self.datatype = datatype
        self.maxab = maxab
        self.scan_ids = list(datalist)
        self.num_scans = len(self.scan_ids)

    def get_labels(self, scan, view):
        meta = {}
        for k in list(scan[view]["meta"].attrs.keys()):
            meta[k] = scan[view]["meta"].attrs[k]
        abns = sorted([i for i in list(scan[view].keys()) if "abnormality" in i])
        y_i = []
        for iabn in abns:
            abn = {}
            for k in list(scan[view][iabn].attrs.keys()):
                abn[k] = scan[view][iabn].attrs[k]
            y_i.append(abn)
        return meta, y_i

    def calc_meta(self, meta):

        calc_type = {
            "N/A": 0,
            "SKIN": 1,
            "COARSE": 2,
            "PUNCTATE": 3,
            "PLEOMORPHIC": 4,
            "VASCULAR": 5,
            "LUCENT_CENTER": 6,
            "ROUND_AND_REGULAR": 7,
            "EGGSHELL": 8,
            "AMORPHOUS": 9,
            "FINE_LINEAR_BRANCHING": 10,
            "DYSTROPHIC": 11,
            "LARGE_RODLIKE": 12,
            "LUCENT_CENTERED": 13,
            "MILK_OF_CALCIUM": 14,
        }  # size: 15
        calc_distribution = {
            "N/A": 0,
            "CLUSTERED": 1,
            "SEGMENTAL": 2,
            "LINEAR": 3,
            "DIFFUSELY_SCATTERED": 4,
            "REGIONAL": 5,
        }  # size: 6

        abn_vec = np.array([0])  # 0 for calc
        abn_feature_list = []

        for i in meta:
            type_vec = [0] * len(calc_type)
            distb_vec = [0] * len(calc_distribution)

            abn_type = i["calc type"]
            for _type in abn_type.split("-"):
                type_vec[calc_type[_type]] = 1

            abn_dist = i["calc distribution"]
            for dist in abn_dist.split("-"):
                distb_vec[calc_distribution[dist]] = 1

            # return the concat of both vectors
            abn_features = np.concatenate((abn_vec, type_vec, distb_vec))  # size: 15
            abn_feature_list.append(abn_features.tolist())

        return abn_feature_list

    def mass_meta(self, meta):

        mass_shape = {
            "N/A": 0,
            "IRREGULAR": 1,
            "ROUND": 2,
            "LOBULATED": 3,
            "OVAL": 4,
            "ARCHITECTURAL_DISTORTION": 5,
            "ASYMMETRIC_BREAST_TISSUE": 6,
            "LYMPH_NODE": 7,
            "FOCAL_ASYMMETRIC_DENSITY": 8,
        }  # size: 9
        mass_margins = {
            "N/A": 0,
            "SPICULATED": 1,
            "CIRCUMSCRIBED": 2,
            "ILL_DEFINED": 3,
            "OBSCURED": 4,
            "MICROLOBULATED": 5,
        }  # size: 6

        abn_vec = np.array([1])  # 1 for mass

        abn_feature_list = []

        for i in meta:
            type_vec = [0] * len(mass_shape)  # mass shape
            distb_vec = [0] * len(mass_margins)  # mass margins

            abn_shape = i["mass shape"]
            for shape in abn_shape.split("-"):
                type_vec[mass_shape[shape]] = 1

            abn_margin = i["mass margins"]
            for margin in abn_margin.split("-"):
                distb_vec[mass_margins[margin]] = 1

            # return the concat of both vectors
            abn_features = np.concatenate((abn_vec, type_vec, distb_vec))  # size: 15
            abn_feature_list.append(abn_features.tolist())

        return abn_feature_list

    def __getitem__(self, i):
        scan_type, scan_id = self.scan_ids[i]
        pid, side = scan_id
        file_list = self.datafile[scan_type]

        ds = h5py.File(file_list, "r")
        scan = ds["scans"][pid][side]

        xcc = scan["cc"]["image"][:]
        xml = scan["mlo"]["image"][:]
        scc = scan["cc"]["segmentations"][:]
        sml = scan["mlo"]["segmentations"][:]

        _, abns_cc = self.get_labels(scan, "cc")
        _, abns_ml = self.get_labels(scan, "mlo")

        label_cc = [int(i["assessment_label"] == "malignant") for i in abns_cc]
        label_ml = [int(i["assessment_label"] == "malignant") for i in abns_ml]

        score_cc = [i["assessment_score"] for i in abns_cc]
        score_ml = [i["assessment_score"] for i in abns_ml]

        if "calc" in scan_type:
            meta_cc_vector = self.calc_meta(abns_cc)
            meta_mlo_vector = self.calc_meta(abns_ml)
        else:
            meta_cc_vector = self.mass_meta(abns_cc)
            meta_mlo_vector = self.mass_meta(abns_ml)

        if self.normalize:
            xcc = self.normalize_image(xcc)
            xml = self.normalize_image(xml)

        xcc = np.expand_dims(xcc, axis=0)
        xml = np.expand_dims(xml, axis=0)
        if self.augment is not None:
            img = augmentation({"xcc": xcc, "scc": scc}, self.augment)
            xcc, scc = img["xcc"], img["scc"]
            img = augmentation({"xml": xml, "sml": sml}, self.augment)
            xml, sml = img["xml"], img["sml"]

        # convert each view's image to torch

        xcc = torch.tensor(xcc.copy(), dtype=self.dtype_x)
        xml = torch.tensor(xml.copy(), dtype=self.dtype_x)
        scc = torch.tensor(scc.copy(), dtype=self.dtype_x)
        sml = torch.tensor(sml.copy(), dtype=self.dtype_x)

        xcc = torch.zeros_like(xcc)
        xml = torch.zeros_like(xml)
        scc = torch.zeros_like(scc)
        sml = torch.zeros_like(sml)

        label_cc = torch.tensor(label_cc, dtype=self.dtype_y)
        label_ml = torch.tensor(label_ml, dtype=self.dtype_y)
        score_cc = torch.tensor(score_cc, dtype=self.dtype_y)
        score_ml = torch.tensor(score_ml, dtype=self.dtype_y)

        meta_cc_vector = torch.tensor(meta_cc_vector)
        meta_mlo_vector = torch.tensor(meta_mlo_vector)

        import torch.nn.functional as F

        maxab = self.maxab
        scc = F.pad(
            scc.permute(1, 2, 0), (0, maxab - scc.shape[0]), "constant", -1
        ).permute(2, 0, 1)
        sml = F.pad(
            sml.permute(1, 2, 0), (0, maxab - sml.shape[0]), "constant", -1
        ).permute(2, 0, 1)

        label_cc = F.pad(label_cc, (0, maxab - label_cc.shape[0]), "constant", -1)
        label_ml = F.pad(label_ml, (0, maxab - label_ml.shape[0]), "constant", -1)
        score_cc = F.pad(score_cc, (0, maxab - score_cc.shape[0]), "constant", -1)
        score_ml = F.pad(score_ml, (0, maxab - score_ml.shape[0]), "constant", -1)

        # print("meta_cc_before", meta_cc_vector.shape)
        # print("meta_mlo_before", meta_mlo_vector.shape)

        meta_cc_vector = F.pad(meta_cc_vector, (0, 256 - meta_cc_vector.shape[1]), "constant", 0)
        meta_mlo_vector = F.pad(meta_mlo_vector, (0, 256 - meta_mlo_vector.shape[1]), "constant", 0)

        # print("meta_cc_after", meta_cc_vector.shape)
        # print("meta_mlo_after", meta_mlo_vector.shape)

        # print("scc", scc.shape)
        # print("sml", sml.shape)

        img = {
            "xcc": xcc,
            "xml": xml,
            "scc": scc,
            "sml": sml,
            "score_cc": score_cc,
            "score_ml": score_ml,
            "meta_cc": meta_cc_vector,
            "meta_mlo": meta_mlo_vector,
        }
        label = {
            "label_cc": label_cc,
            "label_ml": label_ml,
            "score_cc": score_cc,
            "score_ml": score_ml,
        }
        return img, label

    def get_label_stats(self, label):  # SM
        ds = h5py.File(self.datafile, "r")
        val = sum(
            [
                int(ds["scans"][i].attrs["assessment_label"] == label)
                for i in ds["scans"].keys()
            ]
        )

        return val

    def normalize_image(self, x_i):
        if self.normalize:
            x_i = x_i.astype(float)
            # normalize in-place
            fg_mask = x_i > self.FOREGROUND_THRESHOLD
            x_i -= np.mean(x_i[fg_mask])
            x_i /= np.maximum(np.std(x_i[fg_mask]), 1e-5)
        return x_i

    def augment_image(self, x_i, coflip=None):
        # augment the image for one view
        if self.augment:
            x_i = x_i.astype(float)

            if "flip" in self.augment or "coflip" in self.augment:
                # flip (using sample setting for coflip)
                t = coflip if "coflip" in self.augment else np.random.randint(4)
                if t == 1:  # flip first dimension
                    x_i = x_i[::-1, :]
                elif t == 2:  # flip second dimension
                    x_i = x_i[:, ::-1]
                elif t == 3:  # flip both dimensions
                    x_i = x_i[::-1, ::-1]

            if "elastic" in self.augment:
                # elastic deformations
                t = np.random.randint(2)
                if t == 1:
                    # choose a random zoom factor [0.9, 1.1]
                    zoom = np.random.uniform(0.9, 1.1)
                    # choose a random rotation of [-30, +30] degrees
                    rotate = np.random.uniform(-30, 30)
                    x_i = elasticdeform.deform_random_grid(
                        x_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )

            if "crop20" in self.augment:
                # random crop 20 pixels on each side
                offset_x = np.random.randint(40)
                offset_y = np.random.randint(40)
                x_i = x_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]

            if "gaussiannoise" in self.augment:
                x_i = np.random.normal(x_i, 0.01)

            # pytorch does not like negative strides
            x_i = np.ascontiguousarray(x_i)

        return x_i

    def augment_image_S(self, x_i, s_i, coflip=None):
        # augment the image for one view
        if self.augment:
            x_i = x_i.astype(float)
            s_i = s_i.astype(float)

            if "flip" in self.augment or "coflip" in self.augment:
                # flip (using sample setting for coflip)
                t = coflip if "coflip" in self.augment else np.random.randint(4)
                if t == 1:  # flip first dimension
                    x_i = x_i[::-1, :]
                    s_i = s_i[::-1, :]
                elif t == 2:  # flip second dimension
                    x_i = x_i[:, ::-1]
                    s_i = s_i[:, ::-1]
                elif t == 3:  # flip both dimensions
                    x_i = x_i[::-1, ::-1]
                    s_i = s_i[::-1, ::-1]

            if "elastic" in self.augment:
                # elastic deformations
                t = np.random.randint(2)
                if t == 1:
                    # choose a random zoom factor [0.9, 1.1]
                    zoom = np.random.uniform(0.9, 1.1)
                    # choose a random rotation of [-30, +30] degrees
                    rotate = np.random.uniform(-30, 30)
                    x_i = elasticdeform.deform_random_grid(
                        x_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )
                    s_i = elasticdeform.deform_random_grid(
                        s_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )

            if "crop20" in self.augment:
                # random crop 20 pixels on each side
                offset_x = np.random.randint(40)
                offset_y = np.random.randint(40)
                x_i = x_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]
                s_i = s_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]

            if "gaussiannoise" in self.augment:
                x_i = np.random.normal(x_i, 0.01)

            # pytorch does not like negative strides
            x_i = np.ascontiguousarray(x_i)
            s_i = np.ascontiguousarray(s_i)

        return x_i, s_i

    def class_freq(self):
        # return the number of samples in class (e.g., [700, 300])
        freq = torch.tensor([0, 0])
        ds = h5py.File(self.datafile, "r")
        for scan_id in self.scan_ids:
            # determine target label: False for benign, True for malignant
            scan = ds["scans"][scan_id]
            assert scan.attrs["assessment_label"] in ("benign", "malignant")
            y_i = int(scan.attrs["assessment_label"] == "malignant")
            freq[y_i] += 1
        return freq

    def __len__(self):
        return self.num_scans

@register_dataset
class DDSMDataset3(torch.utils.data.Dataset):
    # intensity of the foreground pixels
    FOREGROUND_THRESHOLD = -0.99

    def __init__(
        self,
        datafile,
        datalist,
        maxab=10,
        augment=None,
        dtype=float,
        dtype_x=None,
        dtype_y=None,
        views=["cc", "mlo"],
        normalize=False,
        datatype=None,
    ):
        super().__init__()
        self.datafile = datafile
        self.augment = augment
        self.dtype_x = dtype_x or dtype
        self.dtype_y = dtype_y or dtype
        self.views = views
        self.normalize = normalize
        self.datatype = datatype
        self.maxab = maxab
        self.scan_ids = list(datalist)
        self.num_scans = len(self.scan_ids)

    def get_labels(self, scan, view):
        meta = {}
        for k in list(scan[view]["meta"].attrs.keys()):
            meta[k] = scan[view]["meta"].attrs[k]
        abns = sorted([i for i in list(scan[view].keys()) if "abnormality" in i])
        y_i = []
        for iabn in abns:
            abn = {}
            for k in list(scan[view][iabn].attrs.keys()):
                abn[k] = scan[view][iabn].attrs[k]
            y_i.append(abn)
        return meta, y_i

    def calc_meta(self, meta):

        calc_type = {
            "N/A": 0,
            "SKIN": 1,
            "COARSE": 2,
            "PUNCTATE": 3,
            "PLEOMORPHIC": 4,
            "VASCULAR": 5,
            "LUCENT_CENTER": 6,
            "ROUND_AND_REGULAR": 7,
            "EGGSHELL": 8,
            "AMORPHOUS": 9,
            "FINE_LINEAR_BRANCHING": 10,
            "DYSTROPHIC": 11,
            "LARGE_RODLIKE": 12,
            "LUCENT_CENTERED": 13,
            "MILK_OF_CALCIUM": 14,
        }  # size: 15
        calc_distribution = {
            "N/A": 0,
            "CLUSTERED": 1,
            "SEGMENTAL": 2,
            "LINEAR": 3,
            "DIFFUSELY_SCATTERED": 4,
            "REGIONAL": 5,
        }  # size: 6

        abn_vec = np.array([0])  # 0 for calc
        abn_feature_list = []

        for i in meta:
            type_vec = [0] * len(calc_type)
            distb_vec = [0] * len(calc_distribution)

            abn_type = i["calc type"]
            for _type in abn_type.split("-"):
                type_vec[calc_type[_type]] = 1

            abn_dist = i["calc distribution"]
            for dist in abn_dist.split("-"):
                distb_vec[calc_distribution[dist]] = 1

            # return the concat of both vectors
            abn_features = np.concatenate((abn_vec, type_vec, distb_vec))  # size: 15
            abn_feature_list.append(abn_features.tolist())

        return abn_feature_list

    def mass_meta(self, meta):

        mass_shape = {
            "N/A": 0,
            "IRREGULAR": 1,
            "ROUND": 2,
            "LOBULATED": 3,
            "OVAL": 4,
            "ARCHITECTURAL_DISTORTION": 5,
            "ASYMMETRIC_BREAST_TISSUE": 6,
            "LYMPH_NODE": 7,
            "FOCAL_ASYMMETRIC_DENSITY": 8,
        }  # size: 9
        mass_margins = {
            "N/A": 0,
            "SPICULATED": 1,
            "CIRCUMSCRIBED": 2,
            "ILL_DEFINED": 3,
            "OBSCURED": 4,
            "MICROLOBULATED": 5,
        }  # size: 6

        abn_vec = np.array([1])  # 1 for mass

        abn_feature_list = []

        for i in meta:
            type_vec = [0] * len(mass_shape)  # mass shape
            distb_vec = [0] * len(mass_margins)  # mass margins

            abn_shape = i["mass shape"]
            for shape in abn_shape.split("-"):
                type_vec[mass_shape[shape]] = 1

            abn_margin = i["mass margins"]
            for margin in abn_margin.split("-"):
                distb_vec[mass_margins[margin]] = 1

            # return the concat of both vectors
            abn_features = np.concatenate((abn_vec, type_vec, distb_vec))  # size: 15
            abn_feature_list.append(abn_features.tolist())

        return abn_feature_list

    def __getitem__(self, i):
        scan_type, scan_id = self.scan_ids[i]
        pid, side = scan_id
        file_list = self.datafile[scan_type]

        ds = h5py.File(file_list, "r")
        scan = ds["scans"][pid][side]

        xcc = scan["cc"]["image"][:]
        xml = scan["mlo"]["image"][:]
        scc = scan["cc"]["segmentations"][:]
        sml = scan["mlo"]["segmentations"][:]

        _, abns_cc = self.get_labels(scan, "cc")
        _, abns_ml = self.get_labels(scan, "mlo")

        label_cc = [int(i["assessment_label"] == "malignant") for i in abns_cc]
        label_ml = [int(i["assessment_label"] == "malignant") for i in abns_ml]

        score_cc = [i["assessment_score"] for i in abns_cc]
        score_ml = [i["assessment_score"] for i in abns_ml]

        if "calc" in scan_type:
            meta_cc_vector = self.calc_meta(abns_cc)
            meta_mlo_vector = self.calc_meta(abns_ml)
        else:
            meta_cc_vector = self.mass_meta(abns_cc)
            meta_mlo_vector = self.mass_meta(abns_ml)

        if self.normalize:
            xcc = self.normalize_image(xcc)
            xml = self.normalize_image(xml)

        xcc = np.expand_dims(xcc, axis=0)
        xml = np.expand_dims(xml, axis=0)
        if self.augment is not None:
            img = augmentation({"xcc": xcc, "scc": scc}, self.augment)
            xcc, scc = img["xcc"], img["scc"]
            img = augmentation({"xml": xml, "sml": sml}, self.augment)
            xml, sml = img["xml"], img["sml"]

        # convert each view's image to torch

        xcc = torch.tensor(xcc.copy(), dtype=self.dtype_x)
        xml = torch.tensor(xml.copy(), dtype=self.dtype_x)
        scc = torch.tensor(scc.copy(), dtype=self.dtype_x)
        sml = torch.tensor(sml.copy(), dtype=self.dtype_x)

        label_cc = torch.tensor(label_cc, dtype=self.dtype_y)
        label_ml = torch.tensor(label_ml, dtype=self.dtype_y)
        score_cc = torch.tensor(score_cc, dtype=self.dtype_y)
        score_ml = torch.tensor(score_ml, dtype=self.dtype_y)

        meta_cc_vector = torch.tensor(meta_cc_vector)
        meta_mlo_vector = torch.tensor(meta_mlo_vector)

        import torch.nn.functional as F

        maxab = self.maxab
        scc = F.pad(
            scc.permute(1, 2, 0), (0, maxab - scc.shape[0]), "constant", -1
        ).permute(2, 0, 1)
        sml = F.pad(
            sml.permute(1, 2, 0), (0, maxab - sml.shape[0]), "constant", -1
        ).permute(2, 0, 1)

        label_cc = F.pad(label_cc, (0, maxab - label_cc.shape[0]), "constant", -1)
        label_ml = F.pad(label_ml, (0, maxab - label_ml.shape[0]), "constant", -1)
        score_cc = F.pad(score_cc, (0, maxab - score_cc.shape[0]), "constant", -1)
        score_ml = F.pad(score_ml, (0, maxab - score_ml.shape[0]), "constant", -1)

        meta_cc_vector = F.pad(meta_cc_vector, (0, 256 - meta_cc_vector.shape[1], 0, maxab - meta_cc_vector.shape[0]), "constant", 0)
        meta_mlo_vector = F.pad(meta_mlo_vector, (0, 256 - meta_mlo_vector.shape[1], 0, maxab - meta_mlo_vector.shape[0]), "constant", 0)

        img = {
            "xcc": xcc,
            "xml": xml,
            "scc": scc,
            "sml": sml,
            "score_cc": score_cc,
            "score_ml": score_ml,
            "meta_cc": meta_cc_vector,
            "meta_mlo": meta_mlo_vector,
        }
        label = {
            "label_cc": label_cc,
            "label_ml": label_ml,
            "score_cc": score_cc,
            "score_ml": score_ml,
        }
        return img, label

    def get_label_stats(self, label):  # SM
        ds = h5py.File(self.datafile, "r")
        val = sum(
            [
                int(ds["scans"][i].attrs["assessment_label"] == label)
                for i in ds["scans"].keys()
            ]
        )

        return val

    def normalize_image(self, x_i):
        if self.normalize:
            x_i = x_i.astype(float)
            # normalize in-place
            fg_mask = x_i > self.FOREGROUND_THRESHOLD
            x_i -= np.mean(x_i[fg_mask])
            x_i /= np.maximum(np.std(x_i[fg_mask]), 1e-5)
        return x_i

    def augment_image(self, x_i, coflip=None):
        # augment the image for one view
        if self.augment:
            x_i = x_i.astype(float)

            if "flip" in self.augment or "coflip" in self.augment:
                # flip (using sample setting for coflip)
                t = coflip if "coflip" in self.augment else np.random.randint(4)
                if t == 1:  # flip first dimension
                    x_i = x_i[::-1, :]
                elif t == 2:  # flip second dimension
                    x_i = x_i[:, ::-1]
                elif t == 3:  # flip both dimensions
                    x_i = x_i[::-1, ::-1]

            if "elastic" in self.augment:
                # elastic deformations
                t = np.random.randint(2)
                if t == 1:
                    # choose a random zoom factor [0.9, 1.1]
                    zoom = np.random.uniform(0.9, 1.1)
                    # choose a random rotation of [-30, +30] degrees
                    rotate = np.random.uniform(-30, 30)
                    x_i = elasticdeform.deform_random_grid(
                        x_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )

            if "crop20" in self.augment:
                # random crop 20 pixels on each side
                offset_x = np.random.randint(40)
                offset_y = np.random.randint(40)
                x_i = x_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]

            if "gaussiannoise" in self.augment:
                x_i = np.random.normal(x_i, 0.01)

            # pytorch does not like negative strides
            x_i = np.ascontiguousarray(x_i)

        return x_i

    def augment_image_S(self, x_i, s_i, coflip=None):
        # augment the image for one view
        if self.augment:
            x_i = x_i.astype(float)
            s_i = s_i.astype(float)

            if "flip" in self.augment or "coflip" in self.augment:
                # flip (using sample setting for coflip)
                t = coflip if "coflip" in self.augment else np.random.randint(4)
                if t == 1:  # flip first dimension
                    x_i = x_i[::-1, :]
                    s_i = s_i[::-1, :]
                elif t == 2:  # flip second dimension
                    x_i = x_i[:, ::-1]
                    s_i = s_i[:, ::-1]
                elif t == 3:  # flip both dimensions
                    x_i = x_i[::-1, ::-1]
                    s_i = s_i[::-1, ::-1]

            if "elastic" in self.augment:
                # elastic deformations
                t = np.random.randint(2)
                if t == 1:
                    # choose a random zoom factor [0.9, 1.1]
                    zoom = np.random.uniform(0.9, 1.1)
                    # choose a random rotation of [-30, +30] degrees
                    rotate = np.random.uniform(-30, 30)
                    x_i = elasticdeform.deform_random_grid(
                        x_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )
                    s_i = elasticdeform.deform_random_grid(
                        s_i, sigma=5, points=5, zoom=zoom, rotate=rotate
                    )

            if "crop20" in self.augment:
                # random crop 20 pixels on each side
                offset_x = np.random.randint(40)
                offset_y = np.random.randint(40)
                x_i = x_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]
                s_i = s_i[offset_y : -(40 - offset_y), offset_x : -(40 - offset_x)]

            if "gaussiannoise" in self.augment:
                x_i = np.random.normal(x_i, 0.01)

            # pytorch does not like negative strides
            x_i = np.ascontiguousarray(x_i)
            s_i = np.ascontiguousarray(s_i)

        return x_i, s_i

    def class_freq(self):
        # return the number of samples in class (e.g., [700, 300])
        freq = torch.tensor([0, 0])
        ds = h5py.File(self.datafile, "r")
        for scan_id in self.scan_ids:
            # determine target label: False for benign, True for malignant
            scan = ds["scans"][scan_id]
            assert scan.attrs["assessment_label"] in ("benign", "malignant")
            y_i = int(scan.attrs["assessment_label"] == "malignant")
            freq[y_i] += 1
        return freq

    def __len__(self):
        return self.num_scans
