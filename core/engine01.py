from __future__ import print_function

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.metrics import roc_auc_score
import wandb
import os
# from torchviz import make_dot

from core.common import get_scheduler, Meausres
from core.models import get_model
from core.datasets import (
    prepare_testset,
    prepare_trainset_pairs_fullbody,
    prepare_trainset_pairs_fullbody_combined,
    prepare_trainset_pairs_calc_combined,
    prepare_trainset_pairs_mass_combined,
)


def train_stage(args, device="cuda", config={}, ensamble_id="N/A"):
    wsppath = os.path.join(args.workpath, args.name, f"{args.ensamble_id:04}")
    os.makedirs(wsppath, exist_ok=True)

    dataset = (
        "pairs_fullbody_combined" if not hasattr(args, "dataset") else args.dataset
    )

    test_loader = None

    if dataset == "pairs_fullbody":
        train_loader, valid_loader, test_loader = prepare_trainset_pairs_fullbody(args)
    elif dataset == "pairs_fullbody_combined":
        (
            train_loader,
            valid_loader,
            test_loader,
        ) = prepare_trainset_pairs_fullbody_combined(args)
    elif dataset == "calc-only":
        train_loader, valid_loader, test_loader = prepare_trainset_pairs_calc_combined(
            args
        )
    elif dataset == "mass-only":
        train_loader, valid_loader, test_loader = prepare_trainset_pairs_mass_combined(
            args
        )

    plotter = None

    train_model(
        args,
        wsppath,
        train_loader,
        valid_loader,
        test_loader,
        device=device,
        plotter=plotter,
        config=config,
    )


def move_to_device(data, device):
    if type(data) is dict:
        for item in data.keys():
            data[item] = data[item].to(device)
    else:
        data = data.to(device)
    return data


def one_epoch(
    epoch, data_loader, model, optimizer, device, m=None, phase="train", config={}
):
    criterion = nn.CrossEntropyLoss()
    epoch_train_loss = 0
    epoch_train_accuracy = 0
    lacc = []
    results = {"ref": [], "out": []}
    for data, label in data_loader:
        data = move_to_device(data, device)
        label = move_to_device(label, device)

        if phase == "train":
            optimizer.zero_grad()

        output = model(data)
        # VIZ
        # make_dot(output, params=dict(list(model.named_parameters()))).render("mammogram-dl", format="svg")
        # exit()
        loss = (
            model.criterion(output, label)
            if hasattr(model, "criterion")
            else criterion(output, label)
        )
        if phase == "train":
            loss.backward()
            optimizer.step()

        output = output["output"] if type(output) is dict else output
        if hasattr(model, "accuracy"):
            acc, ref, out = model.accuracy(output, label)
        else:
            acc = (output.argmax(dim=1) == label).float().mean()
            ref = label.long()
            out = output.argmax(dim=1)

        # acc = (output.argmax(dim=1) == label.argmax(dim=1)).float().mean()
        lacc.append(acc)
        epoch_train_accuracy += acc.item() / len(data_loader)
        epoch_train_loss += loss.item() / len(data_loader)

        results["ref"].append(ref)
        results["out"].append(output)

        if m is not None:
            m.measure_weights(model)

        print(
            f"Epoch ({phase}) : {epoch + 1} - loss : {epoch_train_loss:.4f} - acc: {epoch_train_accuracy:.4f}  ({acc.item():.4f}) "
        )

    results["ref"] = torch.cat(results["ref"])
    results["out"] = torch.cat(results["out"])
    return epoch_train_loss, epoch_train_accuracy, results


def calc_auc(val_results):
    vref = val_results["ref"].cpu().detach().numpy()
    vout = val_results["out"].cpu().detach().numpy()
    val_auc = roc_auc_score(vref,vout[:,1])
    return val_auc


def train_model(
    args,
    wsppath,
    train_loader,
    valid_loader,
    test_loader,
    device="cpu",
    plotter=None,
    config={},
):
    epochs = args.epochs
    lr = args.lr
    step_size = args.step_size
    gamma = args.gamma
    modelname = args.name
    model_type = args.model_type

    optimizer_type = args.optimizer_type
    scheduler_type = args.scheduler_type

    os.makedirs(wsppath, exist_ok=True)

    model = get_model(model_type=model_type, config=config)
    # if torch.cuda.device_count() > 1:
    # model = nn.DataParallel(model)
    model = model.to(device)

    # loss function
    # criterion = nn.CrossEntropyLoss()
    # optimizer
    # optimizer = optim.Adam(model.parameters(), lr=lr)
    # optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9, weight_decay=0.1)

    # scheduler
    logfile = os.path.join(wsppath, f"logfile_{modelname}.txt")
    with open(logfile, "a") as f:
        f.close()

    best_model_path = os.path.join(wsppath, f"best_checkpoint_{modelname}.pth")
    best_accuracy = 0
    best_accuracy_test = 0
    begin_epoch = 0
    model_path = os.path.join(wsppath, f"checkpoint.pth")
    if os.path.isfile(model_path):
        checkpoint = torch.load(model_path)
        model.load_state_dict(checkpoint["state_dict"])
        begin_epoch = checkpoint["epoch"]
        best_accuracy = checkpoint["best_accuracy"]
        best_accuracy_test = (
            0
            if "best_accuracy_test" not in checkpoint.keys()
            else checkpoint["best_accuracy_test"]
        )

    if optimizer_type == "SGD":
        optimizer = optim.SGD(
            model.parameters(filter(lambda p: p.requires_grad, model.parameters())),
            lr=lr,
            momentum=0.9,
        )
    elif optimizer_type == "ADAM":
        optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=0.0001)
    else:
        optimizer = optim.SGD(
            model.parameters(filter(lambda p: p.requires_grad, model.parameters())),
            lr=lr,
            momentum=0.9,
        )

    if scheduler_type == "LAMBDA":
        scheduler = get_scheduler(
            optimizer,
            lr_policy="lambda",
            niter=args.niter,
            niter_decay=args.niter_decay,
            begin_epoch=begin_epoch,
        )
    elif scheduler_type == "STEP_LR":
        scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.1)
    else:
        scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.1)
        # scheduler = StepLR(optimizer, step_size=step_size, gamma=gamma)

    m = Meausres()
    for epoch in range(begin_epoch, epochs):
        m.restart()
        epoch_train_loss, epoch_train_accuracy, train_results = one_epoch(
            epoch, train_loader, model, optimizer, device, m
        )

        with torch.no_grad():
            epoch_val_loss, epoch_val_accuracy, val_results = one_epoch(
                epoch, valid_loader, model, optimizer, device, phase="valid"
            )

        with torch.no_grad():
            epoch_test_loss, epoch_test_accuracy, test_results = one_epoch(
                epoch, test_loader, model, optimizer, device, phase="test"
            )

        columns, data = m.get_measures()
        layer_measures = m.get_active_layers()

        tbl = wandb.Table(data=data, columns=columns)
        wandb.log({"classifier_out": tbl})

        val_auc = calc_auc(val_results)
        test_auc = calc_auc(test_results)


        # val_vref = val_results["ref"].cpu().detach().numpy()
        # val_vout = val_results["out"].cpu().detach().numpy()[:,1]
        # test_vref = test_results["ref"].cpu().detach().numpy()
        # test_vout = test_results["out"].cpu().detach().numpy()[:,1]

        # wandb.log({"val-roc" : wandb.plot.roc_curve(val_vref, val_vout)})

        # wandb.log({"test-roc" : wandb.plot.roc_curve(test_vref, test_vout)})

        savedata = {
            "epoch": epoch + 1,
            "state_dict": model.state_dict(),
            "epoch_accuracy": epoch_train_accuracy,
            "epoch_loss": epoch_train_loss,
            "epoch_val_accuracy": epoch_val_accuracy,
            "best_accuracy": best_accuracy,
            "best_accuracy_test": best_accuracy_test,
            "epoch_val_loss": epoch_val_loss,
        }

        wandb.log(
            {
                "epoch": epoch,
                "train_loss": epoch_train_loss,
                "train_accuracy": epoch_train_accuracy,
                "learning_rate": scheduler.get_last_lr()[0],
            }
        )
        wandb.log(
            {
                "epoch": epoch,
                "val_loss": epoch_val_loss,
                "val_accuracy": epoch_val_accuracy,
                "best_accuracy": best_accuracy,
                "val_auc": val_auc,
            }
        )
        wandb.log(
            {
                "epoch": epoch,
                "test_loss": epoch_test_loss,
                "test_accuracy": epoch_test_accuracy,
                "best_accuracy_test": best_accuracy_test,
                "test_auc": test_auc,
            }
        )

        layer_measures["epoch"] = epoch
        wandb.log(layer_measures)

        torch.save(savedata, model_path)
        if epoch_val_accuracy > best_accuracy:
            torch.save(savedata, best_model_path)

            best_accuracy = epoch_val_accuracy
            best_accuracy_test = epoch_test_accuracy
            print(f"Epoch : {epoch + 1} --- best_accuracy : {best_accuracy}\n")

        scheduler.step()

        with open(logfile, "a") as f:
            f.write(
                f"Epoch : {epoch + 1} - loss : {epoch_train_loss:.4f} - acc: {epoch_train_accuracy:.4f} - val_loss : {epoch_val_loss:.4f} - val_acc: {epoch_val_accuracy:.4f}\n"
            )
            f.close()

        print(
            f"Epoch : {epoch+1} - loss : {epoch_train_loss:.4f} - acc: {epoch_train_accuracy:.4f} - val_loss : {epoch_val_loss:.4f} - val_acc: {epoch_val_accuracy:.4f}\n"
        )


def test_model(args, wsppath, test_loader, device="cpu", epoch=-1, config={}):
    model_type = args.model_type

    os.makedirs(wsppath, exist_ok=True)

    model = get_model(model_type=model_type, config=config)
    model = model.to(device)

    model_path = os.path.join(wsppath, f"checkpoint.pth")
    if os.path.isfile(model_path):
        checkpoint = torch.load(model_path)
        model.load_state_dict(checkpoint["state_dict"])

    with torch.no_grad():
        epoch_test_loss, epoch_test_accuracy, test_results = one_epoch(
            epoch, test_loader, model, optimizer=None, device=device, phase="valid"
        )

        vref = test_results["ref"].cpu().detach().numpy()
        vout = test_results["out"].cpu().detach().numpy().reshape(-1, 1)
        val_auc = roc_auc_score(
            np.concatenate(
                (np.expand_dims(1 - vref, 1), np.expand_dims(vref, 1)), axis=1
            ),
            vout,
        )

        wandb.log(
            {
                "epoch": epoch,
                "test_accuracy": epoch_test_accuracy,
                "test_auc": val_auc,
                "test_loss": epoch_test_loss,
            }
        )

        print(
            f"Epoch : {epoch+1} - val_loss : {epoch_test_loss:.4f} - val_acc: {epoch_test_accuracy:.4f}\n"
        )


def test_aug_stage(args, ensamble_id="0000", device="cuda"):
    wsppath = os.path.join(args.workpath, args.name)
    modelname = args.name

    test_loader = prepare_testset(args)

    model = get_model()
    model = model.to(device)

    best_model_path = os.path.join(
        wsppath, ensamble_id, f"best_checkpoint_{modelname}.pth"
    )
    if os.path.isfile(best_model_path):
        checkpoint = torch.load(best_model_path)
        model.load_state_dict(checkpoint["state_dict"])

    accs = []
    scores = []
    labels = []
    for i in range(10):
        test_accuracy, v = test_model(test_loader, model, device)
        accs.append(test_accuracy)
        scores.append(v["pred_scores"])
        labels.append(v["orig_labels"])
        print(f"test_accuracy {test_accuracy}")

    mscore = []
    for i in range(10):
        mscore.append(np.expand_dims(np.argmax(scores[i], axis=1), axis=1))
    mscore = np.concatenate(mscore, axis=1)
    plabels = np.sum(mscore, axis=1) >= 5
    plabels = np.expand_dims(np.array([int(i) for i in plabels]), axis=1)

    sum(plabels == labels[0]) / len(plabels)


def test_stage(args, device="cuda", config={}):
    wsppath = os.path.join(args.workpath, args.name)
    modelname = args.name

    test_loader = prepare_testset(args)

    model_type = args.model_type

    os.makedirs(wsppath, exist_ok=True)

    model = get_model(model_type=model_type, config=config)
    model = model.to(device)

    best_model_path = os.path.join(
        wsppath, f"{args.ensamble_id:04d}", f"best_checkpoint_{modelname}.pth"
    )
    if os.path.isfile(best_model_path):
        checkpoint = torch.load(best_model_path)
        model.load_state_dict(checkpoint["state_dict"])

    test_accuracy, v = test_model(test_loader, model, device)

    test_auc = roc_auc_score(
        np.concatenate(
            (
                np.expand_dims(1 - v["orig_labels"], 1),
                np.expand_dims(v["orig_labels"], 1),
            ),
            axis=1,
        ),
        v["pred_scores"],
    )

    wandb.log(
        {
            "best_model_path": best_model_path,
            "test_accuracy": test_accuracy,
            "test_auc": test_auc,
        }
    )

    return test_accuracy


def test_model(test_loader, model, device):
    vres = []
    vlab = []
    with torch.no_grad():
        epoch_test_accuracy = 0
        for data, label in test_loader:
            data = data.to(device)
            label = label.to(device)

            val_output = model(data)

            acc = (val_output.argmax(dim=1) == label).float().mean()
            epoch_test_accuracy += acc.item() / len(test_loader)

            vres.append(val_output.detach().cpu().numpy())
            vlab.append(label.detach().cpu().numpy())

        vres = np.concatenate(vres, axis=0)
        vlab = np.concatenate(vlab, axis=0)

    return epoch_test_accuracy, {"pred_scores": vres, "orig_labels": vlab}


from shutil import copyfile
import zipfile
from core.fileutils import get_files_list


def zip_source_code(dstpath):
    flist = get_files_list(os.getcwd(), ".py")
    codefile = os.path.join(dstpath, "source_code.zip")

    with zipfile.ZipFile(codefile, "w") as myzip:
        for item in flist:
            myzip.write(item)

    return codefile


def wandb_init(args, config):
    wandb_mode = args.wandb_mode  # "disabled" if True else "online"
    wsppath = os.path.join(args.workpath, args.name, f"{args.ensamble_id:04}")
    wandb_path = os.path.join(wsppath, "opts")  # .replace('/','\\')

    os.makedirs(wandb_path, exist_ok=True)

    if hasattr(args, "job_type"):
        run = wandb.init(
            project="mammo_NN",
            entity="ferastr",
            group=f"{args.name}",
            job_type=args.job_type,
            id=f"{args.name}_{args.job_type}",
            resume="allow",
            mode=wandb_mode,
            dir=wandb_path,
        )
        run.name = f"{args.name}"
    else:
        run = wandb.init(
            project="mammo_NN",
            entity="ferastr",
            id=f"{args.name}_{args.ensamble_id:04}",
            resume="allow",
            mode=wandb_mode,
            dir=wandb_path,
        )
        run.name = f"{args.name}_{args.ensamble_id:04}"

    run.notes = args.notes
    run.save()
    wbcfg = wandb.config
    wbcfg.update(args, allow_val_change=True)
    wbcfg.update(config, allow_val_change=True)

    codefile = zip_source_code(wandb_path)
    wandb.save(codefile, base_path=wandb_path)
    return run