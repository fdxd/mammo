
from __future__ import print_function

import torch.nn as nn

import torchvision.models as models
import numpy as np
# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)
from vit_pytorch.vit import ViT
from vit_pytorch.vitsa import ViTsa
from pytorch_pretrained_vit import ViT as preViT
import torch.nn.functional as F


from torchvision import models
import bit_pytorch.models as bit_models

from torchsummary import summary
#from pytorch_model_summary import summary
from core.common import print_model_grads,create_partial_model,set_requires_grad,reset_grads,remove_blocks_from_model

import torch
import torch
from perceiver_pytorch import Perceiver
from core.perceiver_mv.perceiver_mv import PerceiverMV
from einops.einops import rearrange

class Perceiver01(nn.Module):
    def __init__(self,config):
        super(Perceiver01, self).__init__()

        perceivers_layers_num = 2 if 'perceivers_layers_num' not in config.keys() else config['perceivers_layers_num']

        self.model = Perceiver(
            input_channels=1024*2,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=10.,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=256,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=512,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=0.,
            ff_dropout=0.,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2  # number of self attention blocks per cross attention
        )

        self.fold = nn.Fold(output_size=(32, 32), kernel_size=(1, 1))
        #summary(self.model, (1024, 1024,1), device='cpu')
        #img = torch.randn(1, 224, 224, 1)  # 1 imagenet image, pixelized
        #self.model(img)

        self.loss_fn = torch.nn.CrossEntropyLoss()#pos_weight=class_weight[1]

    def criterion(self,output,label):
        loss = self.loss_fn(output,label.type(torch.long))
        return loss

    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']
        x1 = F.interpolate(x1, (1024, 1024), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (1024, 1024), mode='bicubic', align_corners=True)

        fx1 = self.fold(x1[:,0,:,:]).permute(0,2,3,1)
        fx2 = self.fold(x2[:,0,:,:]).permute(0,2,3,1)
        x = torch.cat((fx1,fx2),dim=-1)
        out = self.model(x)

        return F.log_softmax(out,dim=1)


class Perceiver02(nn.Module):
    def __init__(self,config):
        super(Perceiver02, self).__init__()

        perceivers_layers_num = 2 if 'perceivers_layers_num' not in config.keys() else config['perceivers_layers_num']
        patch_size = 32
        self.model = Perceiver(
            input_channels=patch_size*patch_size,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=10.,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=256,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=512,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=0.,
            ff_dropout=0.,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2  # number of self attention blocks per cross attention
        )

        self.fold = nn.Fold(output_size=(patch_size, patch_size), kernel_size=(1, 1))
        #summary(self.model, (1024, 1024,1), device='cpu')
        #img = torch.randn(1, 224, 224, 1)  # 1 imagenet image, pixelized
        #self.model(img)

        self.loss_fn = torch.nn.CrossEntropyLoss()#pos_weight=class_weight[1]

    def criterion(self,output,label):
        loss = self.loss_fn(output,label.type(torch.long))
        return loss

    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']
        x1 = F.interpolate(x1, (1024, 1024), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (1024, 1024), mode='bicubic', align_corners=True)

        fx1 = self.fold(x1[:,0,:,:])
        fx2 = self.fold(x2[:,0,:,:])
        x = torch.cat((fx1,fx2),dim=1)
        x = x.reshape(x.shape[0],x.shape[1],1,-1)
        out = self.model(x)

        return F.log_softmax(out,dim=1)


class Perceiver03(nn.Module):
    def __init__(self,config):
        super(Perceiver03, self).__init__()

        perceivers_layers_num = 2 if 'perceivers_layers_num' not in config.keys() else config['perceivers_layers_num']
        dropout= 0. if 'dropout' not in config.keys() else config['dropout']
        max_freq= 10. if 'max_freq' not in config.keys() else config['max_freq']
        self.multi_loss = False if 'multi_loss' not in config.keys() else config['multi_loss']

        self.num_stages = 1 if 'num_stages' not in config.keys() else config['num_stages']
        assert(perceivers_layers_num >self.num_stages)
        self.max_resolution = 1024
        self.patch_size = 32
        self.patch_step = 8

        self.model = PerceiverMV(
            input_channels=self.patch_size*self.patch_size,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=256,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=512,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2  # number of self attention blocks per cross attention
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()#pos_weight=class_weight[1]

    def criterion(self,output,label):
        loss = self.loss_fn(output,label.type(torch.long))
        return loss


    def data_fold(self,x1,dsize = 1024):
        x1 = F.interpolate(x1, (dsize, dsize), mode='bicubic', align_corners=True)
        fx1 = nn.functional.unfold(x1, kernel_size=(self.patch_size, self.patch_size), stride=8)
        fx1 = rearrange(fx1, 'n (c ww) l -> n l ww c', ww=self.patch_size ** 2)
        fx1 = fx1.permute(0,1,3,2)
        return fx1

    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']

        y = []
        for stage_idx in range(self.num_stages) :
            factor = self.num_stages - (stage_idx + 1)
            resolution = round(self.max_resolution / pow(2,factor))
            fx1 = self.data_fold(x1, dsize=resolution)
            fx2 = self.data_fold(x2, dsize=resolution)
            y.append(fx1)
            y.append(fx2)

        out = self.model(y,multi_loss = self.multi_loss)

        return F.log_softmax(out,dim=1)






class Perceiver04(nn.Module):
    def __init__(self,config):
        super(Perceiver04, self).__init__()

        perceivers_layers_num = 2 if 'perceivers_layers_num' not in config.keys() else config['perceivers_layers_num']
        dropout= 0. if 'dropout' not in config.keys() else config['dropout']
        max_freq= 10. if 'max_freq' not in config.keys() else config['max_freq']
        self.multi_loss = False if 'multi_loss' not in config.keys() else config['multi_loss']


        self.num_stages = 1 if 'num_stages' not in config.keys() else config['num_stages']
        assert(perceivers_layers_num >self.num_stages)
        #self.max_resolution = 1024
        self.min_resolution = 128
        self.qpatch_size = 16
        self.patch_size = 32
        self.patch_step = 8
        num_latents = round(2 * pow(self.min_resolution / self.qpatch_size, 2)) #query - 2 images - unfolded to patches with size of  qpatch_size
        query_dim = round(pow(self.qpatch_size, 2))
        latent_dim = 2*query_dim
        self.model = PerceiverMV(
            input_channels=self.patch_size*self.patch_size,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=num_latents,  # number of latents, or induced set points, or centroids. different papers giving it different names
            query_dim = query_dim,
            latent_dim=latent_dim,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2  # number of self attention blocks per cross attention
        )

        self.loss_fn = torch.nn.MSELoss()#pos_weight=class_weight[1]


    def criterion(self,output,label):
        l1,l2 = label['msk1'],  label['msk2']
        fl1 = self.base_fold(l1.unsqueeze(1))
        fl2 = self.base_fold(l2.unsqueeze(1))
        out = torch.split(output, round(output.shape[1] / 2), dim=1)
        out1, out2 = out[0],out[1]

        loss1_L2 = F.mse_loss(fl1.mean(dim=-1) , out1.mean(dim=-1) )
        loss1_L1 = F.l1_loss(torch.pow(fl1,2).mean(dim=-1) , torch.pow(out1,2).mean(dim=-1) )
        loss2_L2 = F.mse_loss(fl2.mean(dim=-1) , out2.mean(dim=-1) )
        loss2_L1 = F.l1_loss(torch.pow(fl2,2).mean(dim=-1) , torch.pow(out2,2).mean(dim=-1) )

        loss_acc,_,_ = self.accuracy_loss( output, label)

        loss = loss_acc*0.5 + 0.5*(loss1_L2 + loss1_L1 + loss2_L2 + loss2_L1)
        return loss

    def calc_label(self,x):
        ones = torch.stack([torch.sum(ix > 0.5)/ix.numel() for ix in x])
        zeros = torch.stack([torch.sum(ix < -0.5)/ix.numel() for ix in x])
#        labels = (ones > zeros).float()
        labels = torch.cat((zeros.reshape(-1,1),ones.reshape(-1,1)) , dim=1)
        return labels

    def accuracy_loss(self,output,label):
        criterion = nn.CrossEntropyLoss()
        output_ = self.calc_label(output)
        label_ = label['label'].long()

        acc = criterion(output_, label_)
        return acc,label_,output_

    def accuracy(self,output,label):
        output_ = self.calc_label(output).detach().cpu()
        label_ = label['label'].detach().cpu()

        output_ = torch.argmax(output_,dim=1)
        acc = (output_ == label_).float().mean()

        ref = torch.tensor(label_).long()
        out = torch.tensor(output_).long()

        return acc,ref,out


    def data_fold(self,x1,dsize = 1024,patch_size=32,patch_step=8):
        x1 = F.interpolate(x1, (dsize, dsize), mode='bicubic', align_corners=True)
        fx1 = nn.functional.unfold(x1, kernel_size=(patch_size, patch_size), stride=patch_step)
        fx1 = rearrange(fx1, 'n (c ww) l -> n l ww c', ww=patch_size ** 2)
        fx1 = fx1.permute(0,1,3,2)
        return fx1

    def base_fold(self,x1):
        fx1 = self.data_fold(x1, dsize=self.min_resolution,patch_size=self.qpatch_size, patch_step=self.qpatch_size)
        return fx1.squeeze(2)


    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']

        fx1 = self.base_fold(x1)
        fx2 = self.base_fold(x2)
        query = torch.cat((fx1,fx2),dim=1)

        y = []
        for stage_idx in range(self.num_stages) :
            resolution = round(self.min_resolution * pow(2,stage_idx))
            fx1 = self.data_fold(x1, dsize=resolution,patch_size=self.patch_size,patch_step = self.patch_step)
            fx2 = self.data_fold(x2, dsize=resolution,patch_size=self.patch_size,patch_step = self.patch_step)
            y.append(fx1)
            y.append(fx2)

        out = self.model(y,query=query,multi_loss = self.multi_loss,return_embeddings=True)

        return F.log_softmax(out,dim=1)




class Perceiver05(nn.Module):
    def __init__(self,config):
        super(Perceiver05, self).__init__()

        perceivers_layers_num = 2 if 'perceivers_layers_num' not in config.keys() else config['perceivers_layers_num']
        dropout= 0. if 'dropout' not in config.keys() else config['dropout']
        max_freq= 10. if 'max_freq' not in config.keys() else config['max_freq']
        self.multi_loss = False if 'multi_loss' not in config.keys() else config['multi_loss']

        self.num_stages = 1 if 'num_stages' not in config.keys() else config['num_stages']
        #assert(perceivers_layers_num >self.num_stages)
        self.min_resolution = 128
        self.patch_size = 32
        self.patch_step = 8

        self.model = PerceiverMV(
            input_channels=self.patch_size*self.patch_size,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=256,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=512,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2  # number of self attention blocks per cross attention
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()#pos_weight=class_weight[1]

    def criterion(self,output,label):
        loss = self.loss_fn(output,label.type(torch.long))
        return loss


    def data_fold(self,x1,dsize = 1024,patch_size=32,patch_step=8):
        x1 = F.interpolate(x1, (dsize, dsize), mode='bicubic', align_corners=True)
        fx1 = nn.functional.unfold(x1, kernel_size=(patch_size, patch_size), stride=patch_step)
        fx1 = rearrange(fx1, 'n (c ww) l -> n l ww c', ww=patch_size ** 2)
        fx1 = fx1.permute(0,1,3,2)
        return fx1


    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']

        for stage_idx in range(self.num_stages) :
            y = []
            resolution = round(self.min_resolution * pow(2,stage_idx))
            fx1 = self.data_fold(x1, dsize=resolution,patch_size=self.patch_size,patch_step = self.patch_step)
            fx2 = self.data_fold(x2, dsize=resolution,patch_size=self.patch_size,patch_step = self.patch_step)
            y.append(fx1)
            y.append(fx2)
            y = self.model(y,multi_loss = self.multi_loss)
            y = y + F.log_softmax(y,dim=1)

        return y





class Perceiver06(nn.Module):
    def __init__(self,config):
        super(Perceiver06, self).__init__()

        perceivers_layers_num = 2 if 'perceivers_layers_num' not in config.keys() else config['perceivers_layers_num']
        dropout= 0. if 'dropout' not in config.keys() else config['dropout']
        max_freq= 10. if 'max_freq' not in config.keys() else config['max_freq']
        self.multi_loss = False if 'multi_loss' not in config.keys() else config['multi_loss']
        self.share_weights = False if 'share_weights' not in config.keys() else config['share_weights']
        self.num_stages = 1 if 'num_stages' not in config.keys() else config['num_stages']
        #assert(perceivers_layers_num >self.num_stages)
        self.min_resolution = 128
        self.patch_size = 32
        self.patch_step = 8

        self.model = PerceiverMV(
            input_channels=self.patch_size*self.patch_size,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=256,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=512,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2  # number of self attention blocks per cross attention
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()#pos_weight=class_weight[1]

    def criterion(self,output,label):
        loss = self.loss_fn(output,label.type(torch.long))
        return loss


    def data_fold(self,x1,dsize = 1024,patch_size=32,patch_step=8):
        x1 = F.interpolate(x1, (dsize, dsize), mode='bicubic', align_corners=True)
        fx1 = nn.functional.unfold(x1, kernel_size=(patch_size, patch_size), stride=patch_step)
        fx1 = rearrange(fx1, 'n (c ww) l -> n l ww c', ww=patch_size ** 2)
        fx1 = fx1.permute(0,1,3,2)
        return fx1


    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']

        y = []
        for stage_idx in range(self.num_stages) :
            resolution = round(self.min_resolution * pow(2,stage_idx))
            fx1 = self.data_fold(x1, dsize=resolution,patch_size=self.patch_size,patch_step = self.patch_step)
            fx2 = self.data_fold(x2, dsize=resolution,patch_size=self.patch_size,patch_step = self.patch_step)
            y.append(fx1)
            y.append(fx2)

        out = self.model(y,multi_loss = self.multi_loss,share_weights = self.share_weights)

        return F.log_softmax(out,dim=1)
