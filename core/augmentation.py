
import numpy as np
import skimage.transform
import elasticdeform

import torch.utils
from skimage.transform import resize

def aug_hflip(img) :
    for item in img :
        img[item] = np.fliplr(img[item])
    return img

def aug_vflip(img) :
    for item in img :
        img[item] = np.flipud(img[item])
    return img

def aug_elastic(img , zoom=0.1, angle=30) :
    zoom = np.random.uniform(1-zoom, 1+zoom)
    rotate = np.random.uniform(-angle, angle)

    img_ = {}
    for item in img :
        img_[item] = img[item]
        for idx,i in enumerate(img[item]) :
            img_[item][idx] = elasticdeform.deform_random_grid(i,sigma=5, points=5, zoom=zoom, rotate=rotate)

    return img_

def aug_crop(img, step = 40) :
    # random crop 20 pixels on each side
    offset_x = np.random.randint(step)
    offset_y = np.random.randint(step)

    for item in img :
        c,oh, ow = img[item].shape

        iii = img[item][:,offset_y:-(step - offset_y), offset_x:-(step - offset_x)]
        img[item] = resize(iii, (1, oh, ow))

    return img

def aug_gaussiannoise(img) :
    for item in img :
          gauss = ((0.01**0.5)*np.random.randn(*img[item].shape))
          img[item] = np.add(img[item], gauss, out=img[item], casting="unsafe")
    return img

def coaugmentation(img, augment = ['hflip','vflip','elastic','crop20','gaussiannoise']):
    flaug = {}
    flaug['hflip'] = (np.random.randint(2) == 1)
    flaug['vflip'] = (np.random.randint(2) == 1)
    flaug['elastic'] = (np.random.randint(2) == 1)
    flaug['crop'] = (np.random.randint(2) == 1)
    flaug['gaussiannoise'] = (np.random.randint(2) == 1)

    for aug in flaug :
        flaug[aug] = flaug[aug] if aug in augment else False

    img = img if not flaug['hflip'] else aug_hflip(img)
    img = img if not flaug['vflip'] else aug_vflip(img)
    img = img if not flaug['elastic'] else aug_elastic(img)
    img = img if not flaug['crop'] else aug_crop(img)
    img = img if not flaug['gaussiannoise'] else aug_gaussiannoise(img)

    for item in img :
        img[item] = np.ascontiguousarray(img[item])

    return img

