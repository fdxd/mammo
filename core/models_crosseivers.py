from __future__ import print_function

import torch.nn as nn

import torchvision.models as models
import numpy as np

# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)
from vit_pytorch.vit import ViT
from vit_pytorch.vitsa import ViTsa
from pytorch_pretrained_vit import ViT as preViT
import torch.nn.functional as F
import sys


from torchvision import models
import bit_pytorch.models as bit_models

from torchsummary import summary

# from pytorch_model_summary import summary
from core.common import (
    print_model_grads,
    create_partial_model,
    set_requires_grad,
    reset_grads,
    remove_blocks_from_model,
)

import torch
import torch
from perceiver_pytorch import Perceiver
from core.perceiver_mv.perceiver_mv import PerceiverMV
from core.perceiver_mv.crosseiver_mv import CrosseiverMV
from einops.einops import rearrange


class Crosseiver01(nn.Module):
    def __init__(self, config):
        super(Crosseiver01, self).__init__()

        perceivers_layers_num = (
            2
            if "perceivers_layers_num" not in config.keys()
            else config["perceivers_layers_num"]
        )
        dropout = 0.0 if "dropout" not in config.keys() else config["dropout"]
        max_freq = 10.0 if "max_freq" not in config.keys() else config["max_freq"]
        self.multi_loss = (
            False if "multi_loss" not in config.keys() else config["multi_loss"]
        )
        self.share_weights = (
            False if "share_weights" not in config.keys() else config["share_weights"]
        )
        self.num_stages = (
            1 if "num_stages" not in config.keys() else config["num_stages"]
        )
        # assert(perceivers_layers_num >self.num_stages)
        self.min_resolution = 128
        self.patch_size = 32
        self.patch_step = 8

        self.model = CrossiverMV(
            input_channels=self.patch_size
            * self.patch_size,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=256,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=512,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2,  # number of self attention blocks per cross attention
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()  # pos_weight=class_weight[1]

    def get_label(self, label_):
        lcc, _ = label_["label_cc"].max(dim=1)
        lml, _ = label_["label_ml"].max(dim=1)
        label, _ = torch.cat((lcc[None, :], lml[None, :]), axis=0).max(axis=0)
        return label

    def criterion(self, output, label_):
        label = self.get_label(label_)
        loss = self.loss_fn(output, label.type(torch.long))
        return loss

    def accuracy(self, output_, label_):
        label = self.get_label(label_)
        label = label.detach().cpu()

        output = output_.detach().cpu()
        output = torch.argmax(output, dim=1)

        acc = (output == label).float().mean()

        ref = label.long()
        out = output.long()

        return acc, ref, out

    def data_fold(self, x1, dsize=1024, patch_size=32, patch_step=8):
        x1 = F.interpolate(x1, (dsize, dsize), mode="bicubic", align_corners=True)
        fx1 = nn.functional.unfold(
            x1, kernel_size=(patch_size, patch_size), stride=patch_step
        )
        fx1 = rearrange(fx1, "n (c ww) l -> n l ww c", ww=patch_size**2)
        fx1 = fx1.permute(0, 1, 3, 2)
        return fx1

    def forward(self, x):
        x1 = x["xcc"]
        x2 = x["xml"]

        y = []
        for stage_idx in range(self.num_stages):
            resolution = round(self.min_resolution * pow(2, stage_idx))
            fx1 = self.data_fold(
                x1,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
            )
            fx2 = self.data_fold(
                x2,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
            )
            y.append(fx1)
            y.append(fx2)

        out = self.model(
            y, multi_loss=self.multi_loss, share_weights=self.share_weights
        )

        return F.log_softmax(out, dim=1)


class Crosseiver02(nn.Module):
    def __init__(self, config):
        super(Crosseiver02, self).__init__()

        perceivers_layers_num = (
            2
            if "perceivers_layers_num" not in config.keys()
            else config["perceivers_layers_num"]
        )
        dropout = 0.0 if "dropout" not in config.keys() else config["dropout"]
        max_freq = 10.0 if "max_freq" not in config.keys() else config["max_freq"]
        self.multi_loss = (
            False if "multi_loss" not in config.keys() else config["multi_loss"]
        )
        self.share_weights = (
            False if "share_weights" not in config.keys() else config["share_weights"]
        )
        self.num_stages = (
            1 if "num_stages" not in config.keys() else config["num_stages"]
        )
        # assert(perceivers_layers_num >self.num_stages)
        self.min_resolution = 128
        self.patch_size = 32
        self.patch_step = 8

        self.model = CrosseiverMV(
            input_channels=self.patch_size
            * self.patch_size,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=256,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=512,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2,  # number of self attention blocks per cross attention
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()  # pos_weight=class_weight[1]

    def get_label(self, label_):
        lcc, _ = label_["label_cc"].max(dim=1)
        lml, _ = label_["label_ml"].max(dim=1)
        label, _ = torch.cat((lcc[None, :], lml[None, :]), axis=0).max(axis=0)
        return label

    def criterion(self, output, label_):
        label = self.get_label(label_)
        loss = self.loss_fn(output, label.type(torch.long))
        return loss

    def accuracy(self, output_, label_):
        label = self.get_label(label_)
        label = label.detach().cpu()

        output = output_.detach().cpu()
        output = torch.argmax(output, dim=1)

        acc = (output == label).float().mean()

        ref = label.long()
        out = output.long()

        return acc, ref, out

    def data_fold(self, x1, dsize=1024, patch_size=32, patch_step=8):
        x1 = F.interpolate(x1, (dsize, dsize), mode="bicubic", align_corners=True)
        fx1 = nn.functional.unfold(
            x1, kernel_size=(patch_size, patch_size), stride=patch_step
        )
        fx1 = rearrange(fx1, "n (c ww) l -> n l ww c", ww=patch_size**2)
        fx1 = fx1.permute(0, 1, 3, 2)
        return fx1

    def forward(self, x):
        x1 = x["xcc"]
        x2 = x["xml"]

        data1 = []
        data2 = []
        for stage_idx in range(self.num_stages):
            resolution = round(self.min_resolution * pow(2, stage_idx))
            fx1 = self.data_fold(
                x1,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
            )
            fx2 = self.data_fold(
                x2,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
            )
            data1.append(fx1)
            data2.append(fx2)

        out = self.model(
            data1, data2, multi_loss=self.multi_loss, share_weights=self.share_weights
        )

        return F.log_softmax(out, dim=1)


from core.common import get_params_stats


class Crosseiver03(nn.Module):
    def __init__(self, config):
        super(Crosseiver03, self).__init__()

        self.create_model(config)

    def create_model(self, config):
        perceivers_layers_num = (
            2
            if "perceivers_layers_num" not in config.keys()
            else config["perceivers_layers_num"]
        )
        dropout = 0.0 if "dropout" not in config.keys() else config["dropout"]
        max_freq = 10.0 if "max_freq" not in config.keys() else config["max_freq"]
        self.multi_loss = (
            False if "multi_loss" not in config.keys() else config["multi_loss"]
        )
        self.share_weights = (
            False if "share_weights" not in config.keys() else config["share_weights"]
        )
        self.num_stages = (
            1 if "num_stages" not in config.keys() else config["num_stages"]
        )
        self.use_masks = (
            True if "use_masks" not in config.keys() else config["use_masks"]
        )
        self.min_resolution = (
            128 if "min_resolution" not in config.keys() else config["min_resolution"]
        )

        # assert(perceivers_layers_num >self.num_stages)
        self.patch_size = 32
        self.patch_step = 8
        self.qpatch_size = 16

        self.model = CrosseiverMV(
            input_channels=self.patch_size
            * self.patch_size,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=256,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=1024,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2,  # number of self attention blocks per cross attention
            query_dim=self.min_resolution
            * 2
            * 10,  # 10 - number of maximum mask layers at input
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()  # pos_weight=class_weight[1]

        total_params, total_train_params = get_params_stats(self)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

    def get_label(self, label_):
        lcc, _ = label_["label_cc"].max(dim=1)
        lml, _ = label_["label_ml"].max(dim=1)
        label, _ = torch.cat((lcc[None, :], lml[None, :]), axis=0).max(axis=0)
        return label

    def criterion(self, output, label_):
        label = self.get_label(label_)
        loss = self.loss_fn(output, label.type(torch.long))
        return loss

    def accuracy(self, output_, label_):
        label = self.get_label(label_)
        label = label.detach().cpu()

        output = output_.detach().cpu()
        output = torch.argmax(output, dim=1)

        acc = (output == label).float().mean()

        ref = label.long()
        out = output.long()

        return acc, ref, out

    def data_fold(self, o1, dsize=1024, patch_size=32, patch_step=8, use_diff=False):
        osize = o1.shape[-1]

        if use_diff:
            x1 = F.interpolate(o1, (dsize, dsize), mode="bicubic", align_corners=True)
            y1 = o1 - F.interpolate(
                x1, (osize, osize), mode="bicubic", align_corners=True
            )
        else:
            y1 = o1
        x1 = F.interpolate(y1, (dsize, dsize), mode="bicubic", align_corners=True)
        fx1 = nn.functional.unfold(
            x1, kernel_size=(patch_size, patch_size), stride=patch_step
        )
        fx1 = rearrange(fx1, "n (c ww) l -> n l ww c", ww=patch_size**2)
        fx1 = fx1.permute(0, 1, 3, 2)
        return fx1

    def base_fold(self, x1):
        fx1 = self.data_fold(
            x1,
            dsize=self.min_resolution,
            patch_size=self.qpatch_size,
            patch_step=self.qpatch_size,
        )
        return fx1.squeeze(2)

    def masks_fold(self, x1):
        fx1 = self.data_fold(
            x1,
            dsize=self.min_resolution * 2,
            patch_size=self.qpatch_size,
            patch_step=self.qpatch_size,
        )
        return fx1.squeeze(2)

    def forward(self, x):
        x1 = x["xcc"]
        x2 = x["xml"]

        s1 = x["scc"]
        s2 = x["sml"]

        query1 = self.masks_fold(s1)
        query2 = self.masks_fold(s2)
        query1 = query1.view(query1.shape[0], query1.shape[1], -1)
        query2 = query2.view(query2.shape[0], query2.shape[1], -1)

        data1 = []
        data2 = []
        for stage_idx in range(self.num_stages):
            resolution = round(self.min_resolution * pow(2, stage_idx))
            fx1 = self.data_fold(
                x1,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
            )
            fx2 = self.data_fold(
                x2,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
            )
            data1.append(fx1)
            data2.append(fx2)

        out = self.model(
            data1,
            data2,
            query1,
            query2,
            multi_loss=self.multi_loss,
            share_weights=self.share_weights,
        )

        return F.log_softmax(out, dim=1)


class Crosseiver04(Crosseiver03):
    def __init__(self, config):
        super(Crosseiver04, self).__init__(config)

    def forward(self, x):
        x1 = x["xcc"]
        x2 = x["xml"]

        s1 = x["scc"]
        s2 = x["sml"]

        query1 = self.masks_fold(s1)
        query2 = self.masks_fold(s2)
        query1 = query1.view(query1.shape[0], query1.shape[1], -1)
        query2 = query2.view(query2.shape[0], query2.shape[1], -1)

        data1 = []
        data2 = []
        for stage_idx in range(self.num_stages):
            resolution = round(self.min_resolution * pow(2, stage_idx))
            fx1 = self.data_fold(
                x1,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
                use_diff=True,
            )
            fx2 = self.data_fold(
                x2,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
                use_diff=True,
            )
            data1.append(fx1)
            data2.append(fx2)

        out = self.model(
            data1,
            data2,
            query1,
            query2,
            multi_loss=self.multi_loss,
            share_weights=self.share_weights,
        )

        return F.log_softmax(out, dim=1)


class Crosseiver05(Crosseiver03):
    def __init__(self, config):
        super(Crosseiver05, self).__init__(config)

    def forward(self, x):
        x1 = x["xcc"]
        x2 = x["xml"]

        if self.use_masks:
            s1, s2 = x["scc"], x["sml"]
            query1 = self.masks_fold(s1)
            query2 = self.masks_fold(s2)
            query1 = query1.view(query1.shape[0], query1.shape[1], -1)
            query2 = query2.view(query2.shape[0], query2.shape[1], -1)
        else:
            query1, query2 = None, None

        data1 = []
        data2 = []
        for stage_idx in range(self.num_stages):
            resolution = round(self.min_resolution * pow(2, stage_idx))
            fx1 = self.data_fold(
                x1,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
                use_diff=(stage_idx > 0),
            )  # diff happends only if (stage_idx>0)
            fx2 = self.data_fold(
                x2,
                dsize=resolution,
                patch_size=self.patch_size,
                patch_step=self.patch_step,
                use_diff=(stage_idx > 0),
            )
            data1.append(fx1)
            data2.append(fx2)

        out = self.model(
            data1,
            data2,
            query1,
            query2,
            multi_loss=self.multi_loss,
            share_weights=self.share_weights,
        )

        return F.log_softmax(out, dim=1)


class Crosseiver06(Crosseiver03):
    def __init__(self, config):
        super(Crosseiver06, self).__init__(config)

    def create_model(self, config):
        perceivers_layers_num = (
            2
            if "perceivers_layers_num" not in config.keys()
            else config["perceivers_layers_num"]
        )
        dropout = 0.0 if "dropout" not in config.keys() else config["dropout"]
        max_freq = 10.0 if "max_freq" not in config.keys() else config["max_freq"]
        self.multi_loss = (
            False if "multi_loss" not in config.keys() else config["multi_loss"]
        )
        self.share_weights = (
            False if "share_weights" not in config.keys() else config["share_weights"]
        )
        self.num_stages = (
            1 if "num_stages" not in config.keys() else config["num_stages"]
        )
        # assert(perceivers_layers_num >self.num_stages)
        self.min_resolution = 128
        self.patch_size = 32
        self.patch_step = 8
        self.qpatch_size = 16

        self.model = CrosseiverMV(
            input_channels=self.patch_size
            * self.patch_size,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=256,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=1024,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=64,  # number of dimensions per cross attention head
            latent_dim_head=64,  # number of dimensions per latent self attention head
            num_classes=3,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2,  # number of self attention blocks per cross attention
            query_dim=self.min_resolution
            * 2
            * 10,  # 10 - number of maximum mask layers at input
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()  # pos_weight=class_weight[1]
        self.loss_score = torch.nn.MSELoss()  # pos_weight=class_weight[1]

        total_params, total_train_params = get_params_stats(self)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

    def get_label(self, label_):
        lcc, _ = label_["label_cc"].max(dim=1)
        lml, _ = label_["label_ml"].max(dim=1)
        scc, _ = label_["score_cc"].max(dim=1)
        sml, _ = label_["score_ml"].max(dim=1)
        label, _ = torch.cat((lcc[None, :], lml[None, :]), axis=0).max(axis=0)
        score, _ = torch.cat((scc[None, :], sml[None, :]), axis=0).max(axis=0)
        return label, score

    def criterion(self, output, label_):
        label, score = self.get_label(label_)

        plabel = output[:, 0:2]
        pscore = output[:, 2]
        loss = self.loss_fn(plabel, label.type(torch.long)) + self.loss_score(
            pscore, score.type(torch.float)
        )
        return loss

    def accuracy(self, output_, label_):
        label, score = self.get_label(label_)
        label = label.detach().cpu()

        output = output_.detach().cpu()
        plabel = output[:, 0:2]
        plabel = torch.argmax(plabel, dim=1)

        acc = (plabel == label).float().mean()

        ref = label.long()
        out = plabel.long()

        return acc, ref, out


from core.models_bitpairs import BitFeatures, BitFeatures02


class CrosseiverBiT01(Crosseiver03):
    def __init__(self, config):
        super(CrosseiverBiT01, self).__init__(config)

    def create_model(self, config):
        perceivers_layers_num = (
            2
            if "perceivers_layers_num" not in config.keys()
            else config["perceivers_layers_num"]
        )
        dropout = 0.0 if "dropout" not in config.keys() else config["dropout"]
        max_freq = 10.0 if "max_freq" not in config.keys() else config["max_freq"]
        self.multi_loss = (
            False if "multi_loss" not in config.keys() else config["multi_loss"]
        )
        self.share_weights = (
            False if "share_weights" not in config.keys() else config["share_weights"]
        )
        self.num_stages = (
            1 if "num_stages" not in config.keys() else config["num_stages"]
        )
        self.use_masks = (
            True if "use_masks" not in config.keys() else config["use_masks"]
        )
        self.query_dim = (
            None if "query_dim" not in config.keys() else config["query_dim"]
        )
        self.internal_dim = (
            64 if "internal_dim" not in config.keys() else config["internal_dim"]
        )

        self.use_query = False if self.query_dim is None else True

        # assert(perceivers_layers_num >self.num_stages)
        # self.patch_size = 32
        # self.patch_step = 8
        # self.qpatch_size = 16

        self.model = CrosseiverMV(
            input_channels=256,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=8,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=self.internal_dim,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=8,  # number of heads for latent self attention, 8
            cross_dim_head=self.internal_dim,  # number of dimensions per cross attention head
            latent_dim_head=self.internal_dim,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=2,  # number of self attention blocks per cross attention
            query_dim=self.query_dim,
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()  # pos_weight=class_weight[1]

        total_params, total_train_params = get_params_stats(self)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

        self.features = BitFeatures(config)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

    def folding(self, q, dsize=256):
        from math import isqrt

        fq = []
        for iq in q:
            b, c, w, h = iq.shape
            if c > dsize:
                fx1 = rearrange(iq, "b (c d) w h -> b d c w h", d=dsize)
                fx1 = fx1.reshape(b, dsize, -1)
            else:
                patch_size = dsize // c
                fx1 = nn.functional.unfold(
                    iq, kernel_size=(patch_size, 1), stride=(patch_size, 1)
                )
            fx1 = fx1.permute(0, 2, 1)
            fx1 = fx1.unsqueeze(2)
            fq.append(fx1)

        return fq

    def forward(self, x):
        x1 = x["xcc"]
        x2 = x["xml"]

        s1 = x["scc"]
        s2 = x["sml"]

        q1f, q2f = None, None
        if self.use_query:
            q1f = self.data_fold(x1, patch_size=64, patch_step=64)
            q2f = self.data_fold(x2, patch_size=64, patch_step=64)
            q1f, q2f = q1f.squeeze(2), q2f.squeeze(2)

        x1f = self.features(x1)
        x2f = self.features(x2)

        x1ff = self.folding(x1f)
        x2ff = self.folding(x2f)

        out = self.model(
            x1ff,
            x2ff,
            q1f,
            q2f,
            multi_loss=self.multi_loss,
            share_weights=self.share_weights,
        )

        return F.log_softmax(out, dim=1)


class CrosseiverBiT02(Crosseiver03):  # custom
    def __init__(self, config):
        super(CrosseiverBiT02, self).__init__(config)

    def create_model(self, config):
        perceivers_layers_num = (
            2
            if "perceivers_layers_num" not in config.keys()
            else config["perceivers_layers_num"]
        )
        dropout = 0.0 if "dropout" not in config.keys() else config["dropout"]
        max_freq = 10.0 if "max_freq" not in config.keys() else config["max_freq"]
        self.multi_loss = (
            False if "multi_loss" not in config.keys() else config["multi_loss"]
        )
        self.share_weights = (
            False if "share_weights" not in config.keys() else config["share_weights"]
        )
        # self.num_stages = 1 if 'num_stages' not in config.keys() else config['num_stages']
        self.use_masks = (
            True if "use_masks" not in config.keys() else config["use_masks"]
        )
        self.query_dim = (
            None if "query_dim" not in config.keys() else config["query_dim"]
        )
        self.internal_dim = (
            64 if "internal_dim" not in config.keys() else config["internal_dim"]
        )
        self.maxab = 10 if "maxab" not in config.keys() else config["maxab"]

        self.use_query = False if self.query_dim is None else True

        # assert(perceivers_layers_num >self.num_stages)
        # self.patch_size = 32
        # self.patch_step = 8
        # self.qpatch_size = 16

        self.model = CrosseiverMV(
            input_channels=256,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=8,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=self.internal_dim,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=1,  # number of heads for latent self attention, 8
            cross_dim_head=self.internal_dim,  # number of dimensions per cross attention head
            latent_dim_head=self.internal_dim,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=1,  # number of self attention blocks per cross attention
            query_dim=self.query_dim,
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()  # pos_weight=class_weight[1]

        total_params, total_train_params = get_params_stats(self)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

        self.features = BitFeatures(config)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

    def folding(self, q, dsize=256):
        from math import isqrt

        fq = []
        for iq in q:
            b, c, w, h = iq.shape
            if c > dsize:
                fx1 = rearrange(iq, "b (c d) w h -> b d c w h", d=dsize)
                fx1 = fx1.reshape(b, dsize, -1)
            else:
                patch_size = dsize // c
                fx1 = nn.functional.unfold(
                    iq, kernel_size=(patch_size, 1), stride=(patch_size, 1)
                )
            fx1 = fx1.permute(0, 2, 1)
            fx1 = fx1.unsqueeze(2)
            fq.append(fx1)

        return fq

    def forward(self, x):
        x1 = x["xcc"]
        x2 = x["xml"]

        s1 = x["scc"]
        s2 = x["sml"]

        meta1 = x["meta_cc"]
        meta2 = x["meta_mlo"]

        q1f, q2f = None, None
        if self.use_query:
            q1f = self.data_fold(x1, patch_size=64, patch_step=64)
            q2f = self.data_fold(x2, patch_size=64, patch_step=64)
            q1f, q2f = q1f.squeeze(2), q2f.squeeze(2)

        x1f = self.features(x1)  # x1f[0] shape torch.Size([1, 2048, 1, 1])
        x2f = self.features(x2)

        x1ff = self.folding(x1f)  # replace the first entry of head with new meta
        x2ff = self.folding(x2f)

        meta_1 = torch.unsqueeze(meta1, 2)
        meta_2 = torch.unsqueeze(meta2, 2)
        # print("before x1ff[0].shape:", x1ff[0].shape)
        # print("before x2ff[0].shape:", x1ff[0].shape)

        x1ff[0] = meta_1
        x2ff[0] = meta_2

        # print("after x1ff[0].shape:", x1ff[0].shape)
        # print("after x2ff[0].shape:", x2ff[0].shape)

        out = self.model(
            x1ff,
            x2ff,
            q1f,
            q2f,
            multi_loss=self.multi_loss,
            share_weights=self.share_weights,
        )

        return F.log_softmax(out, dim=1)


class CrosseiverBiT03(Crosseiver03):
    def __init__(self, config):
        super(CrosseiverBiT03, self).__init__(config)

    def create_model(self, config):
        perceivers_layers_num = (
            2
            if "perceivers_layers_num" not in config.keys()
            else config["perceivers_layers_num"]
        )
        dropout = 0.0 if "dropout" not in config.keys() else config["dropout"]
        max_freq = 10.0 if "max_freq" not in config.keys() else config["max_freq"]
        self.multi_loss = (
            False if "multi_loss" not in config.keys() else config["multi_loss"]
        )
        self.share_weights = (
            False if "share_weights" not in config.keys() else config["share_weights"]
        )
        # self.num_stages = 1 if 'num_stages' not in config.keys() else config['num_stages']
        self.use_masks = (
            True if "use_masks" not in config.keys() else config["use_masks"]
        )
        self.query_dim = (
            None if "query_dim" not in config.keys() else config["query_dim"]
        )
        self.internal_dim = (
            64 if "internal_dim" not in config.keys() else config["internal_dim"]
        )
        self.maxab = 10 if "maxab" not in config.keys() else config["maxab"]

        self.use_query = False if self.query_dim is None else True

        # assert(perceivers_layers_num >self.num_stages)
        # self.patch_size = 32
        # self.patch_step = 8
        # self.qpatch_size = 16

        self.model = CrosseiverMV(
            input_channels=256,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=6,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=8,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=self.internal_dim,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=1,  # number of heads for latent self attention, 8
            cross_dim_head=self.internal_dim,  # number of dimensions per cross attention head
            latent_dim_head=self.internal_dim,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=1,  # number of self attention blocks per cross attention
            query_dim=self.query_dim,
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()  # pos_weight=class_weight[1]

        self.score_nn = nn.Linear(self.maxab, self.internal_dim)

        total_params, total_train_params = get_params_stats(self)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

        self.features = BitFeatures(config)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

    def folding(self, q, dsize=256):
        from math import isqrt

        fq = []
        for iq in q:
            b, c, w, h = iq.shape
            if c > dsize:
                fx1 = rearrange(iq, "b (c d) w h -> b d c w h", d=dsize)
                fx1 = fx1.reshape(b, dsize, -1)
            else:
                patch_size = dsize // c
                fx1 = nn.functional.unfold(
                    iq, kernel_size=(patch_size, 1), stride=(patch_size, 1)
                )
            fx1 = fx1.permute(0, 2, 1)
            fx1 = fx1.unsqueeze(2)
            fq.append(fx1)

        return fq

    def forward(self, x):
        x1 = x["xcc"]
        x2 = x["xml"]
        # print("this is supposed to be 0", x1)
        # print("this is supposed to be 0", x2)

        s1 = x["scc"]
        s2 = x["sml"]

        # Get the assesment score query and
        # match it to the correct size using a neural network
        score_cc = x["score_cc"]
        score_ml = x["score_ml"]
        # print("not supposed to be 0", score_cc)
        # print("not supposed to be 0", score_ml)

        # print(score_cc.shape, file=sys.stderr)
        # d = torch.unsqueeze(score_cc, dim=0)
        # f = torch.unsqueeze(score_cc, dim=1)
        # a = torch.unsqueeze(score_cc, dim=2)
        # print(d.shape,f.shape,a.shape, file=sys.stderr)
        score_cc = torch.unsqueeze(score_cc, dim=1)
        score_ml = torch.unsqueeze(score_ml, dim=1)

        score_cc_nn = self.score_nn(score_cc)
        score_ml_nn = self.score_nn(score_ml)

        q1f, q2f = None, None
        if self.use_query:
            q1f = self.data_fold(x1, patch_size=64, patch_step=64)
            q2f = self.data_fold(x2, patch_size=64, patch_step=64)
            q1f, q2f = q1f.squeeze(2), q2f.squeeze(2)

        x1f = self.features(x1)
        x2f = self.features(x2)

        x1ff = self.folding(x1f)
        x2ff = self.folding(x2f)

        out = self.model(
            x1ff,
            x2ff,
            score_cc_nn,
            score_ml_nn,
            multi_loss=self.multi_loss,
            share_weights=self.share_weights,
        )

        return F.log_softmax(out, dim=1)


class PositionalEncoder(nn.Module):
    r"""
    Sine-cosine positional encoder for input points.
    """

    def __init__(self, d_input: int, n_freqs: int, log_space: bool = False):
        super().__init__()
        self.d_input = d_input
        self.n_freqs = n_freqs
        self.log_space = log_space
        self.d_output = d_input * (1 + 2 * self.n_freqs)
        self.embed_fns = [lambda x: x]

        # Define frequencies in either linear or log scale
        if self.log_space:
            freq_bands = 2.0 ** torch.linspace(0.0, self.n_freqs - 1, self.n_freqs)
        else:
            freq_bands = torch.linspace(
                2.0**0.0, 2.0 ** (self.n_freqs - 1), self.n_freqs
            )

        # Alternate sin and cos
        for freq in freq_bands:
            self.embed_fns.append(lambda x, freq=freq: torch.sin(x * freq))
            self.embed_fns.append(lambda x, freq=freq: torch.cos(x * freq))

    def forward(self, x) -> torch.Tensor:
        r"""
        Apply positional encoding to input.
        """
        return torch.cat([fn(x) for fn in self.embed_fns], dim=-1)


class CrosseiverBiT10(Crosseiver03):
    def __init__(self, config):
        super(CrosseiverBiT10, self).__init__(config)

    def create_model(self, config):
        perceivers_layers_num = (
            2
            if "perceivers_layers_num" not in config.keys()
            else config["perceivers_layers_num"]
        )
        dropout = 0.0 if "dropout" not in config.keys() else config["dropout"]
        max_freq = 10.0 if "max_freq" not in config.keys() else config["max_freq"]
        num_freq_bands = (
            6 if "num_freq_bands" not in config.keys() else config["num_freq_bands"]
        )
        num_latents = 8 if "num_latents" not in config.keys() else config["num_latents"]
        self.input_channels = (
            256 if "input_channels" not in config.keys() else config["input_channels"]
        )

        self.multi_loss = (
            False if "multi_loss" not in config.keys() else config["multi_loss"]
        )
        self.share_weights = (
            False if "share_weights" not in config.keys() else config["share_weights"]
        )
        # self.num_stages = 1 if 'num_stages' not in config.keys() else config['num_stages']
        self.use_masks = (
            True if "use_masks" not in config.keys() else config["use_masks"]
        )
        self.query_dim = (
            None if "query_dim" not in config.keys() else config["query_dim"]
        )
        self.internal_dim = (
            64 if "internal_dim" not in config.keys() else config["internal_dim"]
        )
        self.faetures_type = (
            "BitFeatures"
            if "faetures_type" not in config.keys()
            else config["faetures_type"]
        )
        self.ff_mult = 4 if "ff_mult" not in config.keys() else config["ff_mult"]

        self.use_query = False if self.query_dim is None else True

        self.model = CrosseiverMV(
            input_channels=self.input_channels,  # number of channels for each token of the input
            input_axis=2,  # number of axis for input data (2 for images, 3 for video)
            num_freq_bands=num_freq_bands,  # number of freq bands, with original value (2 * K + 1)
            max_freq=max_freq,  # maximum frequency, hyperparameter depending on how fine the data is
            depth=perceivers_layers_num,  # depth of net. The shape of the final attention mechanism will be:
            #   depth * (cross attention -> self_per_cross_attn * self attention)
            num_latents=num_latents,  # number of latents, or induced set points, or centroids. different papers giving it different names
            latent_dim=self.internal_dim,  # latent dimension
            cross_heads=1,  # number of heads for cross attention. paper said 1
            latent_heads=1,  # number of heads for latent self attention, 8
            cross_dim_head=self.internal_dim,  # number of dimensions per cross attention head
            latent_dim_head=self.internal_dim,  # number of dimensions per latent self attention head
            num_classes=2,  # output number of classes
            attn_dropout=dropout,
            ff_dropout=dropout,
            weight_tie_layers=False,  # whether to weight tie layers (optional, as indicated in the diagram)
            fourier_encode_data=True,
            # whether to auto-fourier encode the data, using the input_axis given. defaults to True, but can be turned off if you are fourier encoding the data yourself
            self_per_cross_attn=1,  # number of self attention blocks per cross attention
            query_dim=self.query_dim,
            ff_mult=self.ff_mult,
        )

        self.loss_fn = torch.nn.CrossEntropyLoss()  # pos_weight=class_weight[1]

        total_params, total_train_params = get_params_stats(self)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

        if self.faetures_type == "BitFeatures":
            self.features = BitFeatures(config)
        elif self.faetures_type == "BitFeatures02":
            self.features = BitFeatures02(config)
        else:
            self.features = BitFeatures(config)
        print(
            f"Total params : {total_params:,}, Train params : {total_train_params:,} "
        )

        self.PE = PositionalEncoder(d_input=3, n_freqs=64)

        print_model_grads(self)

    def folding(self, q, dsize=256):
        from math import isqrt

        fq = []
        for iq in q:
            b, c, w, h = iq.shape
            if c > dsize:
                fx1 = rearrange(iq, "b (c d) w h -> b d c w h", d=dsize)
                fx1 = fx1.reshape(b, dsize, -1)
            else:
                patch_size = dsize // c
                fx1 = nn.functional.unfold(
                    iq, kernel_size=(patch_size, 1), stride=(patch_size, 1)
                )
            fx1 = fx1.permute(0, 2, 1)
            fx1 = fx1.unsqueeze(2)
            fq.append(fx1)

        return fq

    def get_segments(self, x, s, lim=4096):
        o = []
        for (ibx, ibs) in zip(x, s):
            v = []
            for idx, layer in enumerate(ibs):
                i, j = torch.where(layer > 0)
                if len(i) > 0 and len(j) > 0:
                    v.append(
                        torch.stack(
                            (
                                ibx[0, i, j],
                                i / layer.shape[0],
                                j / layer.shape[1],
                                idx * torch.ones_like(i),
                            ),
                            dim=-1,
                        )
                    )
            v = torch.cat(v, dim=0)
            idxr = torch.randperm(v.shape[0])
            idxr, _ = torch.sort(idxr[0:lim])
            v = v[idxr, :]
            o.append(v)

        maxlen = max([i.shape[0] for i in o])
        op = [
            torch.nn.functional.pad(
                i, (0, 0, 0, maxlen - i.shape[0]), mode="constant", value=-1.0
            )
            for i in o
        ]

        out = torch.stack(op, dim=0)
        return out

    def forward(self, x):
        x1 = x["xcc"]
        x2 = x["xml"]

        s1 = x["scc"]
        s2 = x["sml"]

        x1f = self.features(x1)
        x2f = self.features(x2)

        x1ff = self.folding(x1f, dsize=self.input_channels)
        x2ff = self.folding(x2f, dsize=self.input_channels)

        segs1 = self.get_segments(x1, s1)
        segs2 = self.get_segments(x2, s2)
        segs1_ = torch.cat((segs1, self.PE(segs1[:, :, 1:4])), dim=-1)
        segs1_ = segs1_[:, :, : self.input_channels].unsqueeze(1)
        segs2_ = torch.cat((segs2, self.PE(segs2[:, :, 1:4])), dim=-1)
        segs2_ = segs2_[:, :, : self.input_channels].unsqueeze(1)

        x1ff.append(segs1_)
        x2ff.append(segs2_)

        out = self.model(
            x1ff,
            x2ff,
            None,
            None,
            multi_loss=self.multi_loss,
            share_weights=self.share_weights,
        )

        return F.log_softmax(out, dim=1)
