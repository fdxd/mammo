
from __future__ import print_function

import torch.nn as nn

import torchvision.models as models
import numpy as np
# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)
from vit_pytorch.vit import ViT
from vit_pytorch.vitsa import ViTsa
from pytorch_pretrained_vit import ViT as preViT
import torch.nn.functional as F


from torchvision import models
import bit_pytorch.models as bit_models

from torchsummary import summary
#from pytorch_model_summary import summary
from core.common import print_model_grads,create_partial_model,set_requires_grad,reset_grads,remove_blocks_from_model

import torch



class VitPairs02(nn.Module):
    def __init__(self,config):
        super(VitPairs02, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        last_fc_size = 768
        self.model1 = preViT('B_16_imagenet1k', pretrained=True)
        self.model1.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        self.model2 = preViT('B_16_imagenet1k', pretrained=True)
        self.model2.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        summary(self.model1, (3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model1)
        print_model_grads(self.model2)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        set_requires_grad(self, 'model1.transformer.blocks.10', True)
        set_requires_grad(self, 'model1.transformer.blocks.11', True)
        set_requires_grad(self, 'model1.norm', True)
        set_requires_grad(self, 'model1.fc', True)
        set_requires_grad(self, 'model2.transformer.blocks.10', True)
        set_requires_grad(self, 'model2.transformer.blocks.11', True)
        set_requires_grad(self, 'model2.norm', True)
        set_requires_grad(self, 'model2.fc', True)
        set_requires_grad(self, 'norm', True)
        set_requires_grad(self, 'classifier', True)

        self.alpha = torch.nn.Parameter(torch.randn(1,last_fc_size), requires_grad=True)

        print_model_grads(self)

        summary(self.model1, ( 3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        layers = ['model1.norm','model2.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))



    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']
        x1 = F.interpolate(x1, (384, 384), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (384, 384), mode='bicubic', align_corners=True)

        out1 = self.model1(x1)
        out2 = self.model2(x2)

        features1 = self._features['model1.norm'][:, 0]
        features2 = self._features['model2.norm'][:, 0]
        alpha = self.alpha.repeat(features1.shape[0], 1)
        features = alpha*features1 + (1-alpha) * features2

        features = self.norm(features)
        out = self.classifier(features)

        return out



class VitPairs02A(nn.Module):
    def __init__(self,config):
        super(VitPairs02A, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        self.use_cross = True if 'use_cross' not in config.keys() else config['use_cross']
        self.freeze_cross = False if 'freeze_cross' not in config.keys() else config['freeze_cross']
        self.unfreeze_blocks = [10,11] if 'unfreeze_blocks' not in config.keys() else config['unfreeze_blocks']
        last_fc_size = 768
        self.model1 = preViT('B_16_imagenet1k', pretrained=True)
        self.model1.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        self.model2 = preViT('B_16_imagenet1k', pretrained=True)
        self.model2.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        summary(self.model1, (3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        self.cross_block = CrossTransformer(d_model=last_fc_size)

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model1)
        print_model_grads(self.model2)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        if 10 in self.unfreeze_blocks :
            set_requires_grad(self, 'model1.transformer.blocks.10', True)
            set_requires_grad(self, 'model2.transformer.blocks.10', True)
        if 11 in self.unfreeze_blocks:
            set_requires_grad(self, 'model1.transformer.blocks.11', True)
            set_requires_grad(self, 'model2.transformer.blocks.11', True)

        set_requires_grad(self, 'model1.norm', True)
        set_requires_grad(self, 'model1.fc', True)
        set_requires_grad(self, 'model2.norm', True)
        set_requires_grad(self, 'model2.fc', True)
        set_requires_grad(self, 'norm', True)

        set_requires_grad(self, 'classifier', True)

        if self.freeze_cross :
            set_requires_grad(self, 'cross_block', False)
        else :
            set_requires_grad(self, 'cross_block', True)

        self.alpha = torch.nn.Parameter(torch.randn(1,last_fc_size), requires_grad=True)

        print_model_grads(self)

        summary(self.model1, ( 3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        layers = ['model1.norm','model2.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))



    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']
        x1 = F.interpolate(x1, (384, 384), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (384, 384), mode='bicubic', align_corners=True)

        out1 = self.model1(x1)
        out2 = self.model2(x2)

        #features1 = self._features['model1.norm'][:, 0]
        #features2 = self._features['model2.norm'][:, 0]
        features1 = self._features['model1.norm']
        features2 = self._features['model2.norm']

        if self.use_cross :
            features1,features2 = self.cross_block(features1,features2)

        features1 = torch.max(features1,dim=1)[0]
        features2 = torch.max(features2,dim=1)[0]

        alpha = self.alpha.repeat(features1.shape[0], 1)
        features = alpha*features1 + (1-alpha) * features2

        features = self.norm(features)
        out = self.classifier(features)

        return out


class VitPairs03(nn.Module):
    def __init__(self,config):
        super(VitPairs03, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        last_fc_size = 768
        self.model1 = preViT('B_16_imagenet1k', pretrained=True)
        self.model1.fc =  nn.Linear(in_features=last_fc_size, out_features=last_fc_size, bias=True)

        self.model2 = preViT('B_16_imagenet1k', pretrained=True)
        self.model2.fc =  nn.Linear(in_features=last_fc_size, out_features=last_fc_size, bias=True)

        summary(self.model1, (3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        self.compose_norm = nn.LayerNorm(last_fc_size*2, eps=1e-6)
        self.compose_layer = nn.Linear(last_fc_size*2, out_features=last_fc_size, bias=True)

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model1)
        print_model_grads(self.model2)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        set_requires_grad(self, 'model1.transformer.blocks.11', True)
        set_requires_grad(self, 'model1.norm', True)
        set_requires_grad(self, 'model1.fc', True)
        set_requires_grad(self, 'model2.transformer.blocks.11', True)
        set_requires_grad(self, 'model2.norm', True)
        set_requires_grad(self, 'model2.fc', True)
        set_requires_grad(self, 'compose_norm', True)
        set_requires_grad(self, 'compose_layer', True)
        set_requires_grad(self, 'norm', True)
        set_requires_grad(self, 'classifier', True)

        print_model_grads(self)

        summary(self.model1, ( 3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')


    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']
        x1 = F.interpolate(x1, (384, 384), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (384, 384), mode='bicubic', align_corners=True)

        features1 = self.model1(x1)
        features2 = self.model2(x2)

        features = self.compose_norm(torch.cat((features1,features2),axis=1))
        features = self.compose_layer(features)

        features = self.norm(features)
        out = self.classifier(features)

        return out




class VitPairs01A(nn.Module):
    def __init__(self,config):
        super(VitPairs01A, self).__init__()
        #bug fixing of VitPairs01 - classifier an norm were freezed !!!!
        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        last_fc_size = 768
        self.model1 = preViT('B_16_imagenet1k', pretrained=True)
        self.model1.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        self.model2 = preViT('B_16_imagenet1k', pretrained=True)
        self.model2.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        summary(self.model1, (3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model1)
        print_model_grads(self.model2)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        set_requires_grad(self, 'model1.transformer.blocks.11', True)
        set_requires_grad(self, 'model1.transformer.blocks.11.attn', False)
        set_requires_grad(self, 'model1.norm', True)
        set_requires_grad(self, 'model1.fc', True)
        set_requires_grad(self, 'model2.transformer.blocks.11', True)
        set_requires_grad(self, 'model2.transformer.blocks.11.attn', False)
        set_requires_grad(self, 'model2.norm', True)
        set_requires_grad(self, 'model2.fc', True)
        set_requires_grad(self, 'norm', True)
        set_requires_grad(self, 'classifier', True)

        print_model_grads(self)

        summary(self.model1, ( 3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        layers = ['model1.norm','model2.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))


        if self.alternative_mode :
            self.count = 0
            self.step_size = self.alternative_step_size
            self.block_count = 0
            self.base_block1 = 'model1.transformer.blocks.11.attn'
            self.alternative_blocks1 = ['model1.transformer.blocks.11.attn.proj_q',
                                       'model1.transformer.blocks.11.attn.proj_k',
                                       'model1.transformer.blocks.11.attn.proj_v']
            self.base_block2 = 'model2.transformer.blocks.11.attn'
            self.alternative_blocks2 = ['model2.transformer.blocks.11.attn.proj_q',
                                       'model2.transformer.blocks.11.attn.proj_k',
                                       'model2.transformer.blocks.11.attn.proj_v']

    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def alternative_step(self):
        self.count = self.count + 1
        if self.count == self.step_size :
            self.count = 0
            set_requires_grad(self, self.base_block1, False)
            set_requires_grad(self, self.alternative_blocks1[self.block_count], True)
            set_requires_grad(self, self.base_block2, False)
            set_requires_grad(self, self.alternative_blocks2[self.block_count], True)
            self.block_count = self.block_count + 1
            if self.block_count == len(self.alternative_blocks1) :
                self.block_count = 0

    def forward(self, x):
        if self.alternative_mode :
            self.alternative_step()
        x1 = x['img1']
        x2 = x['img2']
        x1 = F.interpolate(x1, (384, 384), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (384, 384), mode='bicubic', align_corners=True)

        out1 = self.model1(x1)
        out2 = self.model2(x2)

        features1 = self._features['model1.norm'][:, 0]
        features2 = self._features['model2.norm'][:, 0]

        features = features1 + features2

        features = self.norm(features)
        out = self.classifier(features)

        return out


class VitPairs01(nn.Module):
    def __init__(self,config):
        super(VitPairs01, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        last_fc_size = 768
        self.model1 = preViT('B_16_imagenet1k', pretrained=True)
        self.model1.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        self.model2 = preViT('B_16_imagenet1k', pretrained=True)
        self.model2.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        summary(self.model1, (3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model1)
        print_model_grads(self.model2)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        set_requires_grad(self, 'model1.transformer.blocks.11', True)
        set_requires_grad(self, 'model1.transformer.blocks.11.attn', False)
        set_requires_grad(self, 'model1.norm', True)
        set_requires_grad(self, 'model1.fc', True)
        set_requires_grad(self, 'model2.transformer.blocks.11', True)
        set_requires_grad(self, 'model2.transformer.blocks.11.attn', False)
        set_requires_grad(self, 'model2.norm', True)
        set_requires_grad(self, 'model2.fc', True)

        print_model_grads(self)

        summary(self.model1, ( 3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        layers = ['model1.norm','model2.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))


        if self.alternative_mode :
            self.count = 0
            self.step_size = self.alternative_step_size
            self.block_count = 0
            self.base_block1 = 'model1.transformer.blocks.11.attn'
            self.alternative_blocks1 = ['model1.transformer.blocks.11.attn.proj_q',
                                       'model1.transformer.blocks.11.attn.proj_k',
                                       'model1.transformer.blocks.11.attn.proj_v']
            self.base_block2 = 'model2.transformer.blocks.11.attn'
            self.alternative_blocks2 = ['model2.transformer.blocks.11.attn.proj_q',
                                       'model2.transformer.blocks.11.attn.proj_k',
                                       'model2.transformer.blocks.11.attn.proj_v']

    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def alternative_step(self):
        self.count = self.count + 1
        if self.count == self.step_size :
            self.count = 0
            set_requires_grad(self, self.base_block1, False)
            set_requires_grad(self, self.alternative_blocks1[self.block_count], True)
            set_requires_grad(self, self.base_block2, False)
            set_requires_grad(self, self.alternative_blocks2[self.block_count], True)
            self.block_count = self.block_count + 1
            if self.block_count == len(self.alternative_blocks1) :
                self.block_count = 0

    def forward(self, x):
        if self.alternative_mode :
            self.alternative_step()
        x1 = x['img1']
        x2 = x['img2']
        x1 = F.interpolate(x1, (384, 384), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (384, 384), mode='bicubic', align_corners=True)

        out1 = self.model1(x1)
        out2 = self.model2(x2)

        features1 = self._features['model1.norm'][:, 0]
        features2 = self._features['model2.norm'][:, 0]

        features = features1 + features2

        features = self.norm(features)
        out = self.classifier(features)

        return out



class VitPairs04(nn.Module):
    def __init__(self,config):
        super(VitPairs04, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        last_fc_size = 768
        self.model1 = preViT('B_16_imagenet1k', pretrained=True)
        self.model1.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        self.model2 = preViT('B_16_imagenet1k', pretrained=True)
        self.model2.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        summary(self.model1, (3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model1)
        print_model_grads(self.model2)

        set_requires_grad(self, '', False)
        set_requires_grad(self, 'model1.norm', True)
        set_requires_grad(self, 'model1.fc', True)
        set_requires_grad(self, 'model2.norm', True)
        set_requires_grad(self, 'model2.fc', True)
        set_requires_grad(self, 'norm', True)
        set_requires_grad(self, 'classifier', True)
        print_model_grads(self)

        summary(self.model1, ( 3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        layers = ['model1.norm','model2.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))

        if self.alternative_mode :
            self.count_step = 0
            self.step_size = self.alternative_step_size
            self.count_block = 0
            self.count_part = 0

            self.alternative_blocks = []
            self.alternative_blocks.append({'model1.transformer.blocks.10.attn': ['proj_q', 'proj_k', 'proj_v']})
            self.alternative_blocks.append({'model1.transformer.blocks.11.attn': ['proj_q', 'proj_k', 'proj_v']})
            self.alternative_blocks.append({'model2.transformer.blocks.10.attn': ['proj_q', 'proj_k', 'proj_v']})
            self.alternative_blocks.append({'model2.transformer.blocks.11.attn': ['proj_q', 'proj_k', 'proj_v']})

            #self.alternative_blocks_len = [len(block[block.keys()[0]])  for block in self.alternative_blocks]
            self.alternative_step()

    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def alternative_reset(self,value = False):
        for item in self.alternative_blocks :
            self.alternative_block_set(item, value=value)

    def alternative_block_set(self, item,value=False):
        for curr_path in item.keys():
            for curr_part in item[curr_path]:
                set_requires_grad(self, f'{curr_path}.{curr_part}', value)

    def alternative_step(self):
        self.count_step = self.count_step + 1
        if self.count_step == self.step_size :
            self.count_step = 0

            self.alternative_reset(value = False)
            curr_block = self.alternative_blocks[self.count_block]
            curr_path = list(curr_block.keys())[0]
            curr_part = curr_block[curr_path][self.count_part]
            set_requires_grad(self, f'{curr_path}.{curr_part}', True)

            self.count_part +=1
            if self.count_part == len(curr_block[curr_path]):
                self.count_part = 0
                self.count_block += 1
                if self.count_block >= len(self.alternative_blocks) :
                    self.count_block = 0

    def forward(self, x):
        if self.alternative_mode :
            self.alternative_step()
        x1 = x['img1']
        x2 = x['img1']
        x1 = F.interpolate(x1, (384, 384), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (384, 384), mode='bicubic', align_corners=True)

        x1 = x1 if x1.shape[1] == 3 else x1.repeat(1, 3, 1, 1)
        x2 = x2 if x2.shape[1] == 3 else x2.repeat(1, 3, 1, 1)

        out1 = self.model1(x1)
        out2 = self.model2(x2)

        features1 = self._features['model1.norm'][:, 0]
        features2 = self._features['model2.norm'][:, 0]

        features = features1 + features2

        features = self.norm(features)
        out = self.classifier(features)

        return out



class VitPairs05(nn.Module):
    def __init__(self,config):
        super(VitPairs05, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        last_fc_size = 768
        self.model1 = preViT('B_16_imagenet1k', pretrained=True)
        self.model1.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        self.model2 = preViT('B_16_imagenet1k', pretrained=True)
        self.model2.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        summary(self.model1, (3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model1)
        print_model_grads(self.model2)

        set_requires_grad(self, '', False)
        set_requires_grad(self, 'model1.norm', True)
        set_requires_grad(self, 'model1.fc', True)
        set_requires_grad(self, 'model2.norm', True)
        set_requires_grad(self, 'model2.fc', True)
        set_requires_grad(self, 'norm', True)
        set_requires_grad(self, 'classifier', True)
        print_model_grads(self)

        summary(self.model1, ( 3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        layers = ['model1.norm','model2.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))

        if self.alternative_mode :
            self.count_step = 0
            self.step_size = self.alternative_step_size
            self.count_block = 0
            self.count_part = 0

            self.alternative_blocks = []
            self.alternative_blocks.append({'model1.transformer.blocks': ['10', '11']})
            self.alternative_blocks.append({'model2.transformer.blocks': ['10', '11']})

            #self.alternative_blocks_len = [len(block[block.keys()[0]])  for block in self.alternative_blocks]
            self.alternative_step()

    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def alternative_reset(self,value = False):
        for item in self.alternative_blocks :
            self.alternative_block_set(item, value=value)

    def alternative_block_set(self, item,value=False):
        for curr_path in item.keys():
            for curr_part in item[curr_path]:
                set_requires_grad(self, f'{curr_path}.{curr_part}', value)

    def alternative_step(self):
        self.count_step = self.count_step + 1
        if self.count_step == self.step_size :
            self.count_step = 0

            self.alternative_reset(value = False)
            curr_block = self.alternative_blocks[self.count_block]
            curr_path = list(curr_block.keys())[0]
            curr_part = curr_block[curr_path][self.count_part]
            set_requires_grad(self, f'{curr_path}.{curr_part}', True)

            self.count_part +=1
            if self.count_part == len(curr_block[curr_path]):
                self.count_part = 0
                self.count_block += 1
                if self.count_block >= len(self.alternative_blocks) :
                    self.count_block = 0

    def forward(self, x):
        if self.alternative_mode :
            self.alternative_step()
        x1 = x['img1']
        x2 = x['img1']
        x1 = F.interpolate(x1, (384, 384), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (384, 384), mode='bicubic', align_corners=True)

        out1 = self.model1(x1)
        out2 = self.model2(x2)

        features1 = self._features['model1.norm'][:, 0]
        features2 = self._features['model2.norm'][:, 0]

        features = features1 + features2

        features = self.norm(features)
        out = self.classifier(features)

        return out

from core.lofter.transformer import CrossTransformer




class VitPairs06(nn.Module):
    def __init__(self,config):
        super(VitPairs06, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        self.use_cross = True if 'use_cross' not in config.keys() else config['use_cross']
        self.freeze_cross = False if 'freeze_cross' not in config.keys() else config['freeze_cross']
        self.unfreeze_blocks = [10,11] if 'unfreeze_blocks' not in config.keys() else config['unfreeze_blocks']
        last_fc_size = 768
        self.model1 = preViT('B_16_imagenet1k', pretrained=True)
        self.model1.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        self.model2 = preViT('B_16_imagenet1k', pretrained=True)
        self.model2.fc =  nn.Linear(in_features=last_fc_size, out_features=2, bias=True)

        summary(self.model1, (3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        self.cross_block = CrossTransformer(d_model=last_fc_size)

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model1)
        print_model_grads(self.model2)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        if 10 in self.unfreeze_blocks :
            set_requires_grad(self, 'model1.transformer.blocks.10', True)
            set_requires_grad(self, 'model2.transformer.blocks.10', True)
        if 11 in self.unfreeze_blocks:
            set_requires_grad(self, 'model1.transformer.blocks.11', True)
            set_requires_grad(self, 'model2.transformer.blocks.11', True)

        set_requires_grad(self, 'model1.norm', True)
        set_requires_grad(self, 'model1.fc', True)
        set_requires_grad(self, 'model2.norm', True)
        set_requires_grad(self, 'model2.fc', True)
        set_requires_grad(self, 'norm', True)

        set_requires_grad(self, 'classifier', True)

        if self.freeze_cross :
            set_requires_grad(self, 'cross_block', False)
        else :
            set_requires_grad(self, 'cross_block', True)

        self.alpha = torch.nn.Parameter(torch.randn(1,last_fc_size), requires_grad=True)

        print_model_grads(self)

        summary(self.model1, ( 3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        layers = ['model1.norm','model2.norm']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))

        if self.alternative_mode :
            self.count_step = 0
            self.step_size = self.alternative_step_size
            self.count_block = 0
            self.count_part = 0

            self.alternative_blocks = []
            self.alternative_blocks.append({'cross_block.layers': ['0', '1', '2', '3', '4']})

            #self.alternative_blocks_len = [len(block[block.keys()[0]])  for block in self.alternative_blocks]
            self.alternative_step()

    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def alternative_reset(self,value = False):
        for item in self.alternative_blocks :
            self.alternative_block_set(item, value=value)

    def alternative_block_set(self, item,value=False):
        for curr_path in item.keys():
            for curr_part in item[curr_path]:
                set_requires_grad(self, f'{curr_path}.{curr_part}', value)

    def alternative_step(self):
        self.count_step = self.count_step + 1
        if self.count_step == self.step_size :
            self.count_step = 0

            self.alternative_reset(value = False)
            curr_block = self.alternative_blocks[self.count_block]
            curr_path = list(curr_block.keys())[0]
            curr_part = curr_block[curr_path][self.count_part]
            set_requires_grad(self, f'{curr_path}.{curr_part}', True)

            self.count_part +=1
            if self.count_part == len(curr_block[curr_path]):
                self.count_part = 0
                self.count_block += 1
                if self.count_block >= len(self.alternative_blocks) :
                    self.count_block = 0

    def forward(self, x):
        if self.alternative_mode :
            self.alternative_step()
        x1 = x['img1']
        x2 = x['img2']
        x1 = F.interpolate(x1, (384, 384), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (384, 384), mode='bicubic', align_corners=True)

        out1 = self.model1(x1)
        out2 = self.model2(x2)

        #features1 = self._features['model1.norm'][:, 0]
        #features2 = self._features['model2.norm'][:, 0]
        features1 = self._features['model1.norm']
        features2 = self._features['model2.norm']

        if self.use_cross :
            features1,features2 = self.cross_block(features1,features2)

        features1 = torch.max(features1,dim=1)[0]
        features2 = torch.max(features2,dim=1)[0]

        alpha = self.alpha.repeat(features1.shape[0], 1)
        features = alpha*features1 + (1-alpha) * features2

        features = self.norm(features)
        out = self.classifier(features)

        return out
