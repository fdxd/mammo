
from __future__ import print_function

import torch.nn as nn

import torchvision.models as models
import numpy as np
# model.load_from(np.load(f"{'BiT-M-R101x3'}.npz")).to(device)
from vit_pytorch.vit import ViT
from vit_pytorch.vitsa import ViTsa
from pytorch_pretrained_vit import ViT as preViT
import torch.nn.functional as F


from torchvision import models
import bit_pytorch.models as bit_models

from torchsummary import summary
#from pytorch_model_summary import summary
from core.common import print_model_grads,create_partial_model,set_requires_grad,reset_grads,remove_blocks_from_model

import torch


class BitPairs01(nn.Module):
    def __init__(self,config):
        super(BitPairs01, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        last_fc_size = 2048
        freeze_mode = 'LastBlock' if 'freeze_mode' not in config.keys() else config['freeze_mode']

        pretrained_model = '../bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
        self.model1 = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        weights = np.load(pretrained_model)
        self.model1.load_from(weights)

        self.model2 = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        weights = np.load(pretrained_model)
        self.model2.load_from(weights)

        summary(self.model1, (3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model1)
        print_model_grads(self.model2)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        if freeze_mode == 'LastBlock' :
            set_requires_grad(self, 'model1.body.block4.unit03', True)
            set_requires_grad(self, 'model2.body.block4.unit03', True)

        set_requires_grad(self, 'model1.head', True)
        set_requires_grad(self, 'model2.head', True)
        set_requires_grad(self, 'norm', True)
        set_requires_grad(self, 'classifier', True)
        print_model_grads(self)

        summary(self.model1, ( 3, 384, 384), device='cpu')
        summary(self.model2, (3, 384, 384), device='cpu')

        layers = ['model1.head.avg','model2.head.avg']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))

    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def forward(self, x):
        x1 = x['img1']
        x2 = x['img2']
        x1 = F.interpolate(x1, (384, 384), mode='bicubic', align_corners=True)
        x2 = F.interpolate(x2, (384, 384), mode='bicubic', align_corners=True)

        out1 = self.model1(x1)
        out2 = self.model2(x2)

        features1 = self._features['model1.head.avg'].squeeze(2).squeeze(2)
        features2 = self._features['model2.head.avg'].squeeze(2).squeeze(2)

        features = features1 + features2

        features = self.norm(features)
        out = self.classifier(features)

        return out




class BitFeatures(nn.Module):
    def __init__(self,config):
        super(BitFeatures, self).__init__()

        self.freeze_mode = 'FC_only' if 'freeze_mode' not in config.keys() else config['freeze_mode']
        self.interpolation_size = 384 if 'interpolation_size' not in config.keys() else config['interpolation_size']
        self.alternative_mode = False if 'alternative_mode' not in config.keys() else config['alternative_mode']
        self.alternative_step_size = 1 if 'alternative_step_size' not in config.keys() else config['alternative_step_size']
        last_fc_size = 2048
        freeze_mode = 'LastBlock' if 'freeze_mode' not in config.keys() else config['freeze_mode']

        pretrained_model = 'bit_pytorch/models/BiT-M-R50x1-run2-patch_camelyon.npz'
        self.model = bit_models.KNOWN_MODELS['BiT-M-R50x1'](head_size=len([0, 1]), zero_head=True)
        weights = np.load(pretrained_model)
        self.model.load_from(weights)

        summary(self.model, (3, self.interpolation_size, self.interpolation_size), device='cpu')

        self.norm = nn.LayerNorm(last_fc_size, eps=1e-6)
        self.classifier = nn.Linear(last_fc_size, out_features=2, bias=True)

        print_model_grads(self.model)

        print_model_grads(self)
        set_requires_grad(self, '', False)
        set_requires_grad(self, 'model.head', True)
        set_requires_grad(self, 'norm', True)
        set_requires_grad(self, 'classifier', True)
        print_model_grads(self)

        summary(self.model, (3, self.interpolation_size, self.interpolation_size), device='cpu')

        layers = ['model.body.block1.unit03.relu',
                  'model.body.block2.unit03.relu',
                  'model.body.block3.unit03.relu',
                  'model.body.block4.unit03.relu',
                  'model.head.avg']
        self.layers = layers
        self._features = {layer: torch.empty(0) for layer in layers}
        for layer_id in layers:
            layer = dict([*self.named_modules()])[layer_id]
            layer.register_forward_hook(self.save_outputs_hook(layer_id))

    def save_outputs_hook(self, layer_id: str):
        def fn(_, __, output):
            self._features[layer_id] = output
        return fn

    def forward(self, x):
        x = F.interpolate(x, (self.interpolation_size, self.interpolation_size), mode='bicubic', align_corners=True)
        x = x.repeat(1, 3, 1, 1)
        out1 = self.model(x)
        features = []
        features.append(self._features['model.head.avg'])
        features.append(self._features['model.body.block1.unit03.relu'])
        features.append(self._features['model.body.block2.unit03.relu'])
        features.append(self._features['model.body.block3.unit03.relu'])
        features.append(self._features['model.body.block4.unit03.relu'])

        #features = self.norm(features)
        #out = self.classifier(features)

        return features






class BitFeatures02(BitFeatures):

    def forward(self, x):
        x = F.interpolate(x, (384, 384), mode='bicubic', align_corners=True)
        x = x.repeat(1, 3, 1, 1)
        out1 = self.model(x)
        features = []

        features.append(self._features['model.body.block1.unit03.relu'])
        features.append(self._features['model.body.block2.unit03.relu'])
        features.append(self._features['model.body.block3.unit03.relu'])
        features.append(self._features['model.body.block4.unit03.relu'])
        features.append(self._features['model.head.avg'])
        #features = self.norm(features)
        #out = self.classifier(features)

        return features
